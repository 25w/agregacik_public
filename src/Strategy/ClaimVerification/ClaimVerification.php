<?php


namespace App\Strategy\ClaimVerification;


use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Enum\ClaimType;
use App\Strategy\ClaimVerification\ClaimVerificationStrategyInterface;
use Symfony\Component\HttpFoundation\Request;

class ClaimVerification
{
    private $strategies;

    public function addStrategy(ClaimVerificationStrategyInterface $strategy): void
    {
        $this->strategies[] = $strategy;
    }

    public function run(ClaimType $type, array $payload): void
    {
        /** @var ClaimVerificationStrategyInterface $strategy */
        foreach ($this->strategies as $strategy) {
            if ($strategy->isRunable($type, $payload)) {
                $strategy->run($payload);
                return;
            }
        }
    }
}