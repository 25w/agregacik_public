<?php


namespace App\Strategy\ClaimVerification\Operation;


use App\Agregacik\OperationBundle\DependencyInjection\WatchDogService;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Strategy\Operation\OperationCommon;
use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use App\Entity\Claim;
use App\Entity\User;
use App\Enum\ClaimType;
use App\Factory\DocumentGeneratorFactory;
use App\Service\DocumentGeneratorService;
use App\Strategy\ClaimVerification\ClaimVerificationStrategyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class OdwolanieStrategy extends OperationCommon implements ClaimVerificationStrategyInterface
{
    private $em;
    protected AuthorizationCheckerInterface $authorizationChecker;
    private WatchDogService $wds;
    private ContainerInterface $container;
//    private DocumentGenerator $generator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        ContainerInterface $container,
        WatchDogService $wds
//        DocumentGenerator $generator
    )
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->wds = $wds;
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
//        $this->generator = $generator;
    }

    /**
     * @param ClaimType $type
     * @param array $payload
     * @return bool
     */
    public function isRunable(ClaimType $type, array $payload): bool
    {
        $is_operation_selected = $type->getValue() === ClaimType::ODWOLANIE()->getValue();
        $has_granted_access = $this->authorizationChecker->isGranted('ROLE_WORKER');
        $has_payload = !empty($payload);
        $has_keys = false;
        if ($has_payload) {
            $has_keys = $this->hasKeys($payload, [
                'id', 'claim_type', 'carrier_id', 'logic_question_is_item_readable', 'logic_question_is_item_actual', 'logic_question_three', 'logic_question_was_delay_from_public_data'
            ]);
        }
        $validation_result = $is_operation_selected && $has_granted_access && $has_payload && $has_keys;

        $this->wds->log(ClaimType::ODWOLANIE()->getKey(),
            'RUN', 'ClaimType::ODWOLANIE():isRunable', '', 'TRUE', 'kto odpala? TODO RELACJA');

        return $validation_result;
    }

    public function run(array $payload): void
    {


        //przetwarzanie kluczy : (kod zadba o to aby tutaj sie znalazły wymagane pola)

        if (true) {//$payload['logic_question_is_item_readable'] == 'GO' && $payload['logic_question_is_item_actual'] == 'GO' && $payload['logic_question_was_trip_completed'] == 'GO') {
            if (true) {//$payload['logic_question_was_delay_from_public_data'] == 'END') {
                echo '<p>ZAKONCZONO WERYFIKACJE!</p>';
                echo '<p>TODO zapis danych jak przeprowadzono weryfikacje ??????</p> ';
                echo '<p>TODO przekierowanie do uzupełnienia wzniosku</p> ';
                //TODO end
                /** @var Claim $userClaim */
                $userClaim = $this->em->getRepository(Claim::class)->findOneBy(['id' => (int)$payload['id']]);

                $userClaim->setDataAfterValidation($payload);

                //worker made verification
                $userClaim->setIsVerified();

                //TODO generate documents, APK, regulamin, umowa zrzeczenia roszczeń (ta umowe pokazuje w widoku autoryzacji wniosku biura)
                // documents will be generated on carrier site now. 18.08.2020


//                /** @var DocumentGeneratorFactory $generator */
//                $generator = $this->container->get('app.document.generator');
//                $generator->setTheme('umowa');
//                $generator->setFormat('pdf');
//                $generator->setData(['title' => 'title', 'subject' => 'subject', 'keywords' => 'keywords']);
//                $generator->render();


                //TODO save how claim was confirmed??
                // do mapping for data and save each save, so we have progress.
//TODO save how claim was confirmed??

                print_r($payload);

                die('save claim ver data');

                //TODO set COINS !!!!!!!!!!!!!!!!!1
                //TODO set COINS !!!!!!!!!!!!!!!!!1
                //TODO set COINS !!!!!!!!!!!!!!!!!1
                //TODO set COINS !!!!!!!!!!!!!!!!!1
                //TODO set COINS !!!!!!!!!!!!!!!!!1
                //TODO set COINS !!!!!!!!!!!!!!!!!1


//                //set claim to biuro
                if (!empty($payload['carrier_id'])) {
                    $userClaim->setCarrier($this->em->getRepository(User::class)->find($payload['carrier_id']));
                }

                $this->em->persist($userClaim);
                $this->em->flush();

            } elseif ($payload['logic_question_was_delay_from_public_data'] == 'CONTACT') {
                echo '<p>NOTYFIKACJA KONTAKT Z KLIENTEM !</p>';
                echo '<p>Proszę skontaktować się z klientem i doprecyzować.</p>';

            }
        } else {
            echo '<p>ABORT !</p>'; // var_dump($payload);
        }

        // logic_question_is_item_readable - DLA UPEWNIENIA czy mamy dobre dane
        // logic_question_is_item_actual - DLA UPEWNIENIA czy mamy dobre dane
        // logic_question_three - docelowe pytanie - TAK konczy, NIE odwołanie do kontaktu z klientem
        //koniec


    }
}