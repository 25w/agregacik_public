<?php


namespace App\Strategy\ClaimVerification\Operation;


use App\Agregacik\OperationBundle\DependencyInjection\WatchDogService;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Strategy\Operation\OperationCommon;
use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use App\Entity\Claim;
use App\Entity\User;
use App\Enum\ClaimType;
use App\Factory\DocumentGeneratorFactory;
use App\Service\DocumentGeneratorService;
use App\Strategy\ClaimVerification\ClaimVerificationStrategyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class OdmowaStrategy extends OperationCommon implements ClaimVerificationStrategyInterface
{
    private $em;
    protected AuthorizationCheckerInterface $authorizationChecker;
    private WatchDogService $wds;
    private ContainerInterface $container;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        ContainerInterface $container,
        WatchDogService $wds
    )
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->wds = $wds;
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
    }

    /**
     * @param ClaimType $type
     * @param array $payload
     * @return bool
     */
    public function isRunable(ClaimType $type, array $payload): bool
    {
        $is_operation_selected = $type->getValue() === ClaimType::ODMOWA_PRZYJECIA()->getValue();
        $has_granted_access = $this->authorizationChecker->isGranted('ROLE_WORKER');
        $has_payload = !empty($payload);
        $has_keys = false;
        if ($has_payload) {
            $has_keys = $this->hasKeys($payload, [
                'id', 'claim_type', 'carrier_id', 'logic_question_is_item_readable', 'logic_question_is_item_actual', 'logic_question_three', 'logic_question_was_delay_from_public_data'
            ]);
        }
        $validation_result = $is_operation_selected && $has_granted_access && $has_payload && $has_keys;

        $this->wds->log(ClaimType::ODMOWA_PRZYJECIA()->getKey(),
            'RUN', 'ClaimType::ODMOWA_PRZYJECIA():isRunable', '', 'TRUE', 'kto odpala? TODO RELACJA');

        return $validation_result;
    }

    public function run(array $payload): void
    {

        echo 'TODO ODMOWA strategy';


    }
}