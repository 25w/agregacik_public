<?php


namespace App\Strategy\ClaimVerification;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ClaimVerificationCompilerPass implements CompilerPassInterface
{

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {

        // always first check if the primary service is defined
        if (!$container->has(ClaimVerification::class)) {
            return;
        }

        $resolverService = $container->findDefinition(ClaimVerification::class);

        $strategyServices = array_keys($container->findTaggedServiceIds(ClaimVerificationStrategyInterface::SERVICE_TAG));

        foreach ($strategyServices as $strategyService) {
            $resolverService->addMethodCall('addStrategy', [new Reference($strategyService)]);
        }
    }
}