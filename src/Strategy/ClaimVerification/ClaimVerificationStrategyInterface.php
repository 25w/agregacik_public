<?php


namespace App\Strategy\ClaimVerification;


use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Enum\ClaimType;
use Symfony\Component\HttpFoundation\Request;

interface ClaimVerificationStrategyInterface
{

    public const SERVICE_TAG = 'claim_verification_operation_strategy';
    public const MESSAGE_KEY = 'claim_verification_operation';

    public function isRunable(ClaimType $type, array $payload): bool;
    public function run(array $payload): void;
}