<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class ClaimType
 *
 * https://github.com/myclabs/php-enum
 *
 * @method static self ODMOWA_PRZYJECIA()
 * @method static self OPOZNIENIE()
 * @method static self ODWOLANIE()
 */
class ClaimType extends Enum
{
    private const ODMOWA_PRZYJECIA    = 1;
    private const OPOZNIENIE          = 2;
    private const ODWOLANIE           = 3;
}