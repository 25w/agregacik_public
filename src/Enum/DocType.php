<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class DocType
 *
 * https://github.com/myclabs/php-enum
 *
 * @method static self APK()
 * @method static self REGULAMIN()
 * @method static self UMOWA_ZRZECZENIA()
 * @method static self UMOWA_RAMOWA()
 * @method static self FORMULARZ_INFORMACYJNY()
 */
class DocType extends Enum
{
    private const APK                       = 1;
    private const REGULAMIN                 = 2;
    private const UMOWA_ZRZECZENIA          = 3; // dla klienta
    private const UMOWA_RAMOWA              = 4; // dla biura
    private const FORMULARZ_INFORMACYJNY    = 5; // dla biura
}