<?php


namespace App\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class UserType
 *
 * https://github.com/myclabs/php-enum
 *
 * @method static self ADMIN()
 * @method static self USER()
 * @method static self CARRIER()
 * @method static self WORKER()
 */
class UserType extends Enum
{
    private const ADMIN     = 1;
    private const USER      = 2;
    private const CARRIER   = 3;
    private const WORKER    = 4;
    // INFO API user is in different entity so its omited

    public static function toArrayWithoutAdmin()
    {
        $array = parent::toArray();
        unset($array[self::ADMIN()->getKey()]);
        return $array;
    }
}