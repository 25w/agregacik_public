<?php

namespace App\Entity;

use App\Repository\OperationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OperationRepository::class)
 */
class Operation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $crId;

    /**
     * @ORM\Column(type="integer")
     */
    private $type; // typ akcji

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * One Operation have Many User
     * who made operation?
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="kto_id", referencedColumnName="id")
     */
    private $ktoId;

    /**
     * One Operation have Many User
     * who made operation?
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="komu_id", referencedColumnName="id")
     */
    private $komuId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCrId(): ?int
    {
        return $this->crId;
    }

    public function setCrId(int $crId): self
    {
        $this->crId = $crId;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getKtoId()
    {
        return $this->ktoId;
    }

    /**
     * @param mixed $ktoId
     */
    public function setKtoId($ktoId): void
    {
        $this->ktoId = $ktoId;
    }

    /**
     * @return mixed
     */
    public function getKomuId()
    {
        return $this->komuId;
    }

    /**
     * @param mixed $komuId
     */
    public function setKomuId($komuId): void
    {
        $this->komuId = $komuId;
    }
}
