<?php

namespace App\Entity;

use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfferRepository::class)
 */
class Offer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $carrier;

    /**
     * One Offer has Many Offers.
     * @ORM\OneToMany(targetEntity="Offer", mappedBy="parent", cascade={"persist", "remove"})
     */
    private $children;

    /**
     * Many Offers have One Offer.
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="children", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="File", mappedBy="offer", cascade={"persist", "remove"})
     */
    private $files;

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCarrier(): ?User
    {
        return $this->carrier;
    }

    public function setCarrier(?User $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    public function addChildren(Offer $offer): self
    {
        if (!$this->children->contains($offer)) {
            $this->children[] = $offer;
            $offer->setChildren($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->children->contains($offer)) {
            $this->children->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getChildren() === $this) {
                $offer->setChildren(null);
            }
        }

        return $this;
    }

    public function addFile(File $file): self
    {
        $this->files[] = $file;
        return $this;
    }

    public function removeFile(File $file): bool
    {
        return $this->files->removeElement($file);
    }

    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }
}
