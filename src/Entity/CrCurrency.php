<?php

namespace App\Entity;

use App\Repository\CrCurrencyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Waluta
 *
 * @ORM\Entity(repositoryClass=CrCurrencyRepository::class)
 */
class CrCurrency
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
