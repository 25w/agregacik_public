<?php

namespace App\Entity;

use App\Repository\ApiTokenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApiTokenRepository", repositoryClass=ApiTokenRepository::class)
 */
class ApiToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $token;
    /**
     * @ORM\Column(type="datetime")
     */
    private $expiresAt;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ApiUser", inversedBy="apiTokens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct(ApiUser $user)
    {
        $this->token = bin2hex(random_bytes(60));
        $this->user = $user;
        $this->expiresAt = new \DateTime('+1 hour');
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getToken(): ?string
    {
        return $this->token;
    }
    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }
    public function getUser(): ?ApiUser
    {
        return $this->user;
    }
    public function renewExpiresAt()
    {
        $this->expiresAt = new \DateTime('+1 hour');
    }
    public function isExpired(): bool
    {
        return $this->getExpiresAt() <= new \DateTime();
    }
}
