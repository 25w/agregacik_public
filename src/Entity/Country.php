<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $iso_code;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dafif_code;

    /**
     * @ORM\OneToMany(targetEntity=City::class, mappedBy="country", orphanRemoval=true)
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity=Airline::class, mappedBy="country")
     */
    private $airlines;

    /**
     * @ORM\OneToMany(targetEntity=Airport::class, mappedBy="country")
     */
    private $airports;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="ticket_country_from", orphanRemoval=true)
     */
    private $claimProcessesTicketCountryFrom;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="ticket_country_to", orphanRemoval=true)
     */
    private $claimProcessesTicketCountryTo;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="real_country_from", orphanRemoval=true)
     */
    private $claimProcessesRealCountryFrom;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="real_country_to", orphanRemoval=true)
     */
    private $claimProcessesRealCountryTo;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->airlines = new ArrayCollection();
        $this->airports = new ArrayCollection();
        $this->claimProcessesTicketCountryFrom = new ArrayCollection();
        $this->claimProcessesTicketCountryTo = new ArrayCollection();
        $this->claimProcessesRealCountryFrom = new ArrayCollection();
        $this->claimProcessesRealCountryTo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsoCode(): ?string
    {
        return $this->iso_code;
    }

    public function setIsoCode(string $iso_code): self
    {
        $this->iso_code = $iso_code;

        return $this;
    }

    public function getDafifCode(): ?string
    {
        return $this->dafif_code;
    }

    public function setDafifCode(?string $dafif_code): self
    {
        $this->dafif_code = $dafif_code;

        return $this;
    }

    /**
     * @return Collection|City[]
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(City $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities[] = $city;
            $city->setCountry($this);
        }

        return $this;
    }

    public function removeCity(City $city): self
    {
        if ($this->cities->contains($city)) {
            $this->cities->removeElement($city);
            // set the owning side to null (unless already changed)
            if ($city->getCountry() === $this) {
                $city->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Airline[]
     */
    public function getAirlines(): Collection
    {
        return $this->airlines;
    }

    public function addAirline(Airline $airline): self
    {
        if (!$this->airlines->contains($airline)) {
            $this->airlines[] = $airline;
            $airline->setCountry($this);
        }

        return $this;
    }

    public function removeAirline(Airline $airline): self
    {
        if ($this->airlines->contains($airline)) {
            $this->airlines->removeElement($airline);
            // set the owning side to null (unless already changed)
            if ($airline->getCountry() === $this) {
                $airline->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Airport[]
     */
    public function getAirports(): Collection
    {
        return $this->airports;
    }

    public function addAirport(Airport $airport): self
    {
        if (!$this->airports->contains($airport)) {
            $this->airports[] = $airport;
            $airport->setCountry($this);
        }

        return $this;
    }

    public function removeAirport(Airport $airport): self
    {
        if ($this->airports->contains($airport)) {
            $this->airports->removeElement($airport);
            // set the owning side to null (unless already changed)
            if ($airport->getCountry() === $this) {
                $airport->setCountry(null);
            }
        }

        return $this;
    }



    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesTicketCountryFrom(): Collection
    {
        return $this->claimProcessesTicketCountryFrom;
    }

    public function addClaimProcessTicketCountryFrom(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcessesTicketCountryFrom->contains($claimProcess)) {
            $this->claimProcessesTicketCountryFrom[] = $claimProcess;
            $claimProcess->setTicketCountryFrom($this);
        }

        return $this;
    }

    public function removeClaimProcessTicketCountryFrom(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcessesTicketCountryFrom->contains($claimProcess)) {
            $this->claimProcessesTicketCountryFrom->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getTicketCountryFrom() === $this) {
                $claimProcess->setTicketCountryFrom(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesTicketCountryTo(): Collection
    {
        return $this->claimProcessesTicketCountryTo;
    }

    public function addClaimProcessesTicketCountryTo(ClaimProcess $claimProcesses): self
    {
        if (!$this->claimProcessesTicketCountryTo->contains($claimProcesses)) {
            $this->claimProcessesTicketCountryTo[] = $claimProcesses;
            $claimProcesses->setTicketCountryTo($this);
        }

        return $this;
    }

    public function removeClaimProcessesTicketCountryTo(ClaimProcess $claimProcesses): self
    {
        if ($this->claimProcessesTicketCountryTo->contains($claimProcesses)) {
            $this->claimProcessesTicketCountryTo->removeElement($claimProcesses);
            // set the owning side to null (unless already changed)
            if ($claimProcesses->getTicketCountryTo() === $this) {
                $claimProcesses->setTicketCountryTo(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesRealCountryFrom(): Collection
    {
        return $this->claimProcessesRealCountryFrom;
    }

    public function addClaimProcessRealCountryFrom(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcessesRealCountryFrom->contains($claimProcess)) {
            $this->claimProcessesRealCountryFrom[] = $claimProcess;
            $claimProcess->setRealCountryFrom($this);
        }

        return $this;
    }

    public function removeClaimProcessRealCountryFrom(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcessesRealCountryFrom->contains($claimProcess)) {
            $this->claimProcessesRealCountryFrom->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getRealCountryFrom() === $this) {
                $claimProcess->setRealCountryFrom(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesRealCountryTo(): Collection
    {
        return $this->claimProcessesRealCountryTo;
    }

    public function addClaimProcessesRealCountryTo(ClaimProcess $claimProcesses): self
    {
        if (!$this->claimProcessesRealCountryTo->contains($claimProcesses)) {
            $this->claimProcessesRealCountryTo[] = $claimProcesses;
            $claimProcesses->setRealCountryTo($this);
        }

        return $this;
    }

    public function removeClaimProcessesRealCountryTo(ClaimProcess $claimProcesses): self
    {
        if ($this->claimProcessesRealCountryTo->contains($claimProcesses)) {
            $this->claimProcessesRealCountryTo->removeElement($claimProcesses);
            // set the owning side to null (unless already changed)
            if ($claimProcesses->getRealCountryTo() === $this) {
                $claimProcesses->setRealCountryTo(null);
            }
        }

        return $this;
    }
}
