<?php


namespace App\Entity;

use App\Enum\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="I think you've already registered!"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups("main")
     * @Assert\NotBlank(message="Please enter an login/username")
     */
    private $login; //vel username

    /**
     * @ORM\Column(type="string", length=180, unique=false)
     * @Groups("main")
     * @Assert\NotBlank(message="Please enter an email")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups("main")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups("main")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups("main")
     */
    private $companyName;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * One User have Many UserMeta
     *
     * @ORM\OneToMany(targetEntity="UserMeta", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userMeta;

    /**
     * One User have Many Claim
     *
     * @ORM\OneToMany(targetEntity="Claim", mappedBy="claim", cascade={"persist", "remove"})
     */
    private $claim;

    /**
     * One User have Many Claim
     *
     * @ORM\OneToMany(targetEntity="Claim", mappedBy="claim_author", cascade={"persist", "remove"})
     */
    private $claim_author;

    /**
     * @ORM\Column(type="string", length=3272, nullable=true)
     * @Groups("main")
     */
    private $private_key;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     * @Groups("main")
     */
    private $public_key;

    /**
     * @ORM\OneToMany(targetEntity=Offer::class, mappedBy="carrier", orphanRemoval=true)
     */
    private $offers;

    /**
     * @see UserType
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     * @ORM\Column(type="integer", name="type", options={"default" : 1})
     */
    private $type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="worker", orphanRemoval=true)
     */
    private $claimProcesses;

    /**
     * @ORM\OneToOne(targetEntity=UserSensitiveData::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $userSensitiveData;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $last_login;

    public function __construct()
    {
        $this->userMeta = new ArrayCollection();
        $this->claim = new ArrayCollection();
        $this->claim_author = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->claimProcesses = new ArrayCollection();
    }

    public function getUserMeta(): Collection
    {
        return $this->userMeta;
    }

    public function addUserMeta(?UserMeta $userMeta): void
    {
        $userMeta->setUser($this);
        if (!$this->userMeta->contains($userMeta)) {
            $this->userMeta->add($userMeta);
        }
    }

    public function removeUserMeta(UserMeta $userMeta): void
    {
        $userMeta->setUser(null);
        $this->userMeta->removeElement($userMeta);
    }

    public function getClaim(): Collection
    {
        return $this->claim;
    }

    public function addClaim(?Claim $claim): void
    {
        $claim->setCarrier($this);
        if (!$this->claim->contains($claim)) {
            $this->claim->add($claim);
        }
    }

    public function removeClaim(Claim $claim): void
    {
        $claim->setCarrier(null);
        $this->claim->removeElement($claim);
    }

    public function getClaimAuthor(): Collection
    {
        return $this->claim_author;
    }

    public function addClaimAuthor(?Claim $claim): void
    {
        $claim->setAuthor($this);
        if (!$this->claim_author->contains($claim)) {
            $this->claim_author->add($claim);
        }
    }

    public function removeClaimAuthor(Claim $claim): void
    {
        $claim->setAuthor(null);
        $this->claim_author->removeElement($claim);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function checkIfHasRole(string $role = 'ROLE_USER'): bool
    {
        return array_key_exists($role, $this->getRoles());
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using bcrypt or argon
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function __toString()
    {
        return $this->getFirstName();
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getPrivateKey()
    {
        return $this->private_key;
    }

    /**
     * @param mixed $private_key
     */
    public function setPrivateKey($private_key): void
    {
        $this->private_key = $private_key;
    }

    /**
     * @return mixed
     */
    public function getPublicKey()
    {
        return $this->public_key;
    }

    /**
     * @param mixed $public_key
     */
    public function setPublicKey($public_key): void
    {
        $this->public_key = $public_key;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setCarrier($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getCarrier() === $this) {
                $offer->setCarrier(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function isActive()
    {
        return (bool)$this->active;
    }

    /**W
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcesses(): Collection
    {
        return $this->claimProcesses;
    }

    public function addClaimProcess(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcesses->contains($claimProcess)) {
            $this->claimProcesses[] = $claimProcess;
            $claimProcess->setWorker($this);
        }

        return $this;
    }

    public function removeClaimProcess(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcesses->contains($claimProcess)) {
            $this->claimProcesses->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getWorker() === $this) {
                $claimProcess->setWorker(null);
            }
        }

        return $this;
    }

    public function getUserSensitiveData(): ?UserSensitiveData
    {
        return $this->userSensitiveData;
    }

    public function hasUserSensitiveData(): bool
    {
        return (bool) !empty($this->userSensitiveData);
    }

    public function setUserSensitiveData(?UserSensitiveData $userSensitiveData): self
    {
        $this->userSensitiveData = $userSensitiveData;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $userSensitiveData ? null : $this;
        if ($userSensitiveData->getUser() !== $newUser) {
            $userSensitiveData->setUser($newUser);
        }

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->last_login;
    }

    /**
     * @param \DateTimeInterface $last_login
     * @return $this
     */
    public function setLastLogin(\DateTimeInterface $last_login): self
    {
        $this->last_login = $last_login;

        return $this;
    }

}
