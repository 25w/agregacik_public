<?php

namespace App\Entity;

use App\Repository\CashRegisterRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Kasa (jak urządzenie)
 *
 * @ORM\Entity(repositoryClass=CashRegisterRepository::class)
// * @ORM\HasLifecycleCallbacks
 */
class CashRegister
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

//    /**
//     * One CashRegister have Many CrVolume
//     *
//     * @ORM\ManyToOne(targetEntity="CrVolume")
//     * @ORM\JoinColumn(name="cr_volume_id", referencedColumnName="id")
//     */
//    private $crVolumeId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CrVolume", mappedBy="cash_register")
     */
    private $volume;

    /**
     * One CashRegister has One User.
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;
    
    public function getId(): ?int
    {
        return $this->id;
    }

//    /**
//     * @return mixed
//     */
//    public function getCrVolumeId()
//    {
//        return $this->crVolumeId;
//    }
//
//    /**
//     * @param mixed $crVolumeId
//     */
//    public function setCrVolumeId($crVolumeId): void
//    {
//        $this->crVolumeId = $crVolumeId;
//    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     */
    public function setVolume($volume): void
    {
        $this->volume = $volume;
    }
}
