<?php

namespace App\Entity;

use App\Repository\WatchDogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WatchDogRepository::class)
 */
class WatchDog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $akcja;

    /**
     * @ORM\Column(type="string")
     */
    private $co;

    /**
     * @ORM\Column(type="string")
     */
    private $gdzie;

    /**
     * @ORM\Column(type="string")
     */
    private $zmiana_z;

    /**
     * @ORM\Column(type="string")
     */
    private $zmiana_na;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $kiedy;

    /**
     * @ORM\Column(type="string")
     */
    private $kto;

    public function __construct()
    {
        $this->setKiedy(new \DateTime(date('Y-m-d H:i:s')));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAkcja(): ?string
    {
        return $this->akcja;
    }

    public function setAkcja(string $akcja): self
    {
        $this->akcja = $akcja;

        return $this;
    }

    public function getCo(): ?string
    {
        return $this->co;
    }

    public function setCo(string $co): self
    {
        $this->co = $co;

        return $this;
    }

    public function getGdzie(): ?string
    {
        return $this->gdzie;
    }

    public function setGdzie(string $gdzie): self
    {
        $this->gdzie = $gdzie;

        return $this;
    }

    public function getZmianaZ(): ?string
    {
        return $this->zmiana_z;
    }

    public function setZmianaZ(string $zmiana_z): self
    {
        $this->zmiana_z = $zmiana_z;

        return $this;
    }

    public function getZmianaNa(): ?string
    {
        return $this->zmiana_na;
    }

    public function setZmianaNa(string $zmiana_na): self
    {
        $this->zmiana_na = $zmiana_na;

        return $this;
    }

    public function getKiedy(): ?\DateTimeInterface
    {
        return $this->kiedy;
    }

    public function setKiedy(\DateTimeInterface $kiedy): self
    {
        $this->kiedy = $kiedy;

        return $this;
    }

    public function getKto(): ?string
    {
        return $this->kto;
    }

    public function setKto(string $kto): self
    {
        $this->kto = $kto;

        return $this;
    }


}
