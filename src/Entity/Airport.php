<?php

namespace App\Entity;

use App\Repository\AirportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AirportRepository::class)
 */
class Airport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $iata;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $icao;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $altitude;

    /**
     * @ORM\Column(type="string")
     */
    private $timezone;

    /**
     * @ORM\Column(type="string")
     */
    private $dst;

    /**
     * @ORM\Column(type="string")
     */
    private $tz;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $source;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="airports")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="airports")
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="ticket_airport_from", orphanRemoval=true)
     */
    private $claimProcessesTicketAirportFrom;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="ticket_airport_to", orphanRemoval=true)
     */
    private $claimProcessesTicketAirportTo;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="real_airport_from", orphanRemoval=true)
     */
    private $claimProcessesRealAirportFrom;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="real_airport_to", orphanRemoval=true)
     */
    private $claimProcessesRealAirportTo;

    public function __construct()
    {
        $this->claimProcessesTicketAirportFrom = new ArrayCollection();
        $this->claimProcessesTicketAirportTo = new ArrayCollection();
        $this->claimProcessesRealAirportFrom = new ArrayCollection();
        $this->claimProcessesRealAirportTo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIata(): ?string
    {
        return $this->iata;
    }

    public function setIata(?string $iata): self
    {
        $this->iata = $iata;

        return $this;
    }

    public function getIcao(): ?string
    {
        return $this->icao;
    }

    public function setIcao(?string $icao): self
    {
        $this->icao = $icao;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getAltitude(): ?string
    {
        return $this->altitude;
    }

    public function setAltitude(?string $altitude): self
    {
        $this->altitude = $altitude;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getDst(): ?string
    {
        return $this->dst;
    }

    public function setDst(string $dst): self
    {
        $this->dst = $dst;

        return $this;
    }

    public function getTz(): ?string
    {
        return $this->tz;
    }

    public function setTz(string $tz): self
    {
        $this->tz = $tz;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry(?Country$country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesTicketAirportFrom(): Collection
    {
        return $this->claimProcessesTicketAirportFrom;
    }

    public function addClaimProcessTicketAirportFrom(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcessesTicketAirportFrom->contains($claimProcess)) {
            $this->claimProcessesTicketAirportFrom[] = $claimProcess;
            $claimProcess->setTicketAirportFrom($this);
        }

        return $this;
    }

    public function removeClaimProcessTicketAirportFrom(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcessesTicketAirportFrom->contains($claimProcess)) {
            $this->claimProcessesTicketAirportFrom->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getTicketAirportFrom() === $this) {
                $claimProcess->setTicketAirportFrom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesTicketAirportTo(): Collection
    {
        return $this->claimProcessesTicketAirportTo;
    }

    public function addClaimProcessTicketAirportTo(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcessesTicketAirportTo->contains($claimProcess)) {
            $this->claimProcessesTicketAirportTo[] = $claimProcess;
            $claimProcess->setTicketAirportTo($this);
        }

        return $this;
    }

    public function removeClaimProcessTicketAirportTo(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcessesTicketAirportTo->contains($claimProcess)) {
            $this->claimProcessesTicketAirportTo->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getTicketAirportTo() === $this) {
                $claimProcess->setTicketAirportTo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesRealAirportFrom(): Collection
    {
        return $this->claimProcessesRealAirportFrom;
    }

    public function addClaimProcessRealAirportFrom(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcessesRealAirportFrom->contains($claimProcess)) {
            $this->claimProcessesRealAirportFrom[] = $claimProcess;
            $claimProcess->setRealAirportFrom($this);
        }

        return $this;
    }

    public function removeClaimProcessRealAirportFrom(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcessesRealAirportFrom->contains($claimProcess)) {
            $this->claimProcessesRealAirportFrom->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getRealAirportFrom() === $this) {
                $claimProcess->setRealAirportFrom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcessesRealAirportTo(): Collection
    {
        return $this->claimProcessesRealAirportTo;
    }

    public function addClaimProcessRealAirportTo(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcessesRealAirportTo->contains($claimProcess)) {
            $this->claimProcessesRealAirportTo[] = $claimProcess;
            $claimProcess->setRealAirportTo($this);
        }

        return $this;
    }

    public function removeClaimProcessRealAirportTo(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcessesRealAirportTo->contains($claimProcess)) {
            $this->claimProcessesRealAirportTo->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getRealAirportTo() === $this) {
                $claimProcess->setRealAirportTo(null);
            }
        }

        return $this;
    }
}
