<?php

namespace App\Entity;

use App\Repository\ClaimProcessRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClaimProcessRepository::class)
 */
class ClaimProcess
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    // pytania z kroku 1

    /**
     * @ORM\ManyToOne(targetEntity=Claim::class, inversedBy="claimProcesses")
     */
    private $claim;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $is_readable;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $is_actual;

    /**
     * Aktualnie jeden worker przetwarza wlasne claims???????????? FIXME
     *
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="claimProcesses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $worker;

    // END pytania z kroku 1
    // pytania z kroku 2

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ticket_meta_uid;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $ticket_meta_passenger_name;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Airline::class, inversedBy="claimProcesses")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ticket_meta_carrier;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $ticket_flight_no;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ticket_class;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="claimProcessesTicketCountryFrom")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ticket_country_from;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Airport::class, inversedBy="claimProcessesTicketAirportFrom")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ticket_airport_from;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="claimProcessesTicketCountryTo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ticket_country_to;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Airport::class, inversedBy="claimProcessesTicketAirportTo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $ticket_airport_to;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string A "Y-m-d" formatted value
     */
    private $ticket_date;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string A "H:i:s" formatted value
     */
    private $ticket_time;

    /**
     * @var string
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ticket_seat;

    /**
     * @var string
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ticket_gate;

    // END pytania z kroku 2
    // pytania z kroku 3

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_trip_completed;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_delay_in_public_data;

    /**
     * @var string
     * @ORM\Column(type="text", length=5000, nullable=true)
     */
    private $delay_from_public_data_details;


    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $real_class;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="claimProcessesTicketCountryFrom")
     * @ORM\JoinColumn(nullable=true)
     */
    private $real_country_from;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Airport::class, inversedBy="claimProcessesTicketAirportFrom")
     * @ORM\JoinColumn(nullable=true)
     */
    private $real_airport_from;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="claimProcessesTicketCountryTo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $real_country_to;

    /**
     *
     * @ORM\ManyToOne(targetEntity=Airport::class, inversedBy="claimProcessesTicketAirportTo")
     * @ORM\JoinColumn(nullable=true)
     */
    private $real_airport_to;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string A "Y-m-d" formatted value
     */
    private $real_date;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string A "H:i:s" formatted value
     */
    private $real_time;

    /**
     * @var string
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $real_seat;

    /**
     * @var string
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $real_gate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClaim(): ?Claim
    {
        return $this->claim;
    }

    public function setClaim(?Claim $claim): self
    {
        $this->claim = $claim;

        return $this;
    }

    public function getIsReadable(): ?bool
    {
        return $this->is_readable;
    }

    public function setIsReadable(bool $is_readable): self
    {
        $this->is_readable = $is_readable;

        return $this;
    }

    public function getIsActual(): ?bool
    {
        return $this->is_actual;
    }

    public function setIsActual(bool $is_actual): self
    {
        $this->is_actual = $is_actual;

        return $this;
    }

    public function getIsTripCompleted(): ?bool
    {
        return $this->is_trip_completed;
    }

    public function setIsTripCompleted(bool $is_trip_completed): self
    {
        $this->is_trip_completed = $is_trip_completed;

        return $this;
    }

    public function getWorker(): ?User
    {
        return $this->worker;
    }

    public function setWorker(?User $worker): self
    {
        $this->worker = $worker;

        return $this;
    }

    public function isStepOneComplete() :bool
    {
        return (bool) !is_null($this->is_readable) && !is_null($this->is_actual);
    }

    public function isStepTwoComplete() :bool
    {
        return (bool) !empty($this->ticket_meta_passenger_name) && !empty($this->ticket_flight_no) &&
            !empty($this->ticket_meta_carrier) &&
            !empty($this->ticket_country_from) && !empty($this->ticket_country_to) &&
            !empty($this->ticket_airport_from) && !empty($this->ticket_airport_to) &&
            !empty($this->ticket_date) && !empty($this->ticket_time);
    }

    /**
     * @return string
     */
    public function getTicketMetaUid(): string
    {
        return $this->ticket_meta_uid;
    }

    /**
     * @param string $ticket_meta_uid
     */
    public function setTicketMetaUid(string $ticket_meta_uid): void
    {
        $this->ticket_meta_uid = $ticket_meta_uid;
    }

    /**
     * @return string
     */
    public function getTicketMetaPassengerName(): string
    {
        return $this->ticket_meta_passenger_name;
    }

    /**
     * @param string $ticket_meta_passenger_name
     */
    public function setTicketMetaPassengerName(string $ticket_meta_passenger_name): void
    {
        $this->ticket_meta_passenger_name = $ticket_meta_passenger_name;
    }

    /**
     * @return string
     */
    public function getTicketFlightNo(): string
    {
        return $this->ticket_flight_no;
    }

    /**
     * @param string $ticket_flight_no
     */
    public function setTicketFlightNo(string $ticket_flight_no): void
    {
        $this->ticket_flight_no = $ticket_flight_no;
    }

    /**
     * @return int
     */
    public function getTicketClass(): int
    {
        return $this->ticket_class;
    }

    /**
     * @param int $ticket_class
     */
    public function setTicketClass(int $ticket_class): void
    {
        $this->ticket_class = $ticket_class;
    }

    /**
     * @return mixed
     */
    public function getTicketMetaCarrier()
    {
        return $this->ticket_meta_carrier;
    }

    /**
     * @param mixed $ticket_meta_carrier
     */
    public function setTicketMetaCarrier($ticket_meta_carrier): void
    {
        $this->ticket_meta_carrier = $ticket_meta_carrier;
    }

    /**
     * @return mixed
     */
    public function getTicketCountryFrom()
    {
        return $this->ticket_country_from;
    }

    /**
     * @param mixed $ticket_country_from
     */
    public function setTicketCountryFrom($ticket_country_from): void
    {
        $this->ticket_country_from = $ticket_country_from;
    }

    /**
     * @return mixed
     */
    public function getTicketAirportFrom()
    {
        return $this->ticket_airport_from;
    }

    /**
     * @param mixed $ticket_airport_from
     */
    public function setTicketAirportFrom($ticket_airport_from): void
    {
        $this->ticket_airport_from = $ticket_airport_from;
    }

    /**
     * @return mixed
     */
    public function getTicketCountryTo()
    {
        return $this->ticket_country_to;
    }

    /**
     * @param mixed $ticket_country_to
     */
    public function setTicketCountryTo($ticket_country_to): void
    {
        $this->ticket_country_to = $ticket_country_to;
    }

    /**
     * @return mixed
     */
    public function getTicketAirportTo()
    {
        return $this->ticket_airport_to;
    }

    /**
     * @param mixed $ticket_airport_to
     */
    public function setTicketAirportTo($ticket_airport_to): void
    {
        $this->ticket_airport_to = $ticket_airport_to;
    }

    /**
     * @return string
     */
    public function getTicketDate(): string
    {
        return $this->ticket_date;
    }

    /**
     * @param string $ticket_date
     */
    public function setTicketDate(string $ticket_date): void
    {
        $this->ticket_date = $ticket_date;
    }

    /**
     * @return string
     */
    public function getTicketTime(): string
    {
        return $this->ticket_time;
    }

    /**
     * @param string $ticket_time
     */
    public function setTicketTime(string $ticket_time): void
    {
        $this->ticket_time = $ticket_time;
    }

    /**
     * @return string
     */
    public function getTicketSeat(): string
    {
        return $this->ticket_seat;
    }

    /**
     * @param string $ticket_seat
     */
    public function setTicketSeat(string $ticket_seat): void
    {
        $this->ticket_seat = $ticket_seat;
    }

    /**
     * @return string
     */
    public function getTicketGate(): string
    {
        return $this->ticket_gate;
    }

    /**
     * @param string $ticket_gate
     */
    public function setTicketGate(string $ticket_gate): void
    {
        $this->ticket_gate = $ticket_gate;
    }

    /**
     * @return bool
     */
    public function isIsDelayInPublicData(): bool
    {
        return (bool) $this->is_delay_in_public_data;
    }

    /**
     * @param bool $is_delay_in_public_data
     */
    public function setIsDelayInPublicData(bool $is_delay_in_public_data): void
    {
        $this->is_delay_in_public_data = $is_delay_in_public_data;
    }

    /**
     * @return string|null
     */
    public function getDelayFromPublicDataDetails()
    {
        return $this->delay_from_public_data_details;
    }

    /**
     * @param string $delay_from_public_data_details
     */
    public function setDelayFromPublicDataDetails(string $delay_from_public_data_details): void
    {
        $this->delay_from_public_data_details = $delay_from_public_data_details;
    }

    /**
     * @return int
     */
    public function getRealClass(): int
    {
        return $this->real_class;
    }

    /**
     * @param int $real_class
     */
    public function setRealClass(int $real_class): void
    {
        $this->real_class = $real_class;
    }

    /**
     * @return mixed
     */
    public function getRealCountryFrom()
    {
        return $this->real_country_from;
    }

    /**
     * @param mixed $real_country_from
     */
    public function setRealCountryFrom($real_country_from): void
    {
        $this->real_country_from = $real_country_from;
    }

    /**
     * @return mixed
     */
    public function getRealAirportFrom()
    {
        return $this->real_airport_from;
    }

    /**
     * @param mixed $real_airport_from
     */
    public function setRealAirportFrom($real_airport_from): void
    {
        $this->real_airport_from = $real_airport_from;
    }

    /**
     * @return mixed
     */
    public function getRealCountryTo()
    {
        return $this->real_country_to;
    }

    /**
     * @param mixed $real_country_to
     */
    public function setRealCountryTo($real_country_to): void
    {
        $this->real_country_to = $real_country_to;
    }

    /**
     * @return mixed
     */
    public function getRealAirportTo()
    {
        return $this->real_airport_to;
    }

    /**
     * @param mixed $real_airport_to
     */
    public function setRealAirportTo($real_airport_to): void
    {
        $this->real_airport_to = $real_airport_to;
    }

    /**
     * @return string
     */
    public function getRealDate(): string
    {
        return $this->real_date;
    }

    /**
     * @param string $real_date
     */
    public function setRealDate(string $real_date): void
    {
        $this->real_date = $real_date;
    }

    /**
     * @return string
     */
    public function getRealTime(): string
    {
        return $this->real_time;
    }

    /**
     * @param string $real_time
     */
    public function setRealTime(string $real_time): void
    {
        $this->real_time = $real_time;
    }

    /**
     * @return string
     */
    public function getRealSeat(): string
    {
        return $this->real_seat;
    }

    /**
     * @param string $real_seat
     */
    public function setRealSeat(string $real_seat): void
    {
        $this->real_seat = $real_seat;
    }

    /**
     * @return string
     */
    public function getRealGate(): string
    {
        return $this->real_gate;
    }

    /**
     * @param string $real_gate
     */
    public function setRealGate(string $real_gate): void
    {
        $this->real_gate = $real_gate;
    }


}
