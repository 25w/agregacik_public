<?php

namespace App\Entity;

use App\Repository\FileRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository", repositoryClass=FileRepository::class)
 */
class File
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=3000)
     */
    private $path;

    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Claim", inversedBy="files", cascade={"persist"})
     * @ORM\JoinColumn(name="claim_id", referencedColumnName="id", nullable=true)
     */
    private $claim;

    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="files", cascade={"persist"})
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id", nullable=true)
     */
    private $offer;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getClaim(): Claim
    {
        return $this->claim;
    }

    /**
     * @param Claim $claim
     */
    public function setClaim(Claim $claim): self
    {
        $this->claim = $claim;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param mixed $offer
     */
    public function setOffer($offer): void
    {
        $this->offer = $offer;
    }

}
