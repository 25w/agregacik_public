<?php

namespace App\Entity;

use App\Repository\ClaimRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ClaimRepository::class)
 */
class Claim
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=13, nullable=false)
     */
    private $uid;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * Value of the claim determineted by claim verify
     *
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $worthValue;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank()
     * @var string A "Y-m-d H:i:s" formatted value
     */
    private $date_added;

    /**
     * Change when carrier accept claim
     *
     * @ORM\Column(name="is_authenticated", type="boolean")
     */
    private $isAuthenticated;

    /**
     * Cahnge when worker verifiy claim
     *
     * @ORM\Column(name="is_verified", type="boolean")
     */
    private $isVerified;

    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="File", mappedBy="claim", cascade={"persist", "remove"})
     */
    private $files;

    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="claim", cascade={"persist"})
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="id", nullable=true)
     */
    private $carrier;

    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="claim_author", cascade={"persist"})
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="claim")
     */
    private $claimProcesses;

    public function __construct() {
        $this->uid = uniqid();
        $this->isAuthenticated = false;
        $this->isVerified = false;
        $this->files = new ArrayCollection();
        $this->setDateAdded(new \DateTime(date('Y-m-d H:i:s')));
        $this->claimProcesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDateAdded():  ?\DateTimeInterface
    {
        return $this->date_added;
    }

    /**
     * @param mixed $date_added
     */
    public function setDateAdded(\DateTimeInterface $date_added): self
    {
        $this->date_added = $date_added;

        return $this;
    }

    public function addFile(File $file): self
    {
        $this->files[] = $file;
        return $this;
    }

    public function removeFile(File $file): bool
    {
        return $this->files->removeElement($file);
    }

    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return $this->isAuthenticated;
    }

    /**
     * @param bool $isAuthenticated
     */
    public function setIsAuthenticated(bool $isAuthenticated = true): void
    {
        $this->isAuthenticated = $isAuthenticated;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified(bool $isVerified = true): void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return mixed
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param mixed $carrier
     */
    public function setCarrier($carrier): void
    {
        $this->carrier = $carrier;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcesses(): Collection
    {
        return $this->claimProcesses;
    }

    public function addClaimProcess(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcesses->contains($claimProcess)) {
            $this->claimProcesses[] = $claimProcess;
            $claimProcess->setClaim($this);
        }

        return $this;
    }

    public function removeClaimProcess(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcesses->contains($claimProcess)) {
            $this->claimProcesses->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getClaim() === $this) {
                $claimProcess->setClaim(null);
            }
        }

        return $this;
    }

    /**
     * @return float
     */
    public function getWorthValue(): float
    {
        return $this->worthValue;
    }

    /**
     * @param float $worthValue
     */
    public function setWorthValue(float $worthValue): void
    {
        $this->worthValue = $worthValue;
    }

}
