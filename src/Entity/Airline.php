<?php

namespace App\Entity;

use App\Repository\AirlineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AirlineRepository::class)
 */
class Airline
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $iata;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $icao;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $callsign;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="airlines")
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=ClaimProcess::class, mappedBy="ticket_meta_carrier", orphanRemoval=true)
     */
    private $claimProcesses;

    public function __construct()
    {
        $this->claimProcesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getIata(): ?string
    {
        return $this->iata;
    }

    public function setIata(?string $iata): self
    {
        $this->iata = $iata;

        return $this;
    }

    public function getIcao(): ?string
    {
        return $this->icao;
    }

    public function setIcao(?string $icao): self
    {
        $this->icao = $icao;

        return $this;
    }

    public function getCallsign(): ?string
    {
        return $this->callsign;
    }

    public function setCallsign(?string $callsign): self
    {
        $this->callsign = $callsign;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|ClaimProcess[]
     */
    public function getClaimProcesses(): Collection
    {
        return $this->claimProcesses;
    }

    public function addClaimProcess(ClaimProcess $claimProcess): self
    {
        if (!$this->claimProcesses->contains($claimProcess)) {
            $this->claimProcesses[] = $claimProcess;
            $claimProcess->setTicketMetaCarrier($this);
        }

        return $this;
    }

    public function removeClaimProcess(ClaimProcess $claimProcess): self
    {
        if ($this->claimProcesses->contains($claimProcess)) {
            $this->claimProcesses->removeElement($claimProcess);
            // set the owning side to null (unless already changed)
            if ($claimProcess->getTicketMetaCarrier() === $this) {
                $claimProcess->setTicketMetaCarrier(null);
            }
        }

        return $this;
    }
}
