<?php

namespace App\Entity;

use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Repository\CrVolumeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Wolumen / wartość
 *
 * @ORM\Entity(repositoryClass=CrVolumeRepository::class)
// * @ORM\HasLifecycleCallbacks
 */
class CrVolume
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $worth;

    /**
     * type of worth - can be:
     * 1 as NO operation
     * 2 as ADD on operation
     * 3 as DEL on operation
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     * @ORM\Column(type="integer", name="type", options={"default" : 1})
     */
    private $type;

    /**
     * status - can be:
     * 1 as NO operation
     * 2 as SERVED on operation
     * 3 as ABORTED on operation
     *
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank()
     * @ORM\Column(name="status", type="integer", nullable=false, options={"default" : 1})
     */
    private $status;

    /**
     * One CashRegister have Many CrVolume
     *
     * @ORM\ManyToOne(targetEntity="CashRegister", inversedBy="volume", cascade={"persist"})
     * @ORM\JoinColumn(name="cash_register", referencedColumnName="id", nullable=false)
     */
    private $cash_register;

    /**
     * One CrVolume have Many CrCurrency
     *
     * @ORM\ManyToOne(targetEntity="CrCurrency")
     * @ORM\JoinColumn(name="cr_currency_id", referencedColumnName="id")
     */
    private $crCurrencyId;


    public function __construct() {
        $this->setStatus(OperationType::OPERATION_STATUS_NO_CHANGE()->getValue());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWorth(): ?int
    {
        return $this->worth;
    }

    public function setWorth(int $worth): self
    {
        $this->worth = $worth;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCrCurrency()
    {
        return $this->crCurrencyId;
    }

    /**
     * @param mixed $crCurrencyId
     */
    public function setCrCurrency($crCurrencyId): void
    {
        $this->crCurrencyId = $crCurrencyId;
    }

    /**
     * @return mixed
     */
    public function getCashRegister()
    {
        return $this->cash_register;
    }

    /**
     * @param mixed $cash_register
     */
    public function setCashRegister($cash_register): void
    {
        $this->cash_register = $cash_register;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }
}
