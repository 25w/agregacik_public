<?php

namespace App\Form;

use App\Entity\Offer;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('carrier', EntityType::class,
                [
                    'class'    => User::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->where('u.roles LIKE :role')
                            ->setParameter('role', "%ROLE_CARRIER%")
                            ->orderBy('u.companyName', 'ASC');
                    },
                    'choice_label' => 'companyName',
                    'placeholder' => 'Wybierz',
                    'required' => true,
                    'multiple' => false,
                ]
            )
            ->add('parent', EntityType::class,
                [
                    'class'    => Offer::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('o')
                            ->where('o.parent is NULL') // FIXME remove if want to have full offer list not only top ones.
                            ->orderBy('o.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz',
                    'required' => false,
                    'multiple' => false,
                ]
            )
            ->add(
                'image',
                FileType::class,
                [
                    'label' => 'Zdjęcie',
                    'required' => false,
                    'mapped' => false,
                    'constraints' => [
                        new File([
                            'maxSize' => '2048k',
                            'mimeTypes' => [
//                                'image/png',
                                'image/jpeg',
//                                'image/gif',
//                                'image/bmp',
//                                'image/tiff',
//                                'image/svg+xml',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid JPG image file',
                        ])
                    ],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
