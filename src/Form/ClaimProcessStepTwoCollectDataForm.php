<?php


namespace App\Form;

use App\Entity\Airline;
use App\Entity\Airport;
use App\Entity\ClaimProcess;
use App\Entity\Country;
use App\Entity\User;
use App\Entity\UserMeta;
use App\Enum\ClaimType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ClaimProcessStepTwoCollectDataForm extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'id',
                HiddenType::class,
                [
                    'data' => $options['id'],
                    'data_class' => null,
                    'mapped' => false,
                ]
            )
            ->add(
                'step',
                HiddenType::class,
                [
                    'data' => 'two',
                    'data_class' => null,
                    'mapped' => false,
                ]
            )
            ->add(
                'operation',
                HiddenType::class,
                [
                    'data' => $options['operation'],
                    'data_class' => null,
                    'mapped' => false,
                ]
            )
            ->add(
                'carrier',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'class'    => \App\Entity\User::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->where('u.type = :type')
                            ->setParameter('type', \App\Enum\UserType::CARRIER()->getValue())
                            ->orderBy('u.companyName', 'ASC')
                            ;
                    },
                    'choice_label' => 'company_name',
                    'placeholder' => 'Wybierz',
                    'required' => true,
                    'multiple' => false,
                    'label'      => 'Wybierz biuro do którego kierowany jest wniosek:',
                    'data_class' => null,
                    'mapped' => false,
                    'data' => $options['carrier'],
                ]
            )
            ->add(
                'ticket_meta_uid',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Numer identyfikacyjny biletu',
                    'required' => false,
                ]
            )
            ->add(
                'ticket_meta_passenger_name',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Imię i nazwisko na bilecie',
                    'required' => false,
                ]
            )
            ->add(
                'ticket_meta_carrier',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'class'    => Airline::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->leftJoin('a.country', 'c')
                            ->where('c.iso_code = :id')
                            ->setParameter(':id', 'PL')
                            ->andWhere('a.active = 1')
                            ->orderBy('a.name', 'ASC')
                            ;
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz',
                    'required' => false,
                    'multiple' => false,
                    'label'      => 'Przewoźnik (TODO do zmiany na input choose)'
                ]
            )
            ->add(
                'ticket_flight_no',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Numer lotu',
                    'required' => false,
                ]
            )
            ->add(
                'ticket_class',
                ChoiceType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Klasa',
                    'required' => false,
                    'placeholder' => 'Wybierz',
                    'choices' => [
                        'ekonomiczna' => '1',
                        'biznes' => '2',
                        'pierwsza' => '3',
                    ],
                ]
            )

            ->add(
                'ticket_country_from',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'class'    => Country::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC')
                            ;
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz',
                    'required' => false,
                    'multiple' => false,
                    'label'      => 'Wybierz kraj wylotu z biletu'
                ]
            )
            ->add('ticket_airport_from', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'ajax_airport_autocomplete',
                'remote_params' => ['type' => 2], // static route parameters for request->query
                'class' => Airport::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'property' => 'name',
                'placeholder' => 'Kliknij i wybierz',
                'req_params' => ['ticket_country_from' => 'parent.children[ticket_country_from]'],
                'callback'    => function (QueryBuilder $qb, $data) {
                    $qb->andWhere('e.country = :ticket_country_from');

                    $qb->andWhere('e.type = :type');
                    $qb->setParameter('type', 'airport');

                    $qb->setParameter('ticket_country_from', $data->get('ticket_country_from'));

                },
                'attr' => [
                    'class' => 'select_autocomplete'
                ],
                'label'      => 'Wybierz lotnisko wylotu z biletu'
            ])

            ->add(
                'ticket_country_to',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'class'    => Country::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC')
                            ;
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz',
                    'required' => false,
                    'multiple' => false,
                    'label'      => 'Wybierz kraj biletu do',
                ]
            )
            ->add('ticket_airport_to', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'ajax_airport_autocomplete',
                'remote_params' => ['type' => 2], // static route parameters for request->query
                'class' => Airport::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'property' => 'name',
                'placeholder' => 'Kliknij i wybierz',
                'req_params' => ['ticket_country_to' => 'parent.children[ticket_country_to]'],
                'callback'    => function (QueryBuilder $qb, $data) {
                    $qb->andWhere('e.country = :ticket_country_to');

                    $qb->andWhere('e.type = :type');
                    $qb->setParameter('type', 'airport');
                    $qb->setParameter('ticket_country_to', $data->get('ticket_country_to'));

                },
                'attr' => [
                    'class' => 'select_autocomplete'
                ],
                'label'      => 'Wybierz lotnisko docelowe z biletu'
            ])
            ->add(
                'ticket_date',
                TextType::class, // use text type for js datepicker works
//                DateType::class, // use text type for js datepicker works
                [
                    'attr' => [
                        'class' => 'form-control js-datepicker'
                    ],
                    'label' => 'Data odlotu',
                    'empty_data' => '1970-01-01',
                    'required' => false,
                ]
            )
            ->add(
                'ticket_time',
                TextType::class,
                [
                    'attr' => [
                        'class' => ''
                    ],
                    'label' => 'Godzina odlotu',
                    'empty_data' => '00:00:00',
                    'required' => false,
                ]
            )
            ->add(
                'ticket_seat',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Numer siedzenia',
                    'required' => false,
                ]
            )
            ->add(
                'ticket_gate',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Brama',
                    'required' => false,
                ]
            )
        ;
    }

    // WARNING: this is a MANDATORY block! Only options described here will be allowed to be passed.
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'id' => null,
            'carrier' => null,
            'operation' => null,
            'data_class' => ClaimProcess::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_claim_process_step_two_collect_data';
    }
}