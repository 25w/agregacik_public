<?php


namespace App\Form;

use App\Entity\Airline;
use App\Entity\Airport;
use App\Entity\ClaimProcess;
use App\Entity\Country;
use App\Entity\User;
use App\Entity\UserMeta;
use App\Enum\ClaimType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ClaimProcessStepOneValidationForm extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'id',
                HiddenType::class,
                [
                    'data' => $options['id'],
                    'data_class' => null,
                    'mapped' => false,
                ]
            )
            ->add(
                'step',
                HiddenType::class,
                [
                    'data' => 'one',
                    'data_class' => null,
                    'mapped' => false,
                ]
            )
            ->add(
                'is_readable',
                ChoiceType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Czy wniosek jest czytelny?',
                    'required' => true,
                    'placeholder' => 'Wybierz',
                    'choices' => [
                        'Tak' => 1,
                        'Nie' => 0
                    ],
                ]
            )
            ->add(
                'is_actual',
                ChoiceType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Czy wniosek jest aktualny (nie jest przedawniony)? Proszę sprawdzić datę lotu.',
                    'required' => true,
                    'placeholder' => 'Wybierz',
                    'choices' => [
                        'Tak' => 1,
                        'Nie' => 0
                    ],
                ]
            )
        ;
    }

    // WARNING: this is a MANDATORY block! Only options described here will be allowed to be passed.
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'id' => null,
            'data_class' => ClaimProcess::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_claim_process_step_one_validation';
    }
}