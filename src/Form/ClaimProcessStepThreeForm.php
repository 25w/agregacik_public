<?php


namespace App\Form;

use App\Entity\Airline;
use App\Entity\Airport;
use App\Entity\ClaimProcess;
use App\Entity\Country;
use App\Entity\User;
use App\Entity\UserMeta;
use App\Enum\ClaimType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ClaimProcessStepThreeForm extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'id',
                HiddenType::class,
                [
                    'data' => $options['id'],
                    'data_class' => null,
                    'mapped' => false,
                ]
            )
            ->add(
                'step',
                HiddenType::class,
                [
                    'data' => 'three',
                    'data_class' => null,
                    'mapped' => false,
                ]
            )

            ->add(
                'real_class',
                ChoiceType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Klasa',
                    'required' => false,
                    'placeholder' => 'Wybierz',
                    'choices' => [
                        'ekonomiczna' => '1',
                        'biznes' => '2',
                        'pierwsza' => '3',
                    ],
                ]
            )
            ->add(
                'real_country_from',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'class'    => Country::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC')
                            ;
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz',
                    'required' => false,
                    'multiple' => false,
                    'label'      => 'Wybierz kraj biletu z'
                ]
            )
            ->add('real_airport_from', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'ajax_airport_autocomplete',
                'remote_params' => ['type' => 3], // static route parameters for request->query
                'class' => Airport::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'property' => 'name',
                'placeholder' => 'Wybierz airport',
                'req_params' => ['real_country_from' => 'parent.children[real_country_from]'],
                'callback'    => function (QueryBuilder $qb, $data) {
                    $qb->andWhere('e.country = :real_country_from');
                    $qb->andWhere('e.type = :type');
                    $qb->setParameter('type', 'airport');
                    $qb->setParameter('real_country_from', $data->get('real_country_from'));
                },
                'attr' => [
                    'class' => 'select_autocomplete'
                ],
            ])

            ->add(
                'real_country_to',
                EntityType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'class'    => Country::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC')
                            ;
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'Wybierz',
                    'required' => false,
                    'multiple' => false,
                    'label'      => 'Wybierz kraj biletu do'
                ]
            )
            ->add('real_airport_to', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'ajax_airport_autocomplete',
                'remote_params' => ['type' => 3], // static route parameters for request->query
                'class' => Airport::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'property' => 'name',
                'placeholder' => 'Wybierz airport',
                'req_params' => ['real_country_to' => 'parent.children[real_country_to]'],
                'callback'    => function (QueryBuilder $qb, $data) {
                    $qb->andWhere('e.country = :real_country_to');
                    $qb->andWhere('e.type = :type');
                    $qb->setParameter('type', 'airport');
                    $qb->setParameter('real_country_to', $data->get('real_country_to'));
                },
                'attr' => [
                    'class' => 'select_autocomplete'
                ],
            ])
            ->add(
                'real_date',
                TextType::class, // use text type for js datepicker works
//                DateType::class, // use text type for js datepicker works
                [
                    'attr' => [
                        'class' => 'form-control js-datepicker'
                    ],
                    'label' => 'Data odlotu',
                    'empty_data' => '2000-01-01',
                    'required' => false,
                ]
            )
            ->add(
                'real_time',
                TextType::class,
                [
                    'attr' => [
                        'class' => ''
                    ],
                    'label' => 'Godzina odlotu',
                    'empty_data' => '00:00:00',
                    'required' => false,
                ]
            )
            ->add(
                'real_seat',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Numer siedzenia',
                    'required' => false,
                ]
            )
            ->add(
                'real_gate',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ],
                    'label' => 'Brama',
                    'required' => false,
                ]
            )

        ;

        //FIXME do better wirth getParent();
        //determine type and add proper form to this
        if ($options['operation'] === ClaimType::ODMOWA_PRZYJECIA()->getValue()) {
            $builder
                ->add(
                    'is_trip_completed',
                    ChoiceType::class,
                    [
                        'attr' => [
                            'class' => 'form-control'
                        ],
                        'label' => 'Czy dotarcie do zaplanowanego miejsca było możliwe?',
                        'required' => true,
                        'placeholder' => 'Wybierz',
                        'choices' => [
                            'Tak' => 1,
                            'Nie' => 0
                        ],
                    ]
                )
            ;
        } elseif (
            $options['operation'] === ClaimType::OPOZNIENIE()->getValue()
        ) {
            $builder
                ->add(
                    'is_delay_in_public_data',
                    ChoiceType::class,
                    [
                        'attr' => [
                            'class' => 'form-control'
                        ],
                        'label' => 'Czy wystąpiło opóźnienie z danych ogólno dostępnych?',
                        'required' => true,
                        'placeholder' => 'Wybierz',
                        'choices' => [
                            'Tak' => '1',
                            'Nie' => '0'
                        ],
                    ]
                )
                ->add(
                    'delay_from_public_data_details',
                    TextareaType::class,
                    [
                        'attr' => [
                            'class' => 'form-control'
                        ],
                        'label' => 'Proszę podać dowód opóźnienia (link/i) z danych ogólno dostępnych.',
                        'required' => false,
                    ]
                )
            ;
        } elseif (
            $options['operation'] === ClaimType::ODWOLANIE()->getValue()
        ) {
            $builder
                ->add(
                    'when_trip_was_canceled_date',
                    TextType::class, // use text type for js datepicker works
//                DateType::class, // use text type for js datepicker works
                    [
                        'attr' => [
                            'class' => 'form-control js-datepicker'
                        ],
                        'label' => 'Kiedy odwolano lot?',
                        'empty_data' => '2000-01-01',
                        'required' => false,

                        //FIXME move to entity!!!
                        'data_class' => null,
                        'mapped' => false,
                    ]
                )
            ;
        }

    }

    // WARNING: this is a MANDATORY block! Only options described here will be allowed to be passed.
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'id' => null,
            'operation' => null,
            'data_class' => ClaimProcess::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_claim_process_step_three';
    }

//    /**
//     * Extends actual form with parent form
//     * @return string|null
//     */
//    public function getParent()
//    {
////        if ($operation === ClaimType::ODMOWA_PRZYJECIA()->getValue()) {
////            $formType = ClaimVerificationFormOdmowa::class;
////        } elseif ($operation === ClaimType::OPOZNIENIE()->getValue()) {
////            $formType = ClaimVerificationFormOpoznienie::class;
////        } elseif ($operation === ClaimType::ODWOLANIE()->getValue()) {
////            $formType = ClaimVerificationFormOdwolanie::class;
////        }
//
////        return ClaimVerificationForm::class;
//    }
}