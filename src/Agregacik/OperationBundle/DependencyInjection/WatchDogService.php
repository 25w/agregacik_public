<?php

namespace App\Agregacik\OperationBundle\DependencyInjection;

use App\Agregacik\OperationBundle\Factory\CashRegistry;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use App\Entity\CashRegister;
use App\Entity\CrVolume;
use App\Entity\WatchDog;
use App\Repository\CashRegisterRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WatchDogService
 * @package App\Agregacik\OperationBundle\DependencyInjection
 */
class WatchDogService
{
    private LoggerInterface $logger;
    private $em;

    /**
     * MakeOperationService constructor.
     * @param LoggerInterface $logger
     * @param ContainerInterface $container
     * @throws \Exception
     */
    public function __construct(
        LoggerInterface $logger,
        ContainerInterface $container
    )
    {
        $this->logger = $logger;
        $this->em = $container->get('doctrine')->getManager();
    }

    public function log(string $akcja, string $co, string $gdzie, string $zmiana_z, string $zmiana_na, string $kto)
    {
        $wd = new WatchDog();
        $wd->setAkcja($akcja);
        $wd->setCo($co);
        $wd->setGdzie($gdzie);
        $wd->setZmianaZ($zmiana_z);
        $wd->setZmianaNa($zmiana_na);
        $wd->setKto($kto);

        $this->em->persist($wd);
        $this->em->flush();

    }

}
