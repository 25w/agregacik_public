<?php

namespace App\Agregacik\OperationBundle;

use App\Agregacik\OperationBundle\Strategy\Message;
use App\Agregacik\OperationBundle\Strategy\Operation;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AgregacikOperationBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container): void
    {
        $container->registerForAutoconfiguration(Message\StrategyInterface::class)
            ->addTag(Message\StrategyInterface::SERVICE_TAG)
        ;
        $container->addCompilerPass(new Message\MessageCompilerPass());

        $container->registerForAutoconfiguration(Operation\StrategyInterface::class)
            ->addTag(Operation\StrategyInterface::SERVICE_TAG)
        ;
        $container->addCompilerPass(new Operation\OperationCompilerPass());
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return null;
    }
}
