<?php


namespace App\Agregacik\OperationBundle\Enum;

use MyCLabs\Enum\Enum;

/**
 * Class Currency
 *
 * https://github.com/myclabs/php-enum
 *
 * @method static self POLAND()
 * @method static self EURO()
 * @method static self DOLLAR()
 */
class Currency extends Enum
{
    private const POLAND    = 'PLN';
    private const EURO      = 'EUR';
    private const DOLLAR    = 'USD';
}