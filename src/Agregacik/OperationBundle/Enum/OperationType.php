<?php


namespace App\Agregacik\OperationBundle\Enum;

use MyCLabs\Enum\Enum;


/**
 * Class OperationType
 *
 * https://github.com/myclabs/php-enum
 *
 * @method static self CARRIER_ADD_COIN_TO_POOL()
 * @method static self ADMIN_ADD_COIN_TO_WORKER()
 * @method static self USER_CLAIM_VERIFICATION_BY_WORKER()
 * @method static self CARRIER_CLAIM_AUTHENTICATION()
 * @method static self USER_PURCHASE_OFFER()
 *
 * @method static self MOVE_COIN_FROM_TO()
 *
 * @method static self OPERATION_NO_COIN_CHANGE()
 * @method static self OPERATION_INCREASE_COIN()
 * @method static self OPERATION_DECREASE_COIN()
 *
 * @method static self OPERATION_STATUS_NO_CHANGE()
 * @method static self OPERATION_STATUS_SERVED()
 * @method static self OPERATION_STATUS_REJECTED()
 */
class OperationType extends Enum
{
    private const CARRIER_ADD_COIN_TO_POOL      = 1;
    private const ADMIN_ADD_COIN_TO_WORKER      = 2;
    private const USER_CLAIM_VERIFICATION_BY_WORKER   = 3;
    private const CARRIER_CLAIM_AUTHENTICATION   = 4;
    private const USER_PURCHASE_OFFER           = 5;

    private const MOVE_COIN_FROM_TO           = 11;

    private const OPERATION_NO_COIN_CHANGE      = 1;
    private const OPERATION_INCREASE_COIN       = 2;
    private const OPERATION_DECREASE_COIN       = 3;

    private const OPERATION_STATUS_NO_CHANGE      = 1;
    private const OPERATION_STATUS_SERVED       = 2;
    private const OPERATION_STATUS_REJECTED       = 3;
}
