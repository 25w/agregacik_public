<?php


namespace App\Agregacik\OperationBundle\Validation;


interface ValidationInterface
{
    public function check(array $payload, array $required_keys): bool;
}