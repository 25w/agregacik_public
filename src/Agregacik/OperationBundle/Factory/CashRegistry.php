<?php


namespace App\Agregacik\OperationBundle\Factory;


use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Exceptions\OperationException;
use App\Entity\CashRegister;
use App\Entity\CrCurrency;
use App\Entity\CrVolume;
use App\Entity\User;
use App\Repository\CashRegisterRepository;
use Doctrine\ORM\EntityManager;

class CashRegistry
{
    private CashRegister $userCashRegister;
    private EntityManager $em;
    private int $user_id;

    public function __construct(EntityManager $em, $user_id)
    {
        $this->user_id = $user_id;
        $this->em = $em;

        // get cash registry from user_id
        // get repository of cash register by entity manager
        $crRepo = $em->getRepository(CashRegister::class)->findOneBy(['userId' => $user_id]);
        $this->userCashRegister = $this->getCashRegisterEntity($crRepo);

        return $this;
    }

    private function getCashRegisterEntity($potentialEntity = '')
    {
        if (isset($this->userCashRegister)) {
            return $this->userCashRegister;
        } elseif ($potentialEntity instanceof CashRegister) {
            return $potentialEntity;
        } else {
            $cr = new CashRegister();
            $cr->setUserId($this->em->getRepository(User::class)->findOneBy([ 'id' => $this->user_id]));
            return $cr;
        }

    }

    /**
     * @param int $value
     * @param string $currency
     * @param int $operation
     * @throws OperationException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createBill(int $value, string $currency, int $operation)
    {

        //check if user has coins
//        $actual_coins = (int) $this->getActualCoins();
//        if ($actual_coins < $value) {
//            //TODO throw exception
//            throw new OperationException('Za malo coins '.$actual_coins.' < '.$value.' !');
//        }

        $crVolume = new CrVolume();
        $crVolume->setWorth($value);
        $crVolume->setType($operation);
        $crVolume->setCrCurrency($this->getCurrency($currency));
        $crVolume->setCashRegister($this->userCashRegister);

        return $crVolume;
//        $this->em->persist($crVolume);
//        $this->em->flush();
    }

    public function getCurrency($currency_name)
    {

        return $this->em->getRepository(CrCurrency::class)->findOneBy(['name' => $currency_name]);
    }

    public function getActualCoins()
    {

        $userCoinsCount = $this->em->getRepository(CrVolume::class)->getUserCoinsCount($this->user_id);
        ///???????????????????????????
//var_dump( // FIXME TODO $this->userCashRegister->getUserCoinCount());
//        /** @var CashRegisterRepository $crRepo */
//        $crRepo = $this->em->getRepository(CashRegister::class);
//        var_dump($crRepo->getUserCashRegister($this->user_id));
        return $userCoinsCount[0]['coins'];
    }

//    public function getUserCashRegister()
//    {
//        return $this->userCashRegister;
//    }
}