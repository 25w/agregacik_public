<?php


namespace App\Agregacik\OperationBundle\Strategy\Message;


class Message
{
    private $strategies;

    public function addStrategy(StrategyInterface $strategy): void
    {
        $this->strategies[] = $strategy;
    }

    public function send(string $type, iterable $payload = []): void
    {
        /** @var StrategyInterface $strategy */
        foreach ($this->strategies as $strategy) {
            if ($strategy->isSendable($type, $payload)) {
                $strategy->send($payload);
                return;
            }
        }
    }
}