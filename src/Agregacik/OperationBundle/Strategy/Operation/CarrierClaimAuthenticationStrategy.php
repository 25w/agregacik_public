<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use App\Agregacik\OperationBundle\DependencyInjection\WatchDogService;
use App\Agregacik\OperationBundle\Enum\Currency;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Exceptions\OperationException;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use App\Entity\Claim;
use App\Entity\User;
use App\Enum\ClaimType;
use App\Repository\ClaimRepository;
use App\Strategy\ClaimVerification\ClaimVerification;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CarrierClaimAuthenticationStrategy extends OperationCommon  implements StrategyInterface
{

    private CashRegistryFactory $cashRegistryFactory;
    private HasRequiredKeys $hasRequiredKeys;
    private $em;
    protected AuthorizationCheckerInterface $authorizationChecker;
    private WatchDogService $wds;
    private ClaimVerification $claimVerification;
    private array $payload;
    /**
     * @var Request
     */
    private Request $request;
    private ContainerInterface $c;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        CashRegistryFactory $cashRegistryFactory,
        ContainerInterface $container,
        HasRequiredKeys $hasRequiredKeys,
        WatchDogService $wds,
        ClaimVerification $claimVerification
    )
    {
        $this->cashRegistryFactory = $cashRegistryFactory;
        $this->authorizationChecker = $authorizationChecker;
        $this->wds = $wds;
        $this->c = $container;
        $this->em = $container->get('doctrine')->getManager();
        $this->claimVerification = $claimVerification;
        parent::__construct($hasRequiredKeys);
    }

    /***
     * @param OperationType $type
     * @param Request $request
     * @return bool
     */
    public function isRunable(OperationType $type, Request $request): bool
    {
        $is_operation_selected = $type->getValue() === OperationType::CARRIER_CLAIM_AUTHENTICATION()->getValue();
        $has_granted_access = $this->authorizationChecker->isGranted('ROLE_CARRIER');
        $has_payload = !empty($request->get(self::MESSAGE_KEY . '_data'));
        $has_keys = false;
        if ($has_payload) {
            $this->payload = $this->preparePayload($request, [
                'claim_id'
            ]);
            $has_keys = $this->hasKeys($this->payload, [ 'claim_id' ]);
        }
        $validation_result = $is_operation_selected && $has_granted_access && $has_payload && $has_keys;

        //TODO check if claim is already verified??
        if ($validation_result) {
            $this->request = $request;
        }

        $this->wds->log(OperationType::CARRIER_CLAIM_AUTHENTICATION()->getKey(),
            'RUN', 'CARRIER_CLAIM_AUTHENTICATION:isRunable', '', 'TRUE', 'kto odpala? TODO RELACJA');

        return $validation_result;
    }

    public function run(): void
    {


        /** @var ClaimRepository $repo */
        $repo = $this->em->getRepository(Claim::class);

        $claim = $repo->findOneBy(['id' => $this->payload['claim_id']]);

        $carrier_public_key = $claim->getCarrier()->getPublicKey();
        $carrier_private_key = $claim->getCarrier()->getPrivateKey();

//        $filemd5 = md5_file(__DIR__ . '/../../public/demo_plik.pdf');

// TODO get user (biuro) (actual logged userid) private and public key
//        $publicprivatekey->signFile($filemd5, $carrier_public_key, $carrier_private_key);

        if (true) {
            $claim->setIsAuthenticated();

            //TODO add also worth of claim to user
            /** @var User $claimAuthor */
            $claimAuthor = $claim->getAuthor();
            /** @var User $claimCarrier */
            $claimCarrier = $claim->getCarrier();

            $claimCurrency = Currency::POLAND()->getValue();
            $claimWorth = $claim->getWorthValue();

            //move coins from carrier to client
            $carrierCashRegistry = $this->cashRegistryFactory->createFromUser($this->em, $claimCarrier->getId());
            $userCashRegistry = $this->cashRegistryFactory->createFromUser($this->em, $claimAuthor->getId());


            //get from admin account to worker account (later worker and admin can use same pool)
            try {
                $cashRegistryBill = $carrierCashRegistry->createBill($claimWorth, $claimCurrency, OperationType::OPERATION_DECREASE_COIN()->getValue());
//                $userCashRegistry->createBill($claimWorth, $claimCurrency, OperationType::OPERATION_INCREASE_COIN()->getValue());
            } catch (OperationException $e) {
                var_dump($e->getMessage()); die();
                //TODO log exception
                $this->c->get('session')->getFlashBag()->add('error', $e->getMessage());
            }


            $this->em->persist($cashRegistryBill);
            $this->em->persist($claim);
            $this->em->flush();
        }

//        var_dump('public_key;', $public_key);
//        echo "<br>";
//        var_dump('private_key;', $private_key);
//
//        var_dump('COUNT', strlen($public_key),strlen($private_key));



    }
}