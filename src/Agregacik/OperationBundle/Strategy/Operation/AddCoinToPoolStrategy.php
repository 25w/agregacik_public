<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use App\Agregacik\OperationBundle\DependencyInjection\WatchDogService;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Exceptions\OperationException;
use App\Agregacik\OperationBundle\Exceptions\OperationInputException;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AddCoinToPoolStrategy extends OperationCommon  implements StrategyInterface
{

    private CashRegistryFactory $cashRegistryFactory;
    private $em;
    protected AuthorizationCheckerInterface $authorizationChecker;
    private WatchDogService $wds;
    private array $payload;
    /**
     * @var Request
     */
    private Request $request;

    private $c;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        CashRegistryFactory $cashRegistryFactory,
        ContainerInterface $container,
        WatchDogService $wds
    )
    {
        $this->cashRegistryFactory = $cashRegistryFactory;
        $this->authorizationChecker = $authorizationChecker;
        $this->wds = $wds;
        $this->c = $container;
        $this->em = $container->get('doctrine')->getManager();
    }

    /***
     * @param OperationType $type
     * @param Request $request
     * @return bool
     */
    public function isRunable(OperationType $type, Request $request): bool
    {
        $is_operation_selected = $type->getValue() === OperationType::CARRIER_ADD_COIN_TO_POOL()->getValue();
        $has_granted_access = $this->authorizationChecker->isGranted('ROLE_WORKER');
        $has_payload = !empty($request->get(self::MESSAGE_KEY . '_data'));
        $has_keys = false;
        if ($has_payload) {
            $this->payload = $this->preparePayload($request, ['user_id', 'value', 'currency']);
            $has_keys = $this->hasKeys($this->payload, [ 'user_id', 'value' ]);
        }
        $validation_result = $is_operation_selected && $has_granted_access && $has_payload && $has_keys;

        if ($validation_result) {
            $this->request = $request;
        }

        $this->wds->log(OperationType::CARRIER_ADD_COIN_TO_POOL()->getKey(),
            'RUN', 'AddCoinToPoolStrategy:isRunable', '', 'TRUE', 'kto odpala? TODO RELACJA');

        return $validation_result;
    }

    public function run(): void
    {

        $userCashRegistry = $this->cashRegistryFactory->createFromUser($this->em, $this->payload['user_id']);

        try {
            $userCashRegistry->createBill($this->payload['value'], $this->payload['currency'], OperationType::OPERATION_INCREASE_COIN()->getValue());
        } catch (OperationException $e) {
            //TODO log exception
            $this->c->get('session')->getFlashBag()->add('error', $e->getMessage());
        }

        $this->wds->log(OperationType::CARRIER_ADD_COIN_TO_POOL()->getKey(),
            'RUN', 'AddCoinToPoolStrategy:run', '0', $this->payload['value'], 'kto odpala? TODO RELACJA');

    }
}