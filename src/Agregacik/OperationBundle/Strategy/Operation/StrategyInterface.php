<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use App\Agregacik\OperationBundle\Enum\OperationType;
use Symfony\Component\HttpFoundation\Request;

interface StrategyInterface
{
    public const SERVICE_TAG = 'operation_strategy';
    public const MESSAGE_KEY = 'operation';

    public function isRunable(OperationType $type, Request $request): bool;
    public function run(): void;
}