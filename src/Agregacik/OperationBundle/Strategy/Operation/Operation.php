<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use App\Agregacik\OperationBundle\Enum\OperationType;
use Symfony\Component\HttpFoundation\Request;

class Operation
{
    private $strategies;

    public function addStrategy(StrategyInterface $strategy): void
    {
        $this->strategies[] = $strategy;
    }

    public function run(OperationType $type, Request $request): void
    {
        /** @var StrategyInterface $strategy */
        foreach ($this->strategies as $strategy) {
            if ($strategy->isRunable($type, $request)) {
                $strategy->run($request);
                return;
            }
        }
    }
}