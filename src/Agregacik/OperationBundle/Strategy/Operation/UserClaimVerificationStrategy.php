<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use App\Agregacik\OperationBundle\DependencyInjection\WatchDogService;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use App\Entity\Claim;
use App\Enum\ClaimType;
use App\Strategy\ClaimVerification\ClaimVerification;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserClaimVerificationStrategy extends OperationCommon  implements StrategyInterface
{

    private CashRegistryFactory $cashRegistryFactory;
    private HasRequiredKeys $hasRequiredKeys;
    private $em;
    protected AuthorizationCheckerInterface $authorizationChecker;
    private WatchDogService $wds;
    private ClaimVerification $claimVerification;
    private array $payload;
    /**
     * @var Request
     */
    private Request $request;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        CashRegistryFactory $cashRegistryFactory,
        ContainerInterface $container,
        HasRequiredKeys $hasRequiredKeys,
        WatchDogService $wds,
        ClaimVerification $claimVerification
    )
    {
        $this->cashRegistryFactory = $cashRegistryFactory;
        $this->authorizationChecker = $authorizationChecker;
        $this->wds = $wds;
        $this->em = $container->get('doctrine')->getManager();
        $this->claimVerification = $claimVerification;
        parent::__construct($hasRequiredKeys);
    }

    /***
     * @param OperationType $type
     * @param Request $request
     * @return bool
     */
    public function isRunable(OperationType $type, Request $request): bool
    {
        $is_operation_selected = $type->getValue() === OperationType::USER_CLAIM_VERIFICATION_BY_WORKER()->getValue();
        $has_granted_access = $this->authorizationChecker->isGranted('ROLE_WORKER');
        $has_payload = !empty($request->get(self::MESSAGE_KEY . '_data'));
        $has_keys = false;
        if ($has_payload) {
            $this->payload = $this->preparePayload($request, [
                'id', 'claim_type', 'carrier_id',
                'logic_question_is_item_readable', 'logic_question_is_item_actual', 'logic_question_three', 'logic_question_was_delay_from_public_data', 'logic_question_was_trip_completed',
                'question_ticket_meta_uid', 'question_ticket_meta_passenger_name', 'question_ticket_meta_carrier',
                'question_ticket_flight_no', 'question_ticket_class', 'question_ticket_from', 'question_ticket_to',
                'question_ticket_date', 'question_ticket_time', 'question_ticket_seat', 'question_ticket_gate',

                'question_ask_distance_between_points', 'question_ask_real_flight_from_date', 'question_ask_real_flight_from_time',
                'question_ask_real_flight_destination_date', 'question_ask_real_flight_destination_time',
                'question_ask_real_flight_from', 'question_ask_real_flight_to', 'question_ask_real_class',
            ]);
            $has_keys = $this->hasKeys($this->payload, [ 'id', 'claim_type', 'carrier_id', 'logic_question_is_item_readable', 'logic_question_is_item_actual', 'logic_question_three', 'logic_question_was_delay_from_public_data', 'logic_question_was_trip_completed' ]);
        }
        $validation_result = $is_operation_selected && $has_granted_access && $has_payload && $has_keys;

        //TODO check if claim is already verified??

        if ($validation_result) {
            $this->request = $request;
        }

        $this->wds->log(OperationType::USER_CLAIM_VERIFICATION_BY_WORKER()->getKey(),
            'RUN', 'USER_CLAIM_VERIFICATION_BY_WORKER:isRunable', '', 'TRUE', 'kto odpala? TODO RELACJA');

        return $validation_result;
    }

    public function run(): void
    {
        // FIXME DO IT OTHER WAY
        switch ($this->payload['claim_type']) {
            case ClaimType::ODMOWA_PRZYJECIA()->getValue():

                // przekazanie do biura do "akceptacji" inaczej pisząc - przekazanie informacji.

                $this->claimVerification->run(ClaimType::ODMOWA_PRZYJECIA(), $this->payload);

                break;
            case ClaimType::OPOZNIENIE()->getValue():

                // logic_question_is_item_readable - DLA UPEWNIENIA czy mamy dobre dane
                // logic_question_is_item_actual - DLA UPEWNIENIA czy mamy dobre dane
                // logic_question_three - docelowe pytanie - TAK konczy, NIE kontynuuje
                // logic_question_was_trip_completed - docelowe pytanie - TAK konczy, NIE kontynuuje
                // question_ask_distance_between_points - pytanie - TAK odwołanie do kontaktu z klientem, NIE konczy
                //koniec

                $this->claimVerification->run(ClaimType::OPOZNIENIE(), $this->payload);

                break;
            case ClaimType::ODWOLANIE()->getValue():

                //TODO zrób coś co bedzie pokazywac pola formularza jak i obslugiwac payload send.

                //przetwarzanie kluczy : (front zadba o to aby tutaj sie znalazły wymagane pola)

                // logic_question_is_item_readable - DLA UPEWNIENIA czy mamy dobre dane
                // logic_question_is_item_actual - DLA UPEWNIENIA czy mamy dobre dane
                // logic_question_three - docelowe pytanie - TAK konczy, NIE odwołanie do kontaktu z klientem
                //koniec

                $this->claimVerification->run(ClaimType::ODWOLANIE(), $this->payload);

                break;

            default:
                // no action
                break;


            //TODO AFTER all make:
            // dodatkowe dane zbierz - czyli seria pytan o np dane z biletu itd
            // wygeneruj PDF zalezny od typu wniosku, uzupełnij i wyślij do biura DO AUTENTYKACJI
        }

    }
}