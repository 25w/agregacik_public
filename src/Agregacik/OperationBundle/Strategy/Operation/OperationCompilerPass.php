<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class OperationCompilerPass implements CompilerPassInterface
{

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {

        // always first check if the primary service is defined
        if (!$container->has(Operation::class)) {
            return;
        }

        $resolverService = $container->findDefinition(Operation::class);

        $strategyServices = array_keys($container->findTaggedServiceIds(StrategyInterface::SERVICE_TAG));

        foreach ($strategyServices as $strategyService) {
            $resolverService->addMethodCall('addStrategy', [new Reference($strategyService)]);
        }
    }
}