<?php


namespace App\Agregacik\OperationBundle\Strategy\Operation;


use App\Agregacik\OperationBundle\Validation\HasRequiredKeys;
use Symfony\Component\HttpFoundation\Request;

class OperationCommon
{
    private HasRequiredKeys $hasRequiredKeys;

    public function __construct(
        HasRequiredKeys $hasRequiredKeys
    )
    {
        $this->hasRequiredKeys = $hasRequiredKeys;
    }
    /**
     * @param Request $request
     * @param array $params
     * @return array
     */
    protected function preparePayload(Request $request, $params = []): array
    {
        $tempArray = [];
        foreach ($params as $varName) {
//            var_dump($request->get('operation_data')); die('d');
            // need to send empty string for validation rather than NULL
            if (empty($request->get('operation_data')[0][$varName]) && empty($request->get('operation_data')[$varName])) {
                $tempValue = null;
            } else if (is_array($request->get('operation_data')) && count($request->get('operation_data')) == 1 ) {
                $tempValue = $request->get('operation_data')[0][$varName];
            } else {
                $tempValue = $request->get('operation_data')[$varName];
            }

            $tempArray[$varName] = is_null($tempValue) ? '' : $tempValue;
        }
        return $tempArray;
    }

    /**
     * @param $payload
     * @param $vars
     * @return bool
     */
    protected function hasKeys($payload, $vars): bool
    {
        foreach ($vars as $var) {
            if (!array_key_exists($var, $payload)) {
                return false;
            }
        }
        return true;
//        return $this->hasRequiredKeys->check($payload, $vars);
    }
}