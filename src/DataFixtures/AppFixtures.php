<?php


namespace App\DataFixtures;


use App\Entity\ApiUser;
use App\Entity\Claim;
use App\Entity\File;
use App\Entity\User;
use App\Entity\UserMeta;
use App\Enum\UserType;
use App\Service\PublicPrivateKeyService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Generator;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    /** @var Generator */
    protected $faker;

    private $encoder;

    private PublicPrivateKeyService $publicPrivateKeyService;

//    abstract protected function loadData(ObjectManager $manager);

    public function __construct(UserPasswordEncoderInterface $encoder,  PublicPrivateKeyService $publicPrivateKeyService)
    {
        $this->encoder = $encoder;
        $this->publicPrivateKeyService = $publicPrivateKeyService;

    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();

        $this->loadDefaultUsers($manager);
        // normally we first add claim and next after worker validation we made account if is not already created (check by emial)
        // but here we first make users so we have them set and next make claims and links carriers and users to it
        $this->loadUsersData($manager);
        $this->loadClaimData($manager);
    }

    public function loadDefaultUsers(ObjectManager $manager)
    {
        $demoUsers = [
            [
                'username' => 'crxadmin',
                'name' => 'CRX ADMIN',
                'email' => 'admin@25w.pl',
                'password' => 'admin',
                'role' => 'ROLE_ADMIN',
                'active' => true,
                'type' => UserType::ADMIN()->getValue(),
            ],
            [
                'username' => 'crxklient',
                'name' => 'CRX klient',
                'email' => 'klient@25w.pl',
                'password' => 'klient',
                'role' => 'ROLE_USER',
                'active' => true,
                'type' => UserType::USER()->getValue(),
            ],
            [
                'username' => 'crxbiuro',
                'name' => 'CRX biuro',
                'email' => 'biuro@25w.pl',
                'password' => 'biuro',
                'role' => 'ROLE_CARRIER', // biuro, przewoznik etc
                'active' => true,
                'company_name' => 'np biuro LOT',
                'meta' => [
                    'is_currier' => 1,
                    'address' => 'adres biura'
                ],
                'type' => UserType::CARRIER()->getValue(),
            ],
            [
                'username' => 'crxbiuro2',
                'name' => 'CRX biuro2',
                'email' => 'biuro2@25w.pl',
                'password' => 'biuro2',
                'role' => 'ROLE_CARRIER', // biuro, przewoznik etc
                'active' => true,
                'company_name' => 'np biuro LOT2',
                'private_key' => '',
                'public_key' => '',
                'meta' => [
                    'is_currier' => 1,
                    'address' => 'adres biura2'
                ],
                'type' => UserType::CARRIER()->getValue(),
            ],
            [
                'username' => 'crxpracownik',
                'name' => 'CRX pracownik',
                'email' => 'pracownik@25w.pl',
                'password' => 'pracownik',
                'role' => 'ROLE_WORKER', // pracownik aplikacji
                'active' => true,
                'type' => UserType::WORKER()->getValue(),
            ],
        ];

        $apiUsers = [
            [
                'username' => 'crxapi',
                'name' => 'CRX API',
                'email' => 'apiuser@25w.pl',
                'password' => 'apiuser',
                'role' => 'ROLE_API',
                'active' => true
            ],
        ];

        if (empty($demoUsers)) {
            echo 'No data';
            return 1;
        }


        foreach ($demoUsers as $demoUser) {

            if (! $demoUser['active']) {
                continue;
            }

            $user = $manager->getRepository(User::class)->findOneBy(['login' => $demoUser['username']]);
            if ($user === null) {
                $user = new User();
            }
            $user->setLogin($demoUser['username']);
            $user->setPassword($this->encoder->encodePassword($user, $demoUser['password']));
            $user->setFirstName($demoUser['name']);
            $user->setEmail($demoUser['email']);
            $user->setRoles([$demoUser['role']]);
            $user->setType($demoUser['type']);
            $user->setActive(true); // set all demo data to be active
            $user->setLastLogin(new \DateTime(date('Y-m-d H:i:s')));

            if ($demoUser['role'] == 'ROLE_CARRIER') {
                foreach ($demoUser['meta'] as $meta_key => $meta_value) {
                    $metaEnt = new UserMeta();
                    $metaEnt->setName($meta_key);
                    $metaEnt->setValue($meta_value);
                    $user->addUserMeta($metaEnt);
                }
                $user->setCompanyName($demoUser['company_name']);

                if (empty($user->getPublicKey())) {
//                    /** @var PublicPrivateKeyService $publicprivatekey */
//                    $publicprivatekey = $this->container->get('app.service.publicprivatekey');
                    list($public_key, $private_key) = $this->publicPrivateKeyService->generateBothKeyPair();

                    $user->setPublicKey($public_key);
                    $user->setPrivateKey($private_key);
                }
            }

            $manager->persist($user);
            echo('User ... '.$demoUser['username'].' added.')."\n";
        }
        foreach ($apiUsers as $apiUser) {

            if (! $apiUser['active']) {
                continue;
            }

            $api = $manager->getRepository(ApiUser::class)->findOneBy(['username' => $apiUser['username']]);
            if ($api === null) {
                $api = new ApiUser();
            }
            $api->setUsername($apiUser['username']);
            $api->setPassword($apiUser['password']);
            $api->setEmail($apiUser['email']);
            $api->setApiSecret();
            $api->setRoles([$apiUser['role']]);

            $manager->persist($api);
            echo('API User ... '.$apiUser['username'].' added.')."\n";
        }
        $manager->flush();

    }

    public function loadClaimData(ObjectManager $manager)
    {
        $curriers = $manager->getRepository(User::class)->getIdListByType(UserType::CARRIER()->getValue());
        $users = $manager->getRepository(User::class)->getIdListByType(UserType::USER()->getValue());

        for ($i = 0; $i < 40; $i++) {
            $claim = new Claim();

            // both need to be set after we got users
            $claim->setCarrier($manager->getRepository(User::class)->find($this->faker->randomElement($curriers)));
            $claim->setAuthor($manager->getRepository(User::class)->find($this->faker->randomElement($users)));

            $claim->setDateAdded($this->faker->dateTimeBetween('-30 days', 'now'));
            $claim->setType($this->faker->biasedNumberBetween(1,3));

            $fileEntity = new File();
            $fileEntity->setName('testing_img.jpg');
            $fileEntity->setPath('/testing_img.jpg');
            $fileEntity->setClaim($claim);

            $claim->addFile($fileEntity);

            $manager->persist($fileEntity);

            echo('Claim nr. '.$i.' added.')."\n";
            $manager->persist($claim);
        }

        $manager->flush();
    }

    public function loadUsersData(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {

            $user = new User();
            $userType = $this->faker->randomElement(UserType::toArrayWithoutAdmin());
            $role = 'ROLE_' . UserType::search($userType);

            $user->setLogin($this->faker->userName);
            $user->setPassword($this->encoder->encodePassword($user, 'test'));
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setEmail($this->faker->email);
            $user->setRoles([$role]);
            $user->setType($userType);
            $user->setActive(true); // set all demo data to be active
            $user->setLastLogin(new \DateTime(date('Y-m-d H:i:s')));

            if ($role == 'ROLE_CARRIER') {

                for ($j = 0; $j < 3; $j++) {
                    $userMeta = new UserMeta();
                    $userMeta->setName($this->faker->randomElement([1,2,3,4,5,6]));
                    $userMeta->setValue($this->faker->randomElement([1,2,3,4,5,6]));
                    $manager->persist($userMeta);
                    $user->addUserMeta($userMeta);
                }

                $user->setCompanyName($this->faker->company);

                if (empty($user->getPublicKey())) {
//                    /** @var PublicPrivateKeyService $publicprivatekey */
//                    $publicprivatekey = $this->container->get('app.service.publicprivatekey');
                    list($public_key, $private_key) = $this->publicPrivateKeyService->generateBothKeyPair();

                    $user->setPublicKey($public_key);
                    $user->setPrivateKey($private_key);
                }
            }

            echo('User '.$userType.' with login '.$user->getLogin().' added.')."\n";
            $manager->persist($user);
        }
        $manager->flush();
    }

    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 0; $i < $count; $i++) {
            $entity = new $className();
            $factory($entity, $i);
            $this->manager->persist($entity);
            // store for usage later as App\Entity\ClassName_#COUNT#
            $this->addReference($className . '_' . $i, $entity);
        }
    }
}