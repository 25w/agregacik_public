<?php

namespace App\Algorithm;


/**
 * Class DiffieHellman
 * @author Hamza ESSAYEGH <hamza.essayegh@protonmail.com>
 */
class DiffieHellman
{
    const PREDEFINED_1536 = 0;
    const PREDEFINED_3072 = 1;
    const PREDEFINED_4096 = 2;
    const PREDEFINED_6144 = 3;
    const PREDEFINED_8192 = 4;

    const PRIVATE_KEY_LENGTH = 1024;

    /**
     * @var \GMP
     */
    private $modulus;

    /**
     * @var \GMP
     */
    private $base;

    /**
     * @var \GMP
     */
    private $public;

    /**
     * @var
     */
    private $private;

    /**
     * @var \GMP
     */
    private $secret;

    /**
     * DiffieHellman constructor.
     *
     * @param \GMP $modulus
     * @param \GMP $base
     * @param int  $predefined
     *
     * @throws \Exception
     */
    public function __construct($predefined = null, $modulus = null, $base = null)
    {
        if (null === $modulus && null === $base) {
            switch ($predefined) {
                case self::PREDEFINED_1536:
                    list(
                        $this->modulus,
                        $this->base
                        ) = $this->dh_predefined_1536();
                    break;
                case self::PREDEFINED_3072:
                    list(
                        $this->modulus,
                        $this->base
                        ) = $this->dh_predefined_3072();
                    break;
                case self::PREDEFINED_4096:
                    list(
                        $this->modulus,
                        $this->base
                        ) = $this->dh_predefined_4096();
                    break;
                case self::PREDEFINED_6144:
                    list(
                        $this->modulus,
                        $this->base
                        ) = $this->dh_predefined_6144();
                    break;
                case self::PREDEFINED_8192:
                    list(
                        $this->modulus,
                        $this->base
                        ) = $this->dh_predefined_8192();
                    break;
                case null:
                    list(
                        $this->modulus,
                        $this->base
                        ) = $this->dh_predefined_1536();
                    break;
                default:
                    throw new \InvalidArgumentException("Invalid value for predefined (value=`$predefined`)");
            }
        } else if (null !== $modulus && null !== $base) {
            if (!($modulus instanceof \GMP)) {
                throw new \InvalidArgumentException("Modulus must be an instance of GMP");
            } else if (!($base instanceof \GMP)) {
                throw new \InvalidArgumentException("Base must be an instance of GMP");
            }

            $this->modulus = $modulus;
            $this->base    = $base;
        } else {
            throw new \Exception("Invalid values for the modulus and the base");
        }
    }

    /**
     * Initialize the DH process by generating the public and private key
     */
    public function init($private_key = '')
    {
        if (empty($private_key)) {
            // generate private key
            $this->generate_private_key();
        } else {
            $this->private = $private_key;
        }
//var_dump($this->private);
        // compute public key
        $this->compute_public();
    }

    /**
     * Generate the private key for the built object
     */
    public function generate_private_key()
    {
        // generating private key
        $this->private = gmp_random_bits(self::PRIVATE_KEY_LENGTH);
    }

    /**
     * Compute the public for the built object
     */
    public function compute_public()
    {
        $this->public = gmp_powm($this->base, $this->private, $this->modulus);
    }

    /**
     * Compute the secret with the given public key
     *
     * @param \GMP $public
     */
    public function compute_secret(\GMP $public = null)
    {
        if (null !== $public) {
            $this->secret = gmp_powm($public, $this->private, $this->modulus);
        } else {
            $this->secret = gmp_powm($this->public, $this->private, $this->modulus);
        }
    }

    /**
     * @return \GMP
     */
    public function getModulus()
    {
        return $this->modulus;
    }

    /**
     * @param \GMP $modulus
     *
     * @return DiffieHellman
     */
    public function setModulus(\GMP $modulus)
    {
        $this->modulus = $modulus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param \GMP $base
     *
     * @return DiffieHellman
     */
    public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * @param mixed $public
     *
     * @return DiffieHellman
     */
    public function setPublic($public)
    {
        $this->public = $public;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     *
     * @return DiffieHellman
     */
    public function setPrivate($private)
    {
        $this->private = $private;
        return $this;
    }

    /**
     * @return \GMP
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param \GMP $secret
     *
     * @return DiffieHellman
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }


    /**
     * List of functions that returns predefined g and p,
     * according to the RFC 3256
     *
     * @author Hamza ESSAYEGH <hamza.essayegh@protonmail.com>
     */

    /**
     * RFC 3256 - 1536bits MODP
     *
     * @return \GMP[]
     */
    private function dh_predefined_1536()
    {
        $predefined_p = gmp_init("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF");
        $predefined_g = gmp_init(2);

        return array($predefined_p, $predefined_g);
    }

    /**
     * RFC 3256 - 3072bits MODP
     *
     * @return \GMP[]
     */
    private function dh_predefined_3072()
    {
        $predefined_p = gmp_init("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200CBBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFCE0FD108E4B82D120A93AD2CAFFFFFFFFFFFFFFFF");
        $predefined_g = gmp_init(2);

        return array($predefined_p, $predefined_g);
    }

    /**
     * RFC 3256 - 4096bits MODP
     *
     * @return \GMP[]
     */
    private function dh_predefined_4096()
    {
        $predefined_p = gmp_init("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200CBBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFCE0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B2699C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934063199FFFFFFFFFFFFFFFF");
        $predefined_g = gmp_init(2);

        return array($predefined_p, $predefined_g);
    }

    /**
     * RFC 3256 - 6144bits MODP
     *
     * @return \GMP[]
     */
    private function dh_predefined_6144()
    {
        $predefined_p = gmp_init("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200CBBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFCE0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B2699C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C93402849236C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B332051512BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97FBEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AACC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58BB7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E6DCC4024FFFFFFFFFFFFFFFF");
        $predefined_g = gmp_init(2);

        return array($predefined_p, $predefined_g);
    }

    /**
     * RFC 3256 - 8192bits MODP
     *
     * @return \GMP[]
     */
    private function dh_predefined_8192()
    {
        $predefined_p = gmp_init("0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200CBBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFCE0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B2699C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C93402849236C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B332051512BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97FBEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AACC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58BB7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E6DBE115974A3926F12FEE5E438777CB6A932DF8CD8BEC4D073B931BA3BC832B68D9DD300741FA7BF8AFC47ED2576F6936BA424663AAB639C5AE4F5683423B4742BF1C978238F16CBE39D652DE3FDB8BEFC848AD922222E04A4037C0713EB57A81A23F0C73473FC646CEA306B4BCBC8862F8385DDFA9D4B7FA2C087E879683303ED5BDD3A062B3CF5B3A278A66D2A13F83F44F82DDF310EE074AB6A364597E899A0255DC164F31CC50846851DF9AB48195DED7EA1B1D510BD7EE74D73FAF36BC31ECFA268359046F4EB879F924009438B481C6CD7889A002ED5EE382BC9190DA6FC026E479558E4475677E9AA9E3050E2765694DFC81F56E880B96E7160C980DD98EDD3DFFFFFFFFFFFFFFFFF");
        $predefined_g = gmp_init(2);

        return array($predefined_p, $predefined_g);
    }
}
