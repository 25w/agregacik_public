<?php

namespace App\Security;

use App\Repository\ApiTokenRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiTokenAuthenticator extends AbstractGuardAuthenticator
{

    private $apiTokenRepo;

    public function __construct(ApiTokenRepository $apiTokenRepo)
    {
        $this->apiTokenRepo = $apiTokenRepo;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser(). Returning null will cause this authenticator
     * to be skipped.
     */
    public function getCredentials(Request $request)
    {
        $authorizationHeader = $request->headers->get('Application-Authorization');
        // skip beyond "Bearer "
        return substr($authorizationHeader, 7);

//        if (!$token = $request->headers->get('X-AUTH-TOKEN')) {
//            // No token?
//            $token = null;
//        }
//        // What you return here will be passed to getUser() as $credentials
//        return array(
//            'token' => $token,
//        );
    }
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
////        var_dump($credentials); die;
//
////        $apikey = $credentials['token'];
////        if (null === $apikey) {
////            return;
////        }
//
//        //fixme get user name from token
//        $apikey = 'crxapi';
//        //think of https://symfony.com/doc/4.0/security/api_key_authentication.html#createtoken
//
//        // if null, authentication will fail
//        // if a User object, checkCredentials() is called
//        return $userProvider->loadUserByUsername($apikey);

        $token = $this->apiTokenRepo->findOneBy([
            'token' => $credentials
        ]);

        if (!$token) {
            throw new CustomUserMessageAuthenticationException(
                'Invalid API Token'
            );
        }

        if ($token->isExpired()) {
            throw new CustomUserMessageAuthenticationException(
                'Token expired'
            );
        }

        return $token->getUser();

    }
    public function checkCredentials($credentials, UserInterface $user)
    {
//        dd('checking credentials');

        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case
        // return true to cause authentication success
        return true;
    }
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );
        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }
    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required !!'
        );
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
    public function supportsRememberMe()
    {
        return false;
    }

    public function supports(Request $request)
    {
//        return $request->headers->has('X-AUTH-TOKEN');
        // look for header "Application-Authorization: Bearer <token>"
        return $request->headers->has('Application-Authorization')
            && 0 === strpos($request->headers->get('Application-Authorization'), 'Bearer ');
    }
}