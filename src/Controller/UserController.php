<?php

namespace App\Controller;

use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Entity\CashRegister;
use App\Entity\Claim;
use App\Entity\User;
use App\Entity\UserSensitiveData;
use App\Form\CompleteUserAccountType;
use App\Form\UserType;
use App\Repository\CashRegisterRepository;
use App\Repository\ClaimRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="app_user_index")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');



        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/account/add/data", name="app_user_account_add_required_data")
     */
    public function account_add_required_data(UserInterface $user, Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var User $userEntity */
        $userEntity = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $user->getUsername(),
        ]);

        $form = $this->createForm(CompleteUserAccountType::class, $userEntity->getUserSensitiveData(), ['user' => $userEntity]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (! $userEntity->hasUserSensitiveData()) {
                $userSensitiveData = new UserSensitiveData();
            } else {
                $userSensitiveData = $userEntity->getUserSensitiveData();
            }
            $data = $form->getData();

            $userSensitiveData->setUser($userEntity);
            $userSensitiveData->setAddress($data->getAddress());
            $userSensitiveData->setCity($data->getCity());
            $userSensitiveData->setPostalCode($data->getPostalCode());
//            $userEntity->setUserSensitiveData($userSensitiveData);

            if ($data->getPesel()) {
                $userSensitiveData->setPesel($data->getPesel());
            }
            if ($data->getNip()) {
                $userSensitiveData->setNip($data->getNip());
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userSensitiveData);
            $entityManager->flush();

            //disable requirement for fullfill account
            $request->getSession()->set('user_must_complete_account_data', false);

            $this->addFlash('success', 'Dziękujemy za uzupełnienie danych konta.');

            return $this->redirectToRoute('app_user_index');
        }


        return $this->render('user/complete.data.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
//            'data' => $data,
        ]);
    }

    /**
     * @Route("/user/claims", name="app_user_claims")
     */
    public function claims(UserInterface $user)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);
        $data = $repo->findUserClaims($user->getId());

//        /** @var UserRepository $r */
//        $r = $this->getDoctrine()->getRepository(User::class);
//        var_dump($r->getCurrierList());

        return $this->render('user/claims.html.twig', [
            'controller_name' => 'carrierController',
            'data' => $data,
        ]);
    }

    /**
     * @Route("/user/claim/{id}", name="app_user_claim")
     */
    public function claim($id)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);

        $data = $repo->findOneBy(['id' => $id]);

        return $this->render('user/claim.html.twig', [
            'controller_name' => 'userController',
            'data' => $data,
            'id' => $id,
        ]);
    }

    /**
     * @Route("/user/coins", name="app_user_coins")
     */
    public function coins(UserInterface $user, CashRegistryFactory $cashRegistryFactory)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $em = $this->getDoctrine()->getManager();

        /** @var CashRegisterRepository $crRepo */
        $cr = $cashRegistryFactory->createFromUser($em, $user->getId());
dd( 'TODO user coins ', $cr->getActualCoins());
//        /** @var UserRepository $r */
//        $r = $this->getDoctrine()->getRepository(User::class);
//        var_dump($r->getCurrierList());

        return $this->render('user/claims.html.twig', [
//            'data' => $data,
        ]);
    }
}
