<?php

namespace App\Controller;

use App\Agregacik\OperationBundle\Enum\Currency;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Strategy\Operation\Operation;
use App\Entity\Airport;
use App\Entity\Claim;
use App\Entity\ClaimProcess;
use App\Entity\User;
use App\Enum\ClaimType;
use App\Enum\UserType;
use App\Form\ClaimProcessStepOneValidationForm;
use App\Form\ClaimProcessStepThreeForm;
use App\Form\ClaimProcessStepTwoCollectDataForm;
use App\Repository\ClaimProcessRepository;
use App\Repository\ClaimRepository;
use App\Service\ClaimService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Tetranz\Select2EntityBundle\Service\AutocompleteService;

class WorkerController extends AbstractController
{
    private $operation;

    public function __construct(Operation $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @Route("/worker", name="app_worker_index")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_WORKER');



        return $this->render('worker/index.html.twig', [
            'controller_name' => 'WorkerController',
        ]);
    }

    /**
     * @Route("/worker/claims", name="app_worker_claims")
     */
    public function claims()
    {
        $this->denyAccessUnlessGranted('ROLE_WORKER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);

        $data = $repo->findAll();

        return $this->render('worker/claims.html.twig', [
            'controller_name' => 'workerController',
            'data' => $data,
        ]);
    }

    /**
     * @Route("/worker/claim/{id}", name="app_worker_claim")
     */
    public function claim($id)
    {
        $this->denyAccessUnlessGranted('ROLE_WORKER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);

        $data = $repo->findOneBy(['id' => $id]);

        return $this->render('worker/claim.html.twig', [
            'controller_name' => 'workerController',
            'data' => $data,
            'id' => $id,
        ]);
    }

    /**
     * @Route("/worker/process/claim/{id}", name="app_worker_process_claim")
     */
    public function claimProcess(Request $request, UserInterface $user, ClaimService $claimService, int $id)
    {
        /*
         * 1. najpierw pokaz pierwszy form z pytaniami czy wniosek jest ok - pierwsza validacja
         * 2. pokaz form z uzupelnieniem danych
         * 3. pokaz form z logika działania co ma się dziać po akcjach.
         */

        $this->denyAccessUnlessGranted('ROLE_WORKER');
        $em = $this->getDoctrine()->getManager();

        /** @var ClaimRepository $claimRepo */
        $claimRepo = $em->getRepository(Claim::class);
        $claim = $claimRepo->find($id);





        //FIXME TODO TEST claim worth value
        if ($claim->isVerified()) {
            $this->addFlash('success', 'TEST INFO: Wniosek juz zweryfikowany i czeka na okreslenie wartosci.');
            $valueOfClaim = $claimService->determineValue($claim);
            var_dump('OKRESLENIE WARTOSCI ROSZCZENIA', $valueOfClaim);
        }
        //TESTS END





        if (!$claim) {
            $this->addFlash('danger', 'Wniosek nie istnieje.');
            return $this->redirectToRoute('app_worker_claims');
        }

        //FIXME uncomment after tests
//        if ($claim->isVerified()) {
//            $this->addFlash('danger', 'Wniosek został już przetworzony.');
//            return $this->redirectToRoute('app_worker_claims');
//        }

        $operation = (int)$claim->getType();

        //check if claim process for this $id (claim_id) is strted
        /** @var ClaimProcessRepository $claimProcessRepo */
        $claimProcessRepo = $em->getRepository(ClaimProcess::class);
        $claimProcess = $claimProcessRepo->findOneBy(['claim' => $id]);
        if (empty($claimProcess)) {
            $claimProcess = new ClaimProcess();
        }

        $show_form = true;
        if (!is_null($claimProcess->getIsActual()) && ! $claimProcess->getIsActual()) {
            $this->addFlash('warning', 'Wniosek nie jest aktualny.');
            $show_form = false;
        } elseif (!is_null($claimProcess->getIsReadable()) && ! $claimProcess->getIsReadable()) {
            $this->addFlash('warning', 'Wniosek jest nie czytelny.');
            $show_form = false;
        }
//        elseif (!is_null($claimProcess->isIsDelayInPublicData()) && ! $claimProcess->isIsDelayInPublicData()) {
//
//            //if is_delay_in_public_data ==0 then contact client and get data!!!!
//            //data is saved magic way by form (handleRequest)
//            //FIXME
//            $this->addFlash('warning', 'Prosze skontakotwać się z klientem i zweryfikować!');
//            $show_form = true;
//        }

        //FIXME move below logic to UserClaimVerrificationStrategy and one of odwolanie/odmowa/opoznienie


        $formParams = ['id' => $id];

        $formType = ClaimProcessStepOneValidationForm::class;

        $step = 1;

        if ($claimProcess->isStepOneComplete()) {
            $step = 2;
            $formType = ClaimProcessStepTwoCollectDataForm::class;
            $formParams['carrier'] = $claim->getCarrier();
            $formParams['operation'] = $operation;
        }

        if ($claimProcess->isStepTwoComplete()) {
            $step = 3;
            unset($formParams['carrier']);
            $formType = ClaimProcessStepThreeForm::class;
            $formParams['operation'] = $operation;

            //na tym etapie klient musi juz miec uzupełnione swoje dane prywatne i miec aktywne konto !!!!!!
            /** @var User $claimAuthor */
            $claimAuthor = $claim->getAuthor();
            if (! $claimAuthor->hasUserSensitiveData()) {
                //if user has no sensitive data then block claim
                $show_form = false;
                $this->addFlash('warning', 'Konto klienta nie zostało uzupełnione, procedowanie wniosku wstrzymane do momentu uzupełnienia danych.');
            }
        }

        $form = $this->createForm($formType, $claimProcess, $formParams);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($form->get('step')->getData() == 'one') {

                //save first step to claimprocess
                $claimProcess->setClaim($claim);
                $claimProcess->setWorker($user);
                $claimProcess->setIsActual($data->getIsActual());
                $claimProcess->setIsReadable($data->getIsReadable());
                $em->persist($claimProcess);
                $em->flush();
                $this->addFlash('success', 'Pierwszy krok zapisany.');


            } elseif ($form->get('step')->getData() == 'two') {
                //save currier for claim
                // FIXME move to first step!!!!!!!!!!!!!!!!!!!!!!!!
                $claim->setCarrier($form->get('carrier')->getData()); // TODO move to first step!!!!!!!!!!!!!!!!!!!!!!!!
                // FIXME move to first step!!!!!!!!!!!!!!!!!!!!!!!!
                $em->persist($claim);
                $em->flush();

                //data is saved magic way by form (handleRequest)

                $this->addFlash('success', 'Drugi krok zapisany.');


            } elseif ($form->get('step')->getData() == 'three') {

                if (!empty($data->getDelayFromPublicDataDetails()) && $data->isIsDelayInPublicData()) {
                    $claimProcess->setIsDelayInPublicData($data->isIsDelayInPublicData());
                    $claimProcess->setDelayFromPublicDataDetails($data->getDelayFromPublicDataDetails());
                    $em->persist($claimProcess);

                    $em->flush();
                }


                $this->addFlash('success', 'Trzeci krok zapisany.');

                if ($data->isIsDelayInPublicData()) {
                    //determine value of claim here
                    $valueOfClaim = $claimService->determineValue($claim);
                    $claim->setWorthValue((floatval($valueOfClaim)));

                    //na tym etapie klient musi uzupełnić swoje dane prywatne do tego celu po zrobienia jemu konta
                    $claim->setIsVerified();
                    $em->persist($claim);
                    $em->flush();
                    $this->addFlash('success', 'Wniosek zweryfikowany.');

                }

                //FIXME TODO pomysl o 4 kroku uzupelnienie danych klienta??? to blokuje biuroi przed podgladem dokumentow ... - albo sam user je uzupelni albo mu pomozemy?

            }

            // refresh
            return $this->redirectToRoute('app_worker_process_claim', ['id' => $id]);

        }

        $files = [];
        if ($claim->getFiles()) {
            $files = $claim->getFiles()->toArray();
        }

        return $this->render('worker/claim.process.html.twig', [
            'controller_name' => 'WorkerController',
            'claim' => $claim,
            'form' => $form->createView(),
            'operation' => $operation,
            'files' => $files,
            'step' => $step,
            'show_form' => $show_form,
        ]);
    }

    /**
     * @Route("/worker/claim/search/{query}", name="json_worker_claim_search", methods={"POST", "GET"})
     *
     * @param $query
     * @return JsonResponse
     */
    public function searchClaims(Request $request, $query)
    {
        $data = $this->getDoctrine()->getManager()->getRepository(Claim::class)->findForSearch($query);

        foreach ($data as $key=>$datas){
            $data[$key]['url']= $this->generateUrl('app_worker_claim', ['id' => $datas['id']]);
        }

        $normalizers = [
            new ObjectNormalizer()
        ];
        $encoders =  [
            new JsonEncoder()
        ];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $serializer->serialize($data, 'json');
        return new JsonResponse($data, 200, [], true);
    }


    /**
     * @param Request $request
     *
     * @param AutocompleteService $as
     * @return Response
     * @Route("/autocomplete/airport/{type}", name="ajax_airport_autocomplete")
     *
     */
    public function autocompleteAction(Request $request, AutocompleteService $as, $type)
    {
        // Check security etc. if needed

        $formType = ClaimProcessStepTwoCollectDataForm::class;
        if ($type == 3) {
            $formType = ClaimProcessStepThreeForm::class;
        }

//        $as = $this->get('tetranz_select2entity.autocomplete_service');
        $result = $as->getAutocompleteResults($request, $formType);
        return new JsonResponse($result);

    }

}
