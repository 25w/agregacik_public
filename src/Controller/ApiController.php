<?php

namespace App\Controller;

use App\Entity\ApiUser;
use App\Entity\ApiToken;
use App\Entity\WatchDog;
use App\Manager\PublicFilesManager;
use App\Service\ClaimService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;


class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index()
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    /**
     * @Route("/api/get_access", name="api_get_access", methods={"POST", "GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAccessKey(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $params = json_decode($request->getContent(), true);

        if (is_null($params)) {
            return new JsonResponse(['No params!'], Response::HTTP_BAD_REQUEST, ['Access-Control-Allow-Origin' => '*']);
        }

        $login = $params['l'];
        $secret = $params['s'];

        if (empty($login) || empty($secret)) {
            return new JsonResponse(['No valid params!'], Response::HTTP_BAD_REQUEST, ['Access-Control-Allow-Origin' => '*']);
        }

        /** @var ApiUser $user */
        $user = $em->getRepository(ApiUser::class)->findOneBy(['username' => $login, 'apiSecret' => $secret]);

        if (!$user) {
            return new JsonResponse(['Auth data not valid!'], Response::HTTP_BAD_REQUEST, ['Access-Control-Allow-Origin' => '*']);
        }

        /** @var ApiToken $apiAlreadyHasToken */
        $apiAlreadyHasToken = $em->getRepository(ApiToken::class)->findOneBy(['user' => $user]);

        if (!$apiAlreadyHasToken || $apiAlreadyHasToken->isExpired()) {
            //create new token if old one is invalid
            $apiTokenGenerated = new ApiToken($user);
            $em->persist($apiTokenGenerated);
            $token = $apiTokenGenerated->getToken();
            $em->flush();
        } else {
            $token = $apiAlreadyHasToken->getToken();
        }

        return new JsonResponse(['OK', 'Token' => $token], Response::HTTP_OK, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @Route("/api/send_claim", name="api_receive_claim", methods={"POST", "GET"})
     * @param Request $request
     * @param ClaimService $claimService
     * @return JsonResponse
     *
     * roszczenie / zobowiązanie
     * claim      / obligation
     */
    public function receiveClaim(Request $request, ClaimService $claimService, UserInterface $user): JsonResponse
    {

        //TODO check if us POST REQUEST and ACL??

        $saveResult = $claimService->save($request);
        if ($saveResult instanceof JsonResponse) {
            return $saveResult;
        }

        return new JsonResponse(['status' => 'OK'], Response::HTTP_OK, ['Access-Control-Allow-Origin' => '*']);
    }
}
