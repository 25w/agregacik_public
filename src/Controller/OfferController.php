<?php

namespace App\Controller;

use App\Entity\File;
use App\Entity\Offer;
use App\Form\OfferType;
use App\Manager\PublicFilesManager;
use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/offer")
 */
class OfferController extends AbstractController
{
    /**
     * @Route("/", name="app_admin_offer_index", methods={"GET"})
     */
    public function index(OfferRepository $offerRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('offer/index.html.twig', [
            'offers' => $offerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_admin_offer_new", methods={"GET","POST"})
     */
    public function new(Request $request, PublicFilesManager $filesManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            /** @var UploadedFile $brochureFile */
            $imageFile = $form->get('image')->getData();

            //TODO collect all required files now i know about one
            $filesArray = [
                $imageFile
//                $request->files->get('file1'),
//            $request->files->get('file2'),
            ];

            foreach ($filesArray as $fileA) {
                if (empty($fileA)) break;

                $filesManager->setFileType(PublicFilesManager::FILE_OFFER);
                $uploadResult = $filesManager->upload($fileA);

                if ($uploadResult) {
                    $fileEntity = new File();
                    $fileEntity->setName($uploadResult->getFileName());
                    $fileEntity->setPath($filesManager->transformToPublicPath($uploadResult->getFileName()));
                    $fileEntity->setOffer($offer);

                    $offer->addFile($fileEntity);

                    $entityManager->persist($fileEntity);
                }
            }


            $entityManager->persist($offer);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_offer_index');
        }

        return $this->render('offer/new.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_offer_show", methods={"GET"})
     */
    public function show(Offer $offer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $files = [];
        if ($offer->getFiles()) {
            $files = $offer->getFiles()->toArray();
        }

        return $this->render('offer/show.html.twig', [
            'offer' => $offer,
            'files' => $files,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_admin_offer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Offer $offer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_admin_offer_index');
        }

        return $this->render('offer/edit.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_admin_offer_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Offer $offer): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if ($this->isCsrfTokenValid('delete'.$offer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($offer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_offer_index');
    }
}
