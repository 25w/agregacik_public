<?php

namespace App\Controller;

use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Factory\CashRegistryFactory;
use App\Agregacik\OperationBundle\Strategy\Operation\Operation;
use App\Entity\Claim;
use App\Entity\ClaimProcess;
use App\Entity\User;
use App\Factory\DocumentGeneratorFactory;
use App\Repository\ClaimRepository;
use App\Repository\UserRepository;
use App\Service\DocumentGeneratorService;
use App\Service\PublicPrivateKeyService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class CarrierController extends AbstractController
{

    private $operation;

    public function __construct(Operation $operation)
    {
        $this->operation = $operation;
    }

    /**
     * @Route("/carrier", name="app_carrier_index")
     */
    public function index()
    {

        $this->denyAccessUnlessGranted('ROLE_CARRIER');


        /*
         * TODO
         *  list with claims with currier id and IsVerified as true - make AUTHorisation od data!!!!!! sign document
         */

        return $this->render('carrier/index.html.twig', [
            'controller_name' => 'CarrierController',
        ]);
    }

    /**
     * @Route("/carrier/claims", name="app_carrier_claims")
     */
    public function claims(UserInterface $user)
    {
        $this->denyAccessUnlessGranted('ROLE_CARRIER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);
        /** @var Claim $data */
        $data = $repo->findAllVerifiedClaimsForCarrier($user->getId());

//        /** @var UserRepository $r */
//        $r = $this->getDoctrine()->getRepository(User::class);
//        var_dump($r->getCurrierList());

        return $this->render('carrier/claims.html.twig', [
            'controller_name' => 'carrierController',
            'data' => $data,
        ]);
    }

    /**
     * @Route("/carrier/claim/{id}", name="app_carrier_claim")
     */
    public function claim($id)
    {
        $this->denyAccessUnlessGranted('ROLE_CARRIER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);
        /** @var Claim $data */
        $data = $repo->findOneBy(['id' => $id]);

        /** @var ArrayCollection $claimProcesses */
        $claimProcesses = $data->getClaimProcesses();
//        foreach ($claimProcesses as $claimProcess) {
//            /** @var ClaimProcess $x */
//            var_dump(($claimProcess->getId()));
//        }


        return $this->render('carrier/claim.html.twig', [
            'controller_name' => 'carrierController',
            'data' => $data,
            'id' => $id,
            'claim_process_data' => $claimProcesses[0]
        ]);
    }

    /**
     * @Route("/carrier/testclaimauth/{id}", name="app_carrier_claim_test_auth")
     */
    public function testclaimauth($id, PublicPrivateKeyService $publicprivatekey, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_CARRIER');

        // strategy TODO change to get data from where ;D
        $request->request->set('operation_data', [['claim_id' => $id]]);
        $this->operation->run(OperationType::CARRIER_CLAIM_AUTHENTICATION(), $request);
        $this->addFlash('success', 'Wniosek zautoryzowany.');

        return $this->redirectToRoute('app_carrier_claim', ['id' => $id]);
//        return $this->render('carrier/claim.html.twig', [
//            'controller_name' => 'carrierController',
//            'data' => $claim,
//            'id' => $id,
//        ]);
    }

    /**
     * @Route("/carrier/claim/{id}/file/{doctype}", name="app_carrier_claim_file")
     */
    public function claimFiles($id, $doctype, DocumentGeneratorFactory $generator)
    {
        $this->denyAccessUnlessGranted('ROLE_CARRIER');

        /** @var ClaimRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Claim::class);

        $data = $repo->findOneBy(['id' => $id]);


        if (! $data->isVerified()) {
            return $this->render('errors/400.twig');
        }

        /** @var DocumentGeneratorFactory $generator */
//        $generator = $this->container->get('app.document.generator');
        $generator->setTheme($doctype);
        $generator->setFormat('pdf');
        $generator->setData($data);
        $generator->render();

    }
}
