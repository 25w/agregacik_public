<?php


namespace App\Controller;


use App\Agregacik\OperationBundle\Enum\Currency;
use App\Agregacik\OperationBundle\Enum\OperationType;
use App\Agregacik\OperationBundle\Strategy\Message;
use App\Agregacik\OperationBundle\Strategy\Operation\Operation;
use App\Entity\CrVolume;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        //redirect based on role
        $roles = $this->getUser()->getRoles();
        if (in_array('ROLE_ADMIN', $roles)) {
            $result = $this->redirectToRoute('app_admin_index');
        } elseif (in_array('ROLE_WORKER', $roles)) {
            $result = $this->redirectToRoute('app_worker_index');
        }  elseif (in_array('ROLE_CARRIER', $roles)) {
            $result = $this->redirectToRoute('app_carrier_index');
        } elseif (in_array('ROLE_USER', $roles)) {
            $result = $this->redirectToRoute('app_user_index');
        } else {
            $result = $this->redirectToRoute('app_login');
        }

        return $result;
    }


    //TESTINGS FIXME this need to be in bunde but route from bundle not work

    private $message;
    private $operation;

    public function __construct(Message\Message $message, Operation $operation)
    {
        $this->message = $message;
        $this->operation = $operation;
    }

    /**
     * TESTING ONLY
     *
     * @Route("/strategy/{type}", methods={"POST", "GET"})
     * @param Request $request
     * @param string $type
     * @return Response
     */
    public function strategy(Request $request, string $type)
    {
//        $em = $this->getDoctrine()->getManager();
//        var_dump($em->getRepository(CrVolume::class)->getUserCoinsCount(1));
//die('dup');
//        $payload = json_decode($request->getContent(), true);
//        $payload = ['message' => 'wiadomosc1'];
//        $this->message->send($type, $payload);

        //factory WIP
//        $request->request->set('params',[1, '2']);
//
//        var_dump($request->get('params'), $request->getContent());
//        $input = OperationInputFactory::createFromRequest($request);
//        $input->setOperationType(OperationType::CARRIER_ADD_COIN_TO_POOL());
//
//        var_dump($input->getOrdersId());

        // strategy TODO change to get data from where ;D
        $request->request->set('operation_data',['user_id' => 1, 'value' => rand(10,50), 'currency' => Currency::POLAND()]);
//        $this->operation->run($type, $payload); //OR
        $this->operation->run(OperationType::CARRIER_ADD_COIN_TO_POOL(), $request);
        $this->operation->run(OperationType::ADMIN_ADD_COIN_TO_WORKER(), $request);

        //TODO run next strategies
        // check biznes logic vs actual model
        // i think we have problem with logic :D

        return new Response('OK');
    }

}
