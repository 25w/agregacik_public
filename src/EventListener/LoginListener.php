<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use App\Entity\User;

class LoginListener
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        // Get the User entity.
        $user = $event->getAuthenticationToken()->getUser();

        /** @var User $userEntity */
        $userEntity = $this->em->getRepository(User::class)->findOneBy([
            'email' => $user->getUsername(),
        ]);

        //TODO check if user has completed account if no then redirect to complete account.
        if (!empty($user->getRoles()[0]) && $user->getRoles()[0] === 'ROLE_USER') {
            //check if has all required data?

            //if not set session data that will redirect user to his place - app_user_account_add_required_data


            if (! $userEntity->hasUserSensitiveData()) {
                $event->getRequest()->getSession()->set('user_must_complete_account_data', true);
            }
        }

        if ($userEntity) {
            // Update your field here.
            $userEntity->setLastLogin(new \DateTime(date('Y-m-d H:i:s')));

            // Persist the data to database.
            $this->em->persist($userEntity);
            $this->em->flush();
        }
    }
}