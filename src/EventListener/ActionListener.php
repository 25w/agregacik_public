<?php


namespace App\EventListener;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ActionListener
{
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        if ($event->getRequest()->getRequestUri() == '/logout') {
            return;
        }

        //Listen for session var that user has not completed account data - so make redirect responce.
        $user_must_complete_account_data = $event->getRequest()->getSession()->get('user_must_complete_account_data');
        if ($user_must_complete_account_data && $event->getRequest()->getRequestUri() != '/account/add/data') {
            $url = $this->router->generate('app_user_account_add_required_data');
            $response = new RedirectResponse($url);
            $event->setResponse($response);
        }

    }
}