<?php

namespace App\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserLocaleSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;
    private $em;
    private $allowedLangs;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->allowedLangs = ['de', 'en'];
    }

    public function onKernelController(ControllerEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        if(array_key_exists('_locale', $_GET) && array_search($_GET['_locale'], $this->allowedLangs) !== FALSE) {
            if (!$event->isMasterRequest()) {
                return;
            }

            if (!$token = $this->tokenStorage->getToken()) {
                return ;
            }

            if (!$token->isAuthenticated()) {
                return ;
            }

            if (!$user = $token->getUser()) {
                return ;
            }

//            $locale = $_GET['_locale'];
//            // update user
//            $user->setLocale($locale);
//
//            $this->em->flush($user);
//            $request->setLocale($locale);
//            $request->getSession()->set('_locale', $locale);
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::CONTROLLER => array(array('onKernelController', 1)),
        );
    }
}