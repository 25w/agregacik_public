<?php


namespace App\Service;


use App\Entity\User;
use App\Enum\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

class UserManagerService
{
    /**
     * @var LoggerInterface $logger
     */
    private $logger;
    /**
     * @var EntityManagerInterface $em
     */
    private $em;
    private $doctrine;

    /**
     * @var MailerInterface $mailer
     */
    private $mailer;

    private $resetPasswordHelper;

    public function __construct(LoggerInterface $logger, ContainerInterface $container, MailerInterface $mailer,
                                ResetPasswordHelperInterface $resetPasswordHelper)
    {
        $this->logger = $logger;
        $this->doctrine = $container->get('doctrine');
        $this->em = $this->doctrine->getManager();
        $this->mailer = $mailer;
        $this->resetPasswordHelper = $resetPasswordHelper;
    }

    public function createUserAccount($params)
    {
        //check if user has already account if so do not create new just return user entity
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $params->email]);
        if ($user !== null) {
            return $user;
        }

        $user = new User();
        $user->setLogin($params->email);
        $user->setPassword('FirstPassword'); // will be newer used
        $user->setFirstName($params->name);
        $user->setLastName($params->surname);
        $user->setEmail($params->email);
        $user->setPhone($params->phone);
        $user->setRoles(['ROLE_USER']);
        $user->setType(UserType::USER()->getValue());
        $user->setActive(true);
        $user->setLastLogin(new \DateTime(date('Y-m-d H:i:s')));
        $this->em->persist($user);

        $this->em->flush();


        //in email we send password with link do zmiany hasla (widok zmian hasla z logowaniem po zmianie)
        //after login we check if has data filled if not we block on this step until user fill all userdata and then it can see claims and offers.

        // reset hasla - nie zalogowany
        $this->processSendingPasswordResetEmail($params->email);
        // uzupelnienie danych - juz zalogowany



        return $user;
    }

    private function processSendingPasswordResetEmail(string $emailFormData)
    {
        $user = $this->em->getRepository(User::class)->findOneBy([
            'email' => $emailFormData,
        ]);


        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {

        }

        $email = (new TemplatedEmail())
            ->from(new Address('admin@25w.pl', '25W Mail Bot'))
            ->to($user->getEmail())
            ->subject('Agregacik - zielonareklamacja.pl - Witaj')
            ->htmlTemplate('reset_password/new.email.html.twig')
            ->context([
                'resetToken' => $resetToken,
                'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
            ])
        ;

        $this->mailer->send($email);

    }
}