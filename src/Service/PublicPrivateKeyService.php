<?php


namespace App\Service;



use App\Algorithm\DiffieHellman;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PublicPrivateKeyService
 *
 * used for generate public and private keys
 *
 * @package App\Service
 */
class PublicPrivateKeyService
{
    private $logger;
    private $em;
    private $res;

    public function __construct(LoggerInterface $logger, ContainerInterface $container)
    {
        $this->logger = $logger;
        $this->em = $container->get('doctrine')->getManager();
        $this->res = openssl_pkey_new([
            "digest_alg" => $_ENV['OPENSSL_DIGEST_ALG'],
            "private_key_bits" => $_ENV['OPENSSL_PRIVATE_KEY_BITS'],
            "private_key_type" => $_ENV['OPENSSL_PRIVATE_KEY_TYPE'],
        ]);
    }

    /**
     * @return array with public and private key
     */
    public function generateBothKeyPair()
    {
        // Extract the private key into $private_key
        openssl_pkey_export($this->res, $private_key);
        // Extract the public key into $public_key
        $public_key_array = openssl_pkey_get_details($this->res);

        return [$public_key_array['key'], $private_key];
    }

    public function signFile(string $filemd5, $carrier_public_key, $carrier_private_key)
    {
//        echo "<br><br>This is the original text: $filemd5\n\n";

        // Encrypt using the public key
        openssl_public_encrypt($filemd5, $encrypted, $carrier_public_key);

//        $encrypted_hex = bin2hex($encrypted);
//        echo "<br><br>This is the encrypted text: $encrypted_hex\n\n";

        // Decrypt the data using the private key
        openssl_private_decrypt($encrypted, $decrypted, $carrier_private_key);

//        echo "<br><br>This is the decrypted text: $decrypted\n\n";

        if ($filemd5 === $decrypted) {
            echo "Encryption OK" . PHP_EOL;
        } else {
            echo "Encryption FAIL" . PHP_EOL;
        }

        // 2 other method

        //get master key
        $master_private_key = file_get_contents(__DIR__ . '/../../private/keys/master_key');
        $master_public_key = file_get_contents(__DIR__ . '/../../private/keys/master_key.pub');

        //bob is master
        //alice is carrier

        // alice and bob generate their data and exchange public ones
        $dh_bob   = new DiffieHellman();
        try {
            $dh_alice = new DiffieHellman(null, $dh_bob->getModulus(), $dh_bob->getBase());
        } catch (\Exception $e) {
            $this->logger->warning($e->getMessage());
        }

//        var_dump((int)$master_private_key, $master_private_key, 'x', (int)$carrier_private_key, $carrier_private_key);

// first initialization for both alice and bob (private and public keys generation)
        $dh_bob->init();//(int)$master_private_key);
        $dh_alice->init();//(int)$carrier_private_key);

// generating a shared secret with corresponding public keys
        $dh_alice->compute_secret($dh_bob->getPublic());
        $dh_bob->compute_secret($dh_alice->getPublic());

        if (0 == gmp_cmp($dh_alice->getSecret(), $dh_bob->getSecret())) {
            echo "Alice and Bob share the same secret" . PHP_EOL;
        } else {
            echo "Alice and Bob don't share the same secret" . PHP_EOL;
        }


    }

}