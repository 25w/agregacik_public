<?php

namespace App\Service;

use DateTime;

class DateTimeMagicService
{
    /**
     * //PARA: Date Should In YYYY-MM-DD Format
    //RESULT FORMAT:
    // '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
    // '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
    // '%m Month %d Day'                                            =>  3 Month 14 Day
    // '%d Day %h Hours'                                            =>  14 Day 11 Hours
    // '%d Day'                                                        =>  14 Days
    // '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
    // '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
    // '%h Hours                                                    =>  11 Hours
    // '%a Days                                                        =>  468 Days
     *
     * @param $date_1
     * @param $date_2
     * @param string $differenceFormat
     * @return string
     */
    public static function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);

    }

    /**
     * @param $datetimeStart
     * @param $datetimeEnd
     * @param string $format
     * @return array with sign as minus or plus and format interval
     * @throws \Exception
     */
    public static function dateTimeDifference($datetimeStart, $datetimeEnd, $format = '%h')
    {
        $origin = new DateTime($datetimeStart);
        $target = new DateTime($datetimeEnd);
        $interval = $origin->diff($target);
        $fullDelay = [];
        $fullDelay['seconds'] = $interval->format('%s');
        $fullDelay['minutes'] = $interval->format('%i');
        $fullDelay['hours'] = $interval->format('%h');
        $fullDelay['days'] = $interval->format('%d');
        $fullDelay['months'] = $interval->format('%m');
        $fullDelay['years'] = $interval->format('%y');

        return [$interval->format('%R'), $interval->format($format), $fullDelay];
    }
}