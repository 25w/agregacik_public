<?php


namespace App\Service;



use App\Entity\Airport;
use App\Entity\Claim;
use App\Entity\ClaimProcess;
use App\Entity\File;
use App\Entity\User;
use App\Enum\ClaimType;
use App\Enum\UserType;
use App\Manager\PublicFilesManager;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ClaimService
 *
 * used for save and manage claim from client
 *
 * @package App\Service
 */
class ClaimService
{
    private $logger;
    private $em;
    private $doctrine;
    private $filesManager;
    private $validator;
    private $usermanager;

    public function __construct(LoggerInterface $logger, ContainerInterface $container, PublicFilesManager $filesManager, ValidatorInterface $validator, UserManagerService $usermanager)
    {
        $this->logger = $logger;
        $this->doctrine = $container->get('doctrine');
        $this->em = $this->doctrine->getManager();
        $this->filesManager = $filesManager;
        $this->validator = $validator;
        $this->usermanager = $usermanager;
    }

    /**
     * save claim from user to db
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function save(Request $request)
    {
        /**
         * //        var_dump($params, $request->files->all(), $request->files->get('file1'));
        //FIXME get image and save it in Claim entity or files entity that have relation to Claim entity
        //FIXME add validator
         * var_dump($uploadResult); //FIXME got pathName and fileName
        //TODO rewrite to access complain service
        //claim - upload files
        //claim - save entity with details and files relations
         */


        $varNames = ['name', 'surname', 'phone', 'email', 'type']; // type is claim type

        $tempArray = [];
        foreach ($varNames as $varName) {
            // need to send empty string for validation rather than NULL
            if ($request->get($varName)) {
                $tempArray[$varName] = is_null($request->get($varName)) ? '' : $request->get($varName);
            }
        }
        $paramsWithValues = $tempArray;

        $params = json_decode($request->getContent());
        if ($params) {
            $paramsWithValues = $params;
        }

        /** @var User $user */
        $user = $this->usermanager->createUserAccount($paramsWithValues);

        $claim = new Claim();

        //validate data before with validator? or use entity validation?

        //here add relation to author
        $claim->setAuthor($user);

        $claim->setType((int)$paramsWithValues->type);

        $filesArray = [];
//        foreach ($params as $paramk => $paramv) {
//            //find files
//            if (strpos($paramk, 'file') !== false) {
//                //decode
//                $fileRaw = base64_decode($paramv->raw);
//                $tmpfile = tmpfile();
//                fwrite($tmpfile, $fileRaw);
//                $path = stream_get_meta_data($tmpfile)['uri'];
////                unset($tmpfile, $fileRaw);
//
//                // FIXME kurwi się na The "/tmp/phpD97MBX" file does not exist or is not readable.
//                var_dump($path, file_exists($path), is_readable($path), $paramv->name, $paramv->type);
//
//                $upFile = new UploadedFile($path, $paramv->name, $paramv->type,null,true);
//                $filesArray[] = $upFile;
//                fclose($tmpfile); // this removes the file
////                unset($upFile, $path);
//            }
//        }

        if (!empty($params->file1)) {
            $fileRaw = base64_decode($params->file1->raw);
            $tmpfile = tmpfile();
            fwrite($tmpfile, $fileRaw);
            $path = stream_get_meta_data($tmpfile)['uri'];
            $file1 = new UploadedFile($path, $params->file1->name, $params->file1->type, null, true);
            if ($file1) {
                $filesArray[] = $file1;
            }
        }

        if (!empty($params->file2)) {
            $fileRaw = base64_decode($params->file2->raw);
            $tmpfile2 = tmpfile();
            fwrite($tmpfile2, $fileRaw);
            $path = stream_get_meta_data($tmpfile2)['uri'];
            $file2 = new UploadedFile($path, $params->file2->name, $params->file2->type, null, true);
            if ($file2) {
                $filesArray[] = $file2;
            }
        }

        if (!empty($params->file3)) {
            $fileRaw = base64_decode($params->file3->raw);
            $tmpfile3 = tmpfile();
            fwrite($tmpfile3, $fileRaw);
            $path = stream_get_meta_data($tmpfile3)['uri'];
            $file3 = new UploadedFile($path, $params->file3->name, $params->file3->type, null, true);
            if ($file3) {
                $filesArray[] = $file3;
            }
        }


        foreach ($filesArray as $fileA) {
            if (empty($fileA)) break;

            $uploadResult = $this->filesManager->upload($fileA);

            if ($uploadResult) {
                $fileEntity = new File();
                $fileEntity->setName($uploadResult->getFileName());
                $fileEntity->setPath($this->filesManager->transformToPublicPath($uploadResult->getFileName()));
                $fileEntity->setClaim($claim);

                $claim->addFile($fileEntity);

                $this->em->persist($fileEntity);
            }
        }

        $this->em->persist($claim);


        //save offer to db
        $this->em->flush();
    }


    /**
     * Determine value of claim
     *
     * @param Claim $claim
     * @return int
     */
    public function determineValue(Claim $claim)
    {
        $worthValue = 0;

        $claimProcess = $claim->getClaimProcesses()[0];
        $claimtype = (int)$claim->getType();

        /** @var Airport $airportFrom */
        $airportFrom = $claimProcess->getTicketAirportFrom();
        /** @var Airport $airportTo */
        $airportTo = $claimProcess->getTicketAirportTo();

        //determine odleglosc w lini prostej pomiedzy lotniskami
        /** @var float|int $distance in km */
        $distance = $this->calculateDistanceBetweenPoints(
            $airportFrom->getLatitude(), $airportFrom->getLongitude(),
            $airportTo->getLatitude(), $airportTo->getLongitude(),
        );

        $dateStartJurney = $claimProcess->getTicketDate();
        $timeStartJurney = $claimProcess->getTicketTime();

        $dateStartRealJurney = $claimProcess->getRealDate();
        $timeStartRealJurney = $claimProcess->getRealTime();

        //determine delay between planned and real start date time
        list($delaySign, $delayHours, $fullDelayArray) = DateTimeMagicService::dateTimeDifference($dateStartJurney.' '.$timeStartJurney,
            $dateStartRealJurney.' '.$timeStartRealJurney
        );

        $euroCommissionStandardDelay = $this->determineDelayStandardTimeBasedOnDistance($distance, $claimtype);

        if ($delayHours >= $euroCommissionStandardDelay) {
            //mamy realne opóźnienie - jaka jest wartosc roszczenia?
            $worthValue = $this->determineStandardWorthValueBasedOnDelay($delayHours, $distance, $claimtype);
        }

var_dump('determineValue DEBUG',
    'Data startu z biletu: '. $dateStartJurney.' '.$timeStartJurney,
    ' Data startu od klienta: ' . $dateStartRealJurney.' '.$timeStartRealJurney.' ',
    ' Ile godzin moze byc opóźnienie aby procedowac wniosek: '. $euroCommissionStandardDelay.' h ',
    ' Opóźnienie: '. $delaySign . $delayHours.' h ',
    ' Opóźnienie rozbite: '. $fullDelayArray['years'] .' lat, '. $fullDelayArray['months'] .' miesięcy, ' .
    $fullDelayArray['days'] .' dni, '. $fullDelayArray['hours'] .' godzin, '. $fullDelayArray['minutes'] .' minut, ' .
    $fullDelayArray['seconds'] .' sekund, ' .
    ' Wartość roszczenia: '. $worthValue .' EUR '
);
        return $worthValue;
    }

    /**
     * this will return how many hours can be fly delayed that Euro commision set to proper distance
     *
     * @param $distance
     * @param $claimtype
     * @return int
     */
    private function determineDelayStandardTimeBasedOnDistance($distance, $claimtype)
    {
        switch ($claimtype) {
            case ClaimType::OPOZNIENIE()->getValue():
                //Less than 1,500km - 2 hours
                //Over 1,500km and within the EU -	3 hours
                //Between 1,500km and 3,500km and between EU and non-EU countries -	3 hours
                //More than 3,500km and between EU and non-EU countries -	4 hours

                if ($distance < 1500) {
                    return 2;
                } elseif ($distance >= 1500 && $distance <= 3500) {
                    return 3;
                } elseif ($distance > 3500) {
                    return 4;
                } else {
                    return 0;
                }

                break;
            case ClaimType::ODWOLANIE()->getValue():
                //TODO pobierz dane kiedy odwolano lot
                /*
                 * If your flight was cancelled less than 7 days before departure:
                    Flight distance	Departure and arrival times	Compensation
                    Less than 1,500km 	Departure - at least 1 hour earlier than booked flight
                    Arrival - up to 2 hours later than booked flight 	€125
                        Arrival - at least 2 hours later than booked flight 	€250
                    1,500km to 3,500km 	Departure - at least 1 hour earlier than booked flight
                    Arrival - up to 3 hours later than booked flight 	€200
                        Arrival - at least 3 hours later than booked flight 	€400
                    More than 3,500km 	Departure - at least 1 hour earlier than booked flight
                    Arrival - up to 4 hours later than booked flight 	€300
                        Arrival - at least 4 hours later than booked flight 	€600
                    If your flight was cancelled between 7 and 14 days before departure:
                    Flight distance	Departure and arrival times	Compensation
                    Less than 1,500km 	Departure - from 2+ hours earlier than booked flight
                    Arrival - up to 2 hours later than booked flight 	€125
                        Departure - from 2+ hours earlier than booked flight
                    Arrival - 2+ hours later than booked flight 	€250
                        Arrival - 4+ hours later than booked flight 	€250
                    1,500km to 3,500km 	Departure - from 2+ hours earlier than booked flight
                    Arrival - up to 3 hours later than booked flight 	€200
                        Departure - from 2+ hours earlier than booked flight
                    Arrival - 3 to 4 hours later than booked flight 	€400
                        Arrival - 4+ hours later than booked flight 	€400
                    More than 3,500km 	Departure - from 2+ hours earlier than booked flight
                    Arrival - up to 4 hours later than booked flight 	€300
                        Arrival - 4+ hours later than booked flight 	€600
                 */
                //TODO
                return 0;

                break;
            case ClaimType::ODMOWA_PRZYJECIA()->getValue():
                //TODO
                return 0;

                break;
        }
    }


    /**
     * @param $delay
     * @param $distance
     * @param $claimtype
     * @return int in euro
     */
    private function determineStandardWorthValueBasedOnDelay($delay, $distance, $claimtype)
    {
//        var_dump('determineStandardWorthValueBasedOnDelay', $delay, $distance, $claimtype);

        switch ($claimtype) {
            case ClaimType::OPOZNIENIE()->getValue():
//                Delay to your arrival |	Flight distance |	Compensation
//                3 hours or more 	    Less than 1,500km 	    €250
//                                      Between 1,500km and 3,500km 	€400
//                                      More than 1,500km and within the EU 	€400
//                3-4 hours 	        More than 3,500km, between an EU and non-EU airport 	€300
//                4 hours or more 	    More than 3,500km, between an EU and non-EU airport 	€600

                if ($delay >= 3 && $delay <= 4) {
                    if ($distance >= 3500) {
                        return 300;
                    } else {
                        return 0;
                    }
                } elseif ($delay >= 4) {
                    if ($distance >= 3500) {
                        return 600;
                    } else {
                        return 0;
                    }
                } elseif ($delay >= 3) {
                    if ($distance < 1500) {
                        return 250;
                    } elseif ($distance >= 1500 && $distance <= 3500) {
                        return 400;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }

                break;
            case ClaimType::ODWOLANIE()->getValue():
                //TODO
                return 0;

                break;
            case ClaimType::ODMOWA_PRZYJECIA()->getValue():
                //TODO
                return 0;

                break;
        }
    }

    /**
     * calculate distance between points in kilometers
     *
     * @see https://www.geeksforgeeks.org/program-distance-two-points-earth/
     * @param $latitudeFrom
     * @param $longitudeFrom
     * @param $latitudeTo
     * @param $longitudeTo
     * @return float|int
     */
    private function calculateDistanceBetweenPoints($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        $long1 = deg2rad($longitudeFrom);
        $long2 = deg2rad($longitudeTo);
        $lat1 = deg2rad($latitudeFrom);
        $lat2 = deg2rad($latitudeTo);

        //Haversine Formula
        $dlong = $long2 - $long1;
        $dlati = $lat2 - $lat1;

        $val = pow(sin($dlati/2),2)+cos($lat1)*cos($lat2)*pow(sin($dlong/2),2);

        $res = 2 * asin(sqrt($val));

        $radius = 3958.756;

        return ($res*$radius);
    }

    /**
     * twig global helper for determine name of claim by type
     *
     * @param $operationType
     * @return string
     */
    public function getDetermineByType($operationType)
    {
        if ($operationType === ClaimType::ODMOWA_PRZYJECIA()->getValue()) {
            return 'Odmowa przyjęcia';
        } elseif ($operationType === ClaimType::OPOZNIENIE()->getValue()) {
            return 'Opóźnienie';
        } elseif ($operationType === ClaimType::ODWOLANIE()->getValue()) {
            return 'Odwołanie';
        }
        return 'Błędny typ';
    }

}