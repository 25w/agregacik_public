<?php
//see https://medium.com/@galopintitouan/executing-database-migrations-at-scale-with-symfony-and-doctrine-4c60f86865b4

namespace App\Listener;

//use Psr\SimpleCache\CacheInterface;
//use Symfony\Component\EventDispatcher\EventSubscriberInterface;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpKernel\Event\GetResponseEvent;
//use Symfony\Component\HttpKernel\KernelEvents;
//use Twig\Environment;

class MaintenanceListener //implements EventSubscriberInterface
{
//    private $cache;
//    private $twig;
//
//    public function __construct(CacheInterface $c, Environment $t)
//    {
//        $this->cache = $c;
//        $this->twig = $t;
//    }
//
//    public static function getSubscribedEvents(): array
//    {
//        return [
//            KernelEvents::REQUEST => [
//                'enableMaintenanceOnRequest',
//                1000000 // Always execute the listener as first
//            ],
//        ];
//    }
//
//    public function enableMaintenanceOnRequest(GetResponseEvent $e)
//    {
//        if (!$this->cache->get('maintenance')) {
//            return;
//        }
//
//        $e->setResponse(new Response(
//            $this->twig->render('maintenance.html.twig'),
//            Response::HTTP_SERVICE_UNAVAILABLE
//        ));
//    }
}