<?php


namespace App\Factory;


interface DocumentGeneratorInterface
{
    function init();
    function setTheme(string $doc_type);
    function setFormat(string $format);
    function setData($data);
    function render();
    function getDetermineByType($type);
}