<?php


namespace App\Factory\DocType;


use App\Entity\Claim;
use jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF;
use setasign\Fpdi\Tcpdf\Fpdi;

class Apk extends \Reflection implements DocTypeInterface
{
    private $parser;
    private $data;

    public function render()
    {

        // get the service
        /** @var TCPDF $pdf */
        $pdf = $this->parser;

        // set document information
        $pdf->setCreator(PDF_CREATOR);
        $pdf->setAuthor('zielonareklamacja.pl');
        $pdf->setTitle('Analiza potrzeb klienta');
        $pdf->setSubject('Analiza potrzeb klienta programu zielonareklamacja.pl');

        // remove default header/footer
        $pdf->setPrintHeader(true);// set default header data
        $pdf->setHeaderData('', PDF_HEADER_LOGO_WIDTH, 'ZielonaReklamacja', 'Analiza potrzeb klienta – wersja 18.08.2020');
        $pdf->setPrintFooter(true);
        $pdf->setFooterData(array(0,0,0), array(0,0,0));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->setHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->setFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set font
        $pdf->setFont('freeserif', '', 12);

        $pdf->AddPage();

        $pdf->Write(10, 'Pelkacz Tech spółka z o.o., ul. Racławicka 15-19, 53-149 Wrocław, NIP: 8992881337', '', 0, '', true, 0, false, false, 0);
        $pdf->setFont('freeserif', 'B', 12);
        $pdf->Write(10, '- pośrednik w zamianie roszczenia pomiędzy pasażerem i przewoźnikiem', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(10, 'Infolinia: 71 7998534', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(10, '', '', 0, '', true, 0, false, false, 0);
        $pdf->setFont('freeserif', '', 12);

        $pdf->Write(10, '', '', 0, '', true, 0, false, false, 0);
        $pdf->MultiCell(0, 10, 'ANALIZA POTRZEB KLIENTA:', 0, 'C', false, 1);

        $dc = $this->data;

        $pdf->Write(5, '', '', 0, '', true, 0, false, false, 0);
        $tbl = <<<EOD
<table cellspacing="0" cellpadding="5" border="1">
    <tr>
        <td>Czy jesteś zainteresowana/zainteresowany zamianą swojego roszczenie z tytułu naruszenia przez przewoźnika praw pasażera określonego przez rozporządzenie (WE) nr 261/2004 powstałego {$dc['dataPrzewozu']} na trasie {$dc['miejsceZ']} - {$dc['miejsceDo']}, numer lotu {$dc['numerLotu']}, numer biletu {$dc['numerBiletu']} wystawionego na {$dc['osobaNaBilecie']} na ekwiwalent niepieniężny, którym możesz opłacić usługi (czyli na przykład opłacić bilety lotnicze) i towary w systemie rezerwacji biletów zielonareklamacja.pl</td>
        <td style="text-align: center; vertical-align: middle;"><br><br><br><br><br><br>Tak</td>
    </tr>
    <tr>
        <td>Czy wiesz, że możesz samodzielnie dochodzić swoich roszczeń z tytułu naruszenia przez przewoźnika praw pasażera określonego przez rozporządzenie (WE) nr 261/2004 i uzyskać odszkodowanie do kwoty 600 euro</td>
        <td align="center"><br><br><br>Tak</td>
    </tr>
    <tr>
        <td>Czy wiesz, że po wykonaniu umowy zawartej z pośrednikiem lub najpóźniej po 30 dniach od zawarcia umowy z pośrednikiem nie będziesz mógł dochodzić roszczeń z tytułu naruszenia przez przewoźnika praw pasażera określonego przez rozporządzenie (WE) nr 261/2004 powstałego {$dc['dataPrzewozu']} na trasie {$dc['miejsceZ']} - {$dc['miejsceDo']}, numer lotu {$dc['numerLotu']}, numer biletu {$dc['numerBiletu']}.</td>
        <td align="center"><br><br><br><br>Tak</td>
    </tr>
</table>
EOD;

        $pdf->writeHTML($tbl, true, false, false, false, '');
        $pdf->Write(10, '', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(15, 'Dane uzyskane na podstawie komunikacji telefonicznej lub internetowej.', '', 0, '', true, 0, false, false, 0);

        $pdf->Output('apk.pdf', 'I');

    }

    /**
     * @param object $data
     * @return $this
     */
    function setData($data)
    {

        /** @var Claim $dataClaim */
        $dataClaim = $data;

        //collect data
        $collectedData = [];
        $collectedData['dataPrzewozu'] = $dataClaim->getClaimProcesses()[0]->getTicketDate();
        $collectedData['miejsceZ'] = $dataClaim->getClaimProcesses()[0]->getTicketAirportFrom()->getName() . ' (' .
            $dataClaim->getClaimProcesses()[0]->getTicketCountryFrom()->getName(). ')';
        $collectedData['miejsceDo'] = $dataClaim->getClaimProcesses()[0]->getTicketAirportTo()->getName() . ' (' .
            $dataClaim->getClaimProcesses()[0]->getTicketCountryTo()->getName(). ')';
        $collectedData['numerLotu'] = $dataClaim->getClaimProcesses()[0]->getTicketFlightNo();
        $collectedData['numerBiletu'] = $dataClaim->getClaimProcesses()[0]->getTicketMetaUid();
        $collectedData['osobaNaBilecie'] = $dataClaim->getAuthor()->getFirstName() . ' ' .
            $dataClaim->getAuthor()->getLastName();

        $this->data = $collectedData;
    }

    function setParser($parser)
    {
        $this->parser = $parser;
    }
}