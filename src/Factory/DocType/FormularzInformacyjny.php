<?php


namespace App\Factory\DocType;


use App\Entity\Claim;
use jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF;

class FormularzInformacyjny implements DocTypeInterface
{
    private $parser;
    private $data;

    public function render()
    {
        /** @var TCPDF $pdf */
        $pdf = $this->parser;

        // set document information
        $pdf->setCreator(PDF_CREATOR);
        $pdf->setAuthor('zielonareklamacja.pl');
        $pdf->setTitle('Formularz Informacyjny');
        $pdf->setSubject('Formularz Informacyjny programu zielonareklamacja.pl');

        // remove default header/footer
        $pdf->setPrintHeader(true);// set default header data
        $pdf->setHeaderData('', PDF_HEADER_LOGO_WIDTH, 'ZielonaReklamacja', 'projekt formularza – wersja 19.08.2020');
        $pdf->setPrintFooter(true);
        $pdf->setFooterData(array(0,0,0), array(0,0,0));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->setHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->setFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set font
        $pdf->setFont('freeserif', '', 12);

        $pdf->AddPage();

        $dc = $this->data;

        $pdf->Write(10, 'Formularz informacyjny wniosku o', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(10, 'Data: ', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(10, $dc['przewoznik'] . ' //TODO przewoźnik: Wizz Air Hungary Legikozlekedesi Kft (spółka z o.o.) oddział w Polsce, ul. Wolności 90, 42-625 Pyrzowice, NIP: 000000000', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(5, $dc['dataWniosku'] . ' w systemie zielonareklamacja.pl został złożony wniosek o akceptację roszczenia z tytułu naruszenia przez przewoźnika praw pasażera określonego przez rozporządzenie (WE) nr 261/2004 i zamiany roszczenia na ekwiwalent w systemie rezerwacji biletów zielonareklamacja.pl', '', 0, '', true, 0, false, false, 0);
        $pdf->Write(10, 'Dane wniosku:', '', 0, '', true, 0, false, false, 0);


        $tbl1 = <<<EOD
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td width="30%">przewoźnik:</td>
        <td width="70%">{$dc['przewoznik']} //TODO Wizz Air Hungary Legikozlekedesi Kft (spółka z o.o.) oddział w Polsce, ul. Wolności 90, 42-625 Pyrzowice, NIP: 000000000</td>
    </tr>
    <tr>
        <td>numer lotu:</td>
        <td>{$dc['numerLotu']}</td>
    </tr>
    <tr>
        <td>numer biletu:</td>
        <td>{$dc['numerBiletu']}</td>
    </tr>
    <tr>
        <td>wystawiony na imie:</td>
        <td> {$dc['osobaNaBilecie']} //TODO Mariola, nazwisko: Gronowska, pesel: 81062700218</td>
    </tr>
</table>
EOD;
        $pdf->writeHTML($tbl1, true, false, false, false, '');

        $pdf->setFont('freeserif', 'B', 12);
        $pdf->MultiCell(0, 5, 'PLAN podróży (na podstawie danych z biletu):', 0, 'C', false, 1);
        $pdf->setFont('freeserif', '', 12);
        $tbl2 = <<<EOD
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td width="30%">data podróży:</td>
        <td width="70%">{$dc['dataPrzewozu']}</td>
    </tr>
    <tr>
        <td>z:</td>
        <td>Wrocław</td>
    </tr>
    <tr>
        <td>do:</td>
        <td>Berlin</td>
    </tr>
    <tr>
        <td>planowana godzina odjazdu/odlotu:</td>
        <td>21:00</td>
    </tr>
    <tr>
        <td>planowana godzina przybycia:</td>
        <td>22:00</td>
    </tr>
    <tr>
        <td>klasa:</td>
        <td>ekonomiczna</td>
    </tr>
</table>
EOD;
        $pdf->writeHTML($tbl2, true, false, false, false, '');

        $pdf->setFont('freeserif', 'B', 12);
        $pdf->MultiCell(0, 5, 'DANE RZECZYWISTE (na podstawie oświadczenia pasażera i danych ogólnodostępnych):', 0, 'C', false, 1);
        $pdf->setFont('freeserif', '', 12);
        $tbl2 = <<<EOD
<table cellspacing="0" cellpadding="1" border="1" >
    <tr>
        <td colspan="2">* na czerwono dane niezgodne</td>
    </tr>
    <tr>
        <td width="30%">data podróży:</td>
        <td width="70%" bgcolor="red">08.01.2020</td>
    </tr>
    <tr>
        <td>z:</td>
        <td>Wrocław</td>
    </tr>
    <tr>
        <td>do:</td>
        <td bgcolor="red">Frankfurt</td>
    </tr>
    <tr>
        <td>planowana godzina odjazdu/odlotu:</td>
        <td bgcolor="red">01:00</td>
    </tr>
    <tr>
        <td>planowana godzina przybycia:</td>
        <td bgcolor="red">02:00</td>
    </tr>
    <tr>
        <td>klasa:</td>
        <td>ekonomiczna</td>
    </tr>
</table>
EOD;
        $pdf->writeHTML($tbl2, true, false, false, false, '');

        $pdf->Write(15, 'Zgłoszona wartość roszczenia: 400 euro', '', 0, '', true, 0, false, false, 0);

        $pdf->Output('form.pdf', 'I');


    }

    /**
     * @param object $data
     * @return $this
     */
    function setData($data)
    {
        /** @var Claim $dataClaim */
        $dataClaim = $data;

        //collect data
        $collectedData = [];
        $collectedData['dataWniosku'] = $dataClaim->getDateAdded()->format('d.m.Y');
        $collectedData['dataPrzewozu'] = $dataClaim->getClaimProcesses()[0]->getTicketDate();
        $collectedData['miejsceZ'] = $dataClaim->getClaimProcesses()[0]->getTicketAirportFrom()->getName() . ' (' .
            $dataClaim->getClaimProcesses()[0]->getTicketCountryFrom()->getName(). ')';
        $collectedData['miejsceDo'] = $dataClaim->getClaimProcesses()[0]->getTicketAirportTo()->getName() . ' (' .
            $dataClaim->getClaimProcesses()[0]->getTicketCountryTo()->getName(). ')';
        $collectedData['numerLotu'] = $dataClaim->getClaimProcesses()[0]->getTicketFlightNo();
        $collectedData['numerBiletu'] = $dataClaim->getClaimProcesses()[0]->getTicketMetaUid();
        $collectedData['osobaNaBilecie'] = $dataClaim->getAuthor()->getFirstName() . ' ' .
            $dataClaim->getAuthor()->getLastName();
        $collectedData['przewoznik'] = $dataClaim->getClaimProcesses()[0]->getTicketMetaCarrier()->getName();

        $this->data = $collectedData;
    }

    function setParser($parser)
    {
        $this->parser = $parser;
    }
}