<?php


namespace App\Factory\DocType;


interface DocTypeInterface
{
    function render();
    function setData($data);
    function setParser($parser);
}