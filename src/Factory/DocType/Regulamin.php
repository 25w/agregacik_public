<?php


namespace App\Factory\DocType;


use jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF;
use setasign\Fpdi\Tcpdf\Fpdi;

class Regulamin implements DocTypeInterface
{
    /**
     * @var TCPDF $parser
     */
    private $parser;
    private $data;

    public function render()
    {
        /** @var TCPDF $pdf */
        $pdf = $this->parser;

        // set document information
        $pdf->setCreator(PDF_CREATOR);
        $pdf->setAuthor('zielonareklamacja.pl');
        $pdf->setTitle('Regulamin');
        $pdf->setSubject('Regulamin programu zielonareklamacja.pl');

        // remove default header/footer
        $pdf->setPrintHeader(true);// set default header data
        $pdf->setHeaderData('', PDF_HEADER_LOGO_WIDTH, 'ZielonaReklamacja', 'regulamin – wersja próbna');
        $pdf->setPrintFooter(true);
        $pdf->setFooterData(array(0,0,0), array(0,0,0));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->setHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->setFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set font
        $pdf->setFont('freeserif', '', 12);


        $pdf->AddPage();

        $regulamin_tresc = <<<EOD
<div class="main">
<h1 class="title">
Regulamin Zwrotzalot.pl dotyczący pomocy w uzyskaniu odszkodowania za skutki nieprawidłowo świadczonej usługi transportu lotniczego
</h1>
<p class="sub-title">
</p><p class="sub-title">Niniejszy regulamin opisuje zasady współpracy pomiędzy pasażerem (klientem) a Zwrotzalot.pl. Założenie sprawy na stronie www.zwrotzalot.pl jest równoznaczne z akceptacją Regulaminu.
</p><p class="general-info">
	<b>Informacje ogólne</b>
	<br><br>
	 Zwrotzalot.pl <br>
/SYNCON INTERNATIONAL ApS <br>
	ul. Piłsudskiego 43/3&nbsp;<br>
	50-032 Wrocław&nbsp; <br>
	Polska <br><br>
	E-mail: kontakt@zwrotzalot.pl <br>
	Telefon:&nbsp;+48 71 712 59 93 <br><br>
	VAT: DK-32561950 <br>
</p>
<p></p>
<p></p>
<ol class="terms">
<li class="term">
<h3>
<span>1.</span> Usługi Zwrotzalot.pl
</h3>
<ol class="sub-terms ol-1">
      <li class="sub-term">
        <span>a)</span>
        Ocena możliwości uzyskania odszkodowania.
      </li>
      <li class="sub-term">
        <span>b)</span>
        Obliczanie kwoty odszkodowania.
      </li>
      <li class="sub-term">
        <span>c)</span>
        Zgłoszenie sprawy liniom lotniczym.
      </li>
      <li class="sub-term">
        <span>d)</span>
        Realizacja sprawy, w szczególnych przypadkach na drodze sądowej.
      </li>
      <li class="sub-term">
        <span>e)</span>
       Negocjacje roszczenia.
      </li>
      <li class="sub-term">
        <span>f)</span>
        Inkasowanie i wypłacanie kwoty odszkodowania pasażerowi.
      </li>
      <li class="sub-term">
        <span>g)</span>
       Informowanie klienta o przebiegu sprawy.
      </li>
    </ol>
</li>
<li class="term">
<h3>
<span>2.</span> Obowiązki klienta
</h3>
<p>Klient zobowiązuje się do:</p>
    <ol class="sub-terms ol-1">
      <li class="sub-term">
        <span>a)</span>
        Udzielania Zwrotzalot.pl wszelkich istotnych informacji dotyczących sprawy.
      </li>
      <li class="sub-term">
        <span>b)</span>
        Odpowiadania na pytania Zwrotzalot.pl zgodnie z prawdą i bez zwłoki.
      </li>
      <li class="sub-term">
        <span>c)</span>
        Zaprzestania bezpośredniego kontaktu z liniami lotniczymi - w odniesieniu do sprawy należy kierować się do Zwrotzalot.pl.
      </li>
      <li class="sub-term">
        <span>d)</span>
        W nadzwyczajnych przypadkach, do pomocy w pokryciu kosztów sądowych prywatnym ubezpieczeniem lub ubieganiem się o bezpłatną pomoc prawną ze strony rządu (zgodnie z punktem 5).
      </li>
    </ol>
</li>
<li class="term">
<h3>
<span>3.</span> Prawa Zwrotzalot.pl
</h3>
 <p>Zwrotzalot.pl zastrzega sobie przez cały czas trwania sprawy następujące prawa:</p>
    <ol class="sub-terms ol-1">
      <li class="sub-term">
        <span>a)</span>
        Do uzyskania informacji o sprawie ze źródeł zewnętrznych oraz do ujawnienia danych kontaktowych i informacji dotyczących lotu od linii lotniczej lub dowolnej strony trzeciej opisanej w Polityce Ochrony Danych.
      </li>
      <li class="sub-term">
        <span>b)</span>
        Do podjęcia wszelkich działań prawnych, które mogą przyczynić się do zapłaty odszkodowania.
      </li>
      <li class="sub-term">
        <span>c)</span>
        Do zaprzestania działań, bez uprzedniego poinformowania klienta, na przykład w wyniku pojawienia się nowych informacji w sprawie, które uniemożliwiają Zwrotzalot.pl realizację założeń, zmiany prawa, przedawnienia sprawy lub niewypłacalności linii lotniczych.
      </li>
      <li class="sub-term">
        <span>d)</span>
        Do wyłączności roszczenia złożonego przez klienta, nawet w przypadku osobiście wysłanego wniosku do linii lotniczej. Po przekazaniu sprawy firmie Zwrotzalot.pl, jest ona jest jedynym organem ubiegającym się o odszkodowanie i z jej działań wynika uzyskanie odszkodowania. Klient zobowiązany jest do zapłacenia firmie Zwrotzalot.pl należnej prowizji zgodnie z opisem w punkcie 5a).
      </li>
      <li class="sub-term">
        <span>e)</span>
        Do kontaktu z klientem za pośrednictwem telefonu, listu bądź mediów elektronicznych w celu marketingu produktów oraz świadczenia usług, jak i również dostarczania ofert na temat biuletynów, wydarzeń oraz konkursów. Klient może odmówić w dowolnym momencie kontaktu poprzez wysłanie wiadomości e-mail na adres: kontakt@zwrtozalot.pl.
      </li>
    </ol>
</li>
<li class="term">
<h3>
<span>4.</span> Prawa klienta
</h3>
<p>Klient powinien pozostawić roszczenie przeciwko liniom lotniczym Zwrotzalot.pl.
Klient ma zawsze następujące prawa:</p>
    <ol class="sub-terms ol-1">
      <li class="sub-term">
        <span>a)</span>
        Prawo do zakończenia współpracy z Zwrotzalot.pl w każdym momencie, bez wypowiedzenia umowy o współpracy, w drodze pisemnego powiadomienia i po opłaceniu kosztów, zgodnie z punktem 5g).
      </li>
      <li class="sub-term">
        <span>b)</span>
        Prawo do informacji o ustalonych warunkach rozstrzygnięcia sprawy przed ostatecznym werdyktem. Klient może wybrać inną opcję postępowania niż tę rekomendowaną przez Zwrotzalot.pl. W takim wypadku Zwrotzalot.pl jest uprawniony do naliczenia klientowi kosztów zgodnie z punktem 5g).
      </li>
      <li class="sub-term">
        <span>c)</span>
W przypadku, gdy klient sam osiągnie porozumienie z linią lotniczą bez zaangażowania lub wiedzy firmy Zwrotzalot, klient będzie zobowiązany do pokrycia kosztów sprawy zgodnie z paragrafem 5a), wraz z kosztami, które mogły być należne do postępowania sądowego, do Zwrotzalot paragraf 5d) i 5g).
      </li>
    </ol>
</li>
<li class="term">
<h3>
<span>5.</span> Koszty
</h3>
<ol class="sub-terms ol-1">
      <li class="sub-term">
        <span>a)</span>
        Koszty usług Zwrotzalot.pl wynoszą 25% + VAT od kwoty otrzymanego odszkodowania.
Koszty nie są obliczane na podstawie zwrotu kosztów zakwaterowania w hotelu lub innych wydatków związanych z zakłóceniem lotu, które są uwzględnione w roszczeniu wobec linii lotniczej.
      </li>
      <li class="sub-term">
        <span>b)</span>
       Jeśli, za zgodą klienta, wykonane zostaną inne działania, wykraczające poza standardowe traktowanie roszczeń, zgodnie z ogólnymi zasadami reprezentacji prawnej, koszty są determinowane przez wysokość dodatkowego odszkodowania, podjętych działań i osiągniętych wyników.
      </li>
      <li class="sub-term">
        <span>c)</span>
       Koszty i odsetki nie są pobierane, jeśli Zwrotzalot.pl zaprzestanie dążenia do uzyskania odszkodowania.
      </li>
      <li class="sub-term">
        <span>d)</span>
        Koszty roszczenia odszkodowania mogą wzrosnąć w związku z opłatami sądowymi, prawnymi i innymi. Opłaty te są wnoszone są przez Zwrotzalot.pl i, jeśli sprawa jest wygrana, zwracane później przez linie lotnicze. Jeśli sprawa zostanie przegrana lub oddalona, Zwrotzalot.pl pokrywa te koszty.
      </li>
      <li class="sub-term">
        <span>e)</span>
       Koszty odlicza się od kwoty wypłaconego odszkodowania, a opłaty za usługi pobierane są dopiero po otrzymaniu odszkodowania przez klienta.
      </li>
      <li class="sub-term">
        <span>f)</span>
        W szczególnych przypadkach Zwrotzalot.pl ma prawo do naliczenia kosztów bezpośrednio do klienta. Może się to zdarzyć w następujących przypadkach:
        <ol type="i">
          <li>
           Jeśli linie lotnicze wypłaciły kwotę roszczenia bezpośrednio klientowi.
          </li>
          <li>
           Jeśli klient chce rozwiązać umowę z Zwrotzalot.pl, wtedy Zwrotzalot.pl jest uprawniony do naliczenia jemu kosztów zgodnie z punktem 5g).
          </li>
          <li>
           Jeśli klient chce przyjąć lub odrzucić ugodę w sprzeczności z rekomendacją Zwrotzalot.pl, Zwrotzalot.pl jest uprawniony do naliczenia jemu kosztów obliczonych zgodnie z punktem 5g).
          </li>
          <li>
            Jeśli klient celowo zataił informacje o sprawie lub celowo podał Zwrotzalot.pl informacje niezgodne z prawdą oraz jeśli na pisemną prośbę Zwrotzalot.pl nie wypełnia obowiązków wskazanych w punkcie 2), Zwrotzalot.pl ma prawo do wypowiedzenia umowy z klientem ze skutkiem natychmiastowym i naliczenia kosztów zgodnie z punktem 5 g)
          </li>
<li>
           Jeśli klient uzyskał odszkodowanie przez współpracę z inną firmą, mimo iż założył sprawę i zaakceptował regulamin Zwrotzalot.pl,  Zwrotzalot.pl jest uprawniony do naliczenia jemu kosztów obliczonych zgodnie z punktem 5g).
          </li>
        </ol>
      </li>
      <li class="sub-term">
        <span>g)</span>
        Jeżeli istnieją szczególne okoliczności, zgodnie z opisem w punkcie 5f), Zwrotzalot.pl ma prawo do pobrania opłaty od klienta, odpowiadającej 25% kwoty wnioskowanego odszkodowania, a także udokumentowanych kosztów, takich jak opłaty sądowe i koszty dodane do pomocy prawnej + podatku VAT.
      </li>
<li class="sub-term">
        <span>h)</span>
        W przypadku, gdy głównym celem roszczenia jest odzyskanie przez Klientów dodatkowo poniesionych kosztów, w szczególności zwrotu kosztów biletów lotniczych, Zwrotzalot naliczy opłatę odpowiadającą 25% + vat zwróconej kwoty, jednak nie mniej niż 25 euro + VAT
      </li>
    </ol>
</li>
<li class="term">
<h3>
<span>6.</span> Wypowiedzenie oraz wycofanie
</h3>
<ol class="sub-terms ol-1">
      <li class="sub-term">
Niniejsze Zasady i Warunki obowiązują, gdy Klient złożył sprawę na stronie www.zwrotzalot.pl i dopóki jedna ze stron nie wypowie umowy. Klient ma prawo do odstąpienia od niniejszej umowy w terminie 14 dni od daty jej wniesienia bez podania przyczyny. Aby skorzystać z prawa odstąpienia od umowy, Klient może skorzystać z poniższego wzoru formularza.
<p class="form">
<br>
	<b>Wzór formularza odstąpienia od umowy</b>
	<br><br>
	<i>Formularz ten należy wypełnić i odesłać tylko w przypadku chęci odstąpienia od umowy.</i>
	<br><br>

	Adresat: <br>
	Zwrotzalot.pl <br>
	ul. Piłsudskiego 43/3 <br>
	50-032 Wrocław <br>
	Polska <br>
	E-mail: kontakt@zwrotzalot.pl <br><br><br>


	Ja niniejszym informuję o moim odstąpieniu o świadczenie następującej usługi:
	<span class="underline fullwidth"></span>

	<span class="label">Data zamówienia:</span><span class="underline"></span> <br><br>

	<span class="label">Imię i nazwisko konsumenta:</span><span class="underline"></span><br><br>

	<span class="label">Adres konsumenta:</span><span class="underline"></span><br><br>

	<span class="label">Data:</span><span class="underline"></span><br><br><br>
	<span class="signature">
		<span class="underline"></span>
		<br>
		Podpis konsumenta <br>
		(tylko jeżeli formularz jest przesyłany w wersji papierowej)
	</span>

</p>
      </li>
    </ol>
</li>
<li class="term">
<h3>
<span>7.</span> Prywatność
</h3>
<ol class="sub-terms">
<li class="sub-term">
Wszystkie informacje o kliencie są traktowane poufnie zgodnie z obowiązującym prawem UE.
</li>
</ol>
</li>
<li class="term">
<h3>
<span>8.</span> Arbitraż
</h3>
<ol class="sub-terms">
<li class="sub-term">
Wszelkie spory wynikające z niniejszych Warunków i Zasad mogą być zgłaszane do polskiego sądu i podlegają prawu polskiemu oraz prawu międzynarodowemu mającemu zastosowanie do niniejszych warunków umowy.
</li>
</ol>
</li>
</ol>
</div>
EOD;


        $pdf->writeHTML($regulamin_tresc, true, false, false, false, '');

        $pdf->Output('regulamin.pdf', 'I');

    }

    /**
     * @param object $data
     * @return $this
     */
    function setData($data)
    {
        $this->data = $data;
    }

    function setParser($parser)
    {
        $this->parser = $parser;
    }
}