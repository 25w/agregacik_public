<?php


namespace App\Factory\DocType;


use jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF;

class UmowaRamowa implements DocTypeInterface
{
    /**
     * @var TCPDF $parser
     */
    private $parser;
    private $data;

    public function render()
    {

        //FIXME this need to save a file locally!! need for later file md5 sign for carrier (biuro) claim auth

        // get the service
        /** @var TCPDI $pdf */
        $pdf = $this->parser;






        // get the service
        /** @var TCPDF $pdf */
        $pdf = $this->parser;

//        $this->SetSourceFile('logo.pdf');

        // set document information
        $pdf->setCreator(PDF_CREATOR);
        $pdf->setAuthor('');
        $pdf->setTitle($this->data['title']);
        $pdf->setSubject($this->data['subject']);
        $pdf->setKeywords($this->data['keywords']);

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->setMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set font
        $pdf->setFont('times', '', 12);

        $pdf->AddPage();

        // set some text to print
        $txt = <<<EOD
dokument umowy do zrobienia.

ponizej zmienne jakie mozna w pliku uzyc

EOD;
        foreach ($this->data as $datk => $datv) {
            $txt .= <<<EOD
$datk => $datv

EOD;
        }


        // print a block of text using Write()
        $pdf->Write(0, $txt, '', 0, '', true, 0, false, false, 0);

        $pdf->Output('example_002.pdf', 'I');

    }

    /**
     * @param object $data
     * @return $this
     */
    function setData($data)
    {
        $this->data = $data;
    }

    function setParser($parser)
    {
        $this->parser = $parser;
    }
}