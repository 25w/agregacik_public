<?php


namespace App\Factory;



use App\Enum\DocType;
use App\Factory\DocType\Apk;
use App\Factory\DocType\DocTypeInterface;
use App\Factory\DocType\FormularzInformacyjny;
use App\Factory\DocType\Regulamin;
use App\Factory\DocType\Umowa;
use App\Factory\DocType\UmowaRamowa;
use jonasarts\Bundle\TCPDFBundle\TCPDF\TCPDF;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentGeneratorFactory implements DocumentGeneratorInterface
{

    private ContainerInterface $container;
    private DocTypeInterface $instance;
    private $data;
    private $parser;

    public function __construct(
        ContainerInterface $container
    )
    {

        $this->container = $container;
    }

    public function init() {
        return $this;
    }

    public function setTheme($doc_type = 'temp')
    {
        switch ($doc_type) {
            case DocType::APK()->getValue():
                $className = Apk::class;
                //$instance = new Apk();
                break;
            case DocType::REGULAMIN()->getValue():
                $className = Regulamin::class;
                //$instance = new Regulamin();
                break;
            case DocType::UMOWA_ZRZECZENIA()->getValue():
                $className = Umowa::class;
                //$instance = new Umowa();
                break;
            case DocType::UMOWA_RAMOWA()->getValue():
                $className = UmowaRamowa::class;
                //$instance = new UmowaRamowa();
                break;
            case DocType::FORMULARZ_INFORMACYJNY()->getValue():
                $className = FormularzInformacyjny::class;
                //$instance = new FormularzInformacyjny();
                break;
            default:
                $className = Regulamin::class;
            //$instance = new Regulamin();
        }

        $this->instance = new $className;

        return $this;
    }

    function setFormat(string $format)
    {
        if ($format == 'pdf') {
            /** @var TCPDF $parser */
            $parser = $this->container->get('tcpdf');
        } else {
            /** @var TCPDF $parser */
            $parser = $this->container->get('tcpdf');
        }

        $this->parser = $parser;
        return $this;
    }

    /**
     * @param object $data
     * @return $this
     */
    function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    function render()
    {
        $this->instance->setData($this->data);
        $this->instance->setParser($this->parser);
        $this->instance->render();
    }



    /**
     * twig global helper for determine document by type
     *
     * @param $type
     * @return string
     */
    public function getDetermineByType($type)
    {
        if ($type === DocType::APK()->getValue()) {
            return 'Analiza Potrzeb Klienta';
        } elseif ($type === DocType::REGULAMIN()->getValue()) {
            return 'Regulamin';
        } elseif ($type === DocType::UMOWA_ZRZECZENIA()->getValue()) {
            return 'Umowa Zrzeczenia';
        } elseif ($type === DocType::UMOWA_RAMOWA()->getValue()) {
            return 'Umowa Ramowa';
        } elseif ($type === DocType::FORMULARZ_INFORMACYJNY()->getValue()) {
            return 'Formularz Informacyjny';
        }
        return 'Błędny typ';
    }
}