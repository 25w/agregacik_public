<?php

namespace App\Repository;

use App\Entity\CashRegister;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CashRegister|null find($id, $lockMode = null, $lockVersion = null)
 * @method CashRegister|null findOneBy(array $criteria, array $orderBy = null)
 * @method CashRegister[]    findAll()
 * @method CashRegister[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CashRegisterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CashRegister::class);
    }

    /**
    * @return CashRegister[] Returns an array of CashRegister objects
    */
    public function getUserCashRegister($user_id)
    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
        ///?? FIXME cos nie tak z baza - zaprojektuj od nowa bo sie kupy nie trzyma :/
        /// // albo :
        /// select * from cash_register
        //left join cr_volume crv on cash_register.cr_volume_id
        //left join cr_currency crc on crv.cr_currency_id;

        $qb = $this->createQueryBuilder('cr');
        $query = $qb->select('cr, crv, crc')
            ->leftJoin('cr.cr_volume_id', 'crv')
            ->leftJoin('crv.cr_currency_id', 'crc')
            ->where('cr.user_id = '.$user_id)
            ->getQuery();
        $result = $query->getResult(AbstractQuery::HYDRATE_ARRAY);
        return $result;
    }
    

}
