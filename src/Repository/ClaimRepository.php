<?php

namespace App\Repository;

use App\Entity\Claim;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Claim|null find($id, $lockMode = null, $lockVersion = null)
 * @method Claim|null findOneBy(array $criteria, array $orderBy = null)
 * @method Claim[]    findAll()
 * @method Claim[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClaimRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Claim::class);
    }



    // /**
    //  * @return Claim[] Returns an array of Claim objects
    //  */

    public function findAllVerifiedClaimsForCarrier($carrier_id)
    {


        $qb = $this->createQueryBuilder('c');
        $query = $qb->select('c')
            ->leftJoin('c.carrier', 'cc')
            ->where('cc.id = :carrier_id')
            ->setParameter('carrier_id', $carrier_id)
            ->andWhere('c.isVerified = 1')
            ->getQuery();
        $result = $query->getResult();//AbstractQuery::HYDRATE_ARRAY);
        return $result;
    }

    public function findUserClaims($user_id)
    {

        $qb = $this->createQueryBuilder('c');
        $query = $qb->select('c')
            ->leftJoin('c.author', 'cc')
            ->where('cc.id = :author_id')
            ->setParameter('author_id', $user_id)
            ->getQuery();
        $result = $query->getResult(AbstractQuery::HYDRATE_ARRAY);
        return $result;
    }

    /**
     *
     *
     * @param $value
     *
     * @return array
     */
    public function findForSearch($value)
    {
        return $this->createQueryBuilder('c')
            ->select('a.firstName, a.lastName, c.id')
            ->leftJoin('c.author', 'a')
            ->where('a.firstName LIKE :query')
            ->orWhere('a.lastName like :query')
            ->setParameter('query', "%". $value ."%")
            ->getQuery()->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Claim
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
