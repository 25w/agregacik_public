<?php

namespace App\Repository;

use App\Entity\ClaimProcess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClaimProcess|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClaimProcess|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClaimProcess[]    findAll()
 * @method ClaimProcess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClaimProcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClaimProcess::class);
    }

    // /**
    //  * @return ClaimProcess[] Returns an array of ClaimProcess objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClaimProcess
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
