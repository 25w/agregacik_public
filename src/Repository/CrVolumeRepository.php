<?php

namespace App\Repository;

use App\Entity\CrVolume;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CrVolume|null find($id, $lockMode = null, $lockVersion = null)
 * @method CrVolume|null findOneBy(array $criteria, array $orderBy = null)
 * @method CrVolume[]    findAll()
 * @method CrVolume[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CrVolumeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CrVolume::class);
    }

    public function getUserCoinsCount($cash_register_id)//: ?CrVolume
    {
        ///FIXME rebuild and add currency like Currency::POLAND()

        $query = $this->createQueryBuilder('crv')
            ->select('SUM(crv.worth) AS coins')
//            ->leftJoin('crv.cash_register', 'cr')
            ->andWhere('crv.cash_register = :cash_register_id')
            ->setParameter('cash_register_id', $cash_register_id)
            ->getQuery();
        return $query->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    // /**
    //  * @return CashRegister[] Returns an array of CashRegister objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CashRegister
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
