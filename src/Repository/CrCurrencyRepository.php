<?php

namespace App\Repository;

use App\Entity\CrCurrency;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CrCurrency|null find($id, $lockMode = null, $lockVersion = null)
 * @method CrCurrency|null findOneBy(array $criteria, array $orderBy = null)
 * @method CrCurrency[]    findAll()
 * @method CrCurrency[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CrCurrencyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CrCurrency::class);
    }

    // /**
    //  * @return CashRegister[] Returns an array of CashRegister objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CashRegister
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
