<?php

namespace App\Repository;

use App\Entity\UserSensitiveData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserSensitiveData|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSensitiveData|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSensitiveData[]    findAll()
 * @method UserSensitiveData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSensitiveDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSensitiveData::class);
    }

    // /**
    //  * @return UserSensitiveData[] Returns an array of UserSensitiveData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSensitiveData
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
