<?php

namespace App\Repository;

use App\Entity\WatchDog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WatchDog|null find($id, $lockMode = null, $lockVersion = null)
 * @method WatchDog|null findOneBy(array $criteria, array $orderBy = null)
 * @method WatchDog[]    findAll()
 * @method WatchDog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WatchDogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WatchDog::class);
    }

    // /**
    //  * @return WatchDog[] Returns an array of WatchDog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WatchDog
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
