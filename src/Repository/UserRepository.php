<?php

namespace App\Repository;

use App\Entity\User;
use App\Enum\UserType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getListByType($type = null, $select = null)
    {
        if (empty($type)) {
            $type = UserType::USER()->getValue();
        }
        if (empty($select)) {
            $select = 'u';
        }

        $q = $this->createQueryBuilder('u')
            ->select($select)
            ->where('u.type = :type')
            ->setParameter('type', $type)
            ->getQuery();
        return $q->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function getIdListByType($type = null, $select = 'u.id')
    {
        $array = $this->getListByType($type, $select);
        $list = [];
        foreach ($array as $c) {
            $list[] = $c['id'];
        }
        return $list;
    }

//    public function findAllVerifiedClaimsForCarrier($carrier_id)
//    {
//
//
//        $qb = $this->createQueryBuilder('u');
//        $query = $qb->select('u, um, c')
//            ->leftJoin('u.userMeta', 'um')
//                ->where('um.name = :name')
//                ->setParameter(':name', 'is_currier')
//                ->andWhere('um.value = :value')
//                ->setParameter(':value', '1')
//            ->andWhere('u.id = :carrier')
//            ->setParameter(':carrier', $carrier_id)
//            ->leftJoin('u.claim', 'c')
////            ->andWhere('c = :is_verified')
////            ->setParameter(':is_verified', '1')
//            ->getQuery();
//        $result = $query->getResult(AbstractQuery::HYDRATE_ARRAY);
//        print_r($result); die();
//        return $result;
//    }

}
