<?php

namespace App\Manager;

use App\Utils\Model\File;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @class PublicFilesManager
 * This manager is used to provide functionality to upload and use user's files.
 * @package App\Manager
 */
class PublicFilesManager
{
    protected $dir;
    protected $webDirectory;
    protected $secretDirectory;
    private $slugger;
    private $logger;

    private $file_type;

    const FILE_COMPLAIN = 1;
    const FILE_OFFER = 2;

    public function __construct($dir, SluggerInterface $slugger, LoggerInterface $logger)
    { //'%kernel.project_dir%/public/complains', '%kernel.project_dir%/public/'
        $this->dir = $dir . '/public/complains';

        $this->complainsDirectory = $dir . '/public/complains';
        $this->offerDirectory = $dir . '/public/offers';
        $this->webDirectory = $dir . '/public/';
        $this->secretDirectory = $dir . '/private/';
        $this->slugger = $slugger;
        $this->logger = $logger;
        $this->setFileType(self::FILE_COMPLAIN);
    }

    public function getDirectory()
    {
        if ($this->getFileType() == self::FILE_COMPLAIN) {
            $this->dir = $this->complainsDirectory;
        } else {
            $this->dir = $this->offerDirectory;
        }
        return $this->dir;
    }

    public function getPublicDirectory()
    {
        return str_replace(
            realpath($this->webDirectory),
            '',
            realpath($this->dir)
        );
    }

    public function setFileType($type = self::FILE_COMPLAIN)
    {
        $this->file_type = $type;
    }

    public function getFileType()
    {
        return $this->file_type;
    }

    public function transformToPublicPath($name)
    {
        return rtrim($this->getPublicDirectory(), '/') . '/' . $name;
    }

    public function nameToPath($name)
    {
        return realpath(rtrim($this->getDirectory()) . '/' . $name);
    }

    /**
     * @return File[]
     */
    public function getFiles()
    {
        return array_filter(array_map(function ($e) {
            if ($e && substr($e, 0, 1) == '.') {
                return false;
            }

            return new File(rtrim($this->getDirectory(), '/') . '/' . $e, $this->transformToPublicPath($e));
        }, scandir($this->getDirectory())));
    }

    public function exists($name)
    {
        foreach ($this->getFiles() as $file) {
            if (strtolower($file->getName()) == strtolower($name)) {
                return true;
            }
        }

        return false;
    }

    public function upload(UploadedFile $file)
    {
//        $i = 0;
//        do {
//            $name = $file->getClientOriginalName();
//
//            if ($i) {
//                $name = substr_replace($name, ".$i", strrpos($name, '.'), 0);
//            }
//
//            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
//            $safeFilename = $this->slugger->slug($originalFilename);
//            $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
//
//            ++$i;
//        } while ($this->exists($name));

        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        //FIXME add VALIDATORS??????????????????????

        $uploadResult = $file->move($this->getDirectory(), $fileName);
        if (empty($uploadResult->getPathName()) || empty($uploadResult->getFileName)) {
            $this->logger->warning('File not attached to Claim.');
        }

        return $uploadResult;
    }

    public function delete($name)
    {
//        if ($this->exists($name)) {
//            unlink($this->nameToPath($name));
//        }
    }
}
