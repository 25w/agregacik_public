<?php

namespace App\Command;

use App\Entity\CashRegister;
use App\Entity\CrVolume;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * Class getDemoDataCommand
 * @package App\Command
 * @deprecated use data fixtures instead
 */
class GetDemoDataCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:make:data:demo';
    /** @var ContainerInterface $container */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('make demo data.')
            ->setHelp('This command allows you to make demo data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Make demo data',
            '============',
            '',
        ]);

        $output->writeln('Started ...');

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        $user = $em->getRepository(User::class)->findOneBy(['id' => 1]);// deault admin

        $cashRegister = $em->getRepository(CashRegister::class)->findOneBy(['userId' => $user->getId()]);
        if (empty($cashRegister)) {
            $cashRegister = new CashRegister();
            $cashRegister->setUserId($user);
            $em->persist($cashRegister);
        }

        foreach ([10, 20, 30] as $item) {
            $crVolume = new CrVolume();
            $crVolume->setCashRegister($cashRegister);
            $crVolume->setWorth($item);
            $em->persist($crVolume);
            unset($crVolume);
        }

        $em->flush();

        $output->writeln('Job done!');
        return 0;
    }
}
