<?php

namespace App\Command;

use App\Entity\ApiUser;
use App\Entity\User;
use App\Entity\UserMeta;
use App\Service\PublicPrivateKeyService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This command make Users for basic usage auth.
 *
 * Class MakeAuthUsersCommand
 * @package App\Command
 * @deprecated use data fixtures instead
 */
class MakeAuthUsersCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:make:auth:users';
    /** @var ContainerInterface $container */
    private $container;
    private $encoder;
    private PublicPrivateKeyService $publicPrivateKeyService;

    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $encoder, PublicPrivateKeyService $publicPrivateKeyService)
    {
        parent::__construct();
        $this->container = $container;
        $this->encoder = $encoder;
        $this->publicPrivateKeyService = $publicPrivateKeyService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create demo users for users entity.')
            ->setHelp('This command allows you to make dame users');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Make demo users',
            '============',
            '',
        ]);

        $output->writeln('Started ...');


        $demoUsers = [
            [
                'username' => 'crxadmin',
                'name' => 'CRX ADMIN',
                'email' => 'admin@25w.pl',
                'password' => 'admin',
                'role' => 'ROLE_ADMIN',
                'active' => true,
            ],
            [
                'username' => 'crxklient',
                'name' => 'CRX klient',
                'email' => 'klient@25w.pl',
                'password' => 'klient',
                'role' => 'ROLE_USER',
                'active' => true,
            ],
            [
                'username' => 'crxbiuro',
                'name' => 'CRX biuro',
                'email' => 'biuro@25w.pl',
                'password' => 'biuro',
                'role' => 'ROLE_CARRIER', // biuro, przewoznik etc
                'active' => true,
                'company_name' => 'np biuro LOT',
                'meta' => [
                    'is_currier' => 1,
                    'address' => 'adres biura'
                ]
            ],
            [
                'username' => 'crxbiuro2',
                'name' => 'CRX biuro2',
                'email' => 'biuro2@25w.pl',
                'password' => 'biuro2',
                'role' => 'ROLE_CARRIER', // biuro, przewoznik etc
                'active' => true,
                'company_name' => 'np biuro LOT2',
                'private_key' => '',
                'public_key' => '',
                'meta' => [
                    'is_currier' => 1,
                    'address' => 'adres biura2'
                ]
            ],
            [
                'username' => 'crxpracownik',
                'name' => 'CRX pracownik',
                'email' => 'pracownik@25w.pl',
                'password' => 'pracownik',
                'role' => 'ROLE_WORKER', // pracownik aplikacji
                'active' => true,
            ],
        ];

        $apiUsers = [
            [
                'username' => 'crxapi',
                'name' => 'CRX API',
                'email' => 'apiuser@25w.pl',
                'password' => 'apiuser',
                'role' => 'ROLE_API',
                'active' => true,
                'api_secret' => bin2hex(random_bytes(32))
            ],
        ];

        if (empty($demoUsers)) {
            echo 'No data';
            return 1;
        }

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        foreach ($demoUsers as $demoUser) {

            if (! $demoUser['active']) {
                continue;
            }

            $user = $em->getRepository(User::class)->findOneBy(['login' => $demoUser['username']]);
            if ($user === null) {
                $user = new User();
            }
            $user->setLogin($demoUser['username']);
            $user->setPassword($this->encoder->encodePassword($user, $demoUser['password']));
            $user->setFirstName($demoUser['name']);
            $user->setEmail($demoUser['email']);
            $user->setRoles([$demoUser['role']]);

            if ($demoUser['role'] == 'ROLE_CARRIER') {
                foreach ($demoUser['meta'] as $meta_key => $meta_value) {
                    $metaEnt = new UserMeta();
                    $metaEnt->setName($meta_key);
                    $metaEnt->setValue($meta_value);
                    $user->addUserMeta($metaEnt);
                }
                $user->setCompanyName($demoUser['company_name']);

                if (empty($user->getPublicKey())) {
//                    /** @var PublicPrivateKeyService $publicprivatekey */
//                    $publicprivatekey = $this->container->get('app.service.publicprivatekey');
                    list($public_key, $private_key) = $this->publicPrivateKeyService->generateBothKeyPair();

                    $user->setPublicKey($public_key);
                    $user->setPrivateKey($private_key);
                }
            }

            $em->persist($user);

            $output->writeln('User ... '.$demoUser['username'].' added.');
        }
        foreach ($apiUsers as $apiUser) {

            if (! $apiUser['active']) {
                continue;
            }

            $api = $em->getRepository(ApiUser::class)->findOneBy(['username' => $apiUser['username']]);
            if ($api === null) {
                $api = new ApiUser();
            }
            $api->setUsername($apiUser['username']);
            $api->setPassword($apiUser['password']);
            $api->setEmail($apiUser['email']);
            $api->setApiSecret($apiUser['api_secret']);
            $api->setRoles([$apiUser['role']]);

            $em->persist($api);

            $output->writeln('API User ... '.$apiUser['username'].' added.');
        }
        $em->flush();

        $output->writeln('Job done!');
        return 0;
    }
}