<?php
//see https://medium.com/@galopintitouan/executing-database-migrations-at-scale-with-symfony-and-doctrine-4c60f86865b4

namespace App\Command;

//use Doctrine\Bundle\MigrationsBundle\Command\DoctrineCommand;
//use Doctrine\Bundle\MigrationsBundle\Command\Helper\DoctrineCommandHelper;
//use Psr\SimpleCache\CacheInterface;
//use Symfony\Component\Console\Input\InputInterface;
//use Symfony\Component\Console\Input\InputOption;
//use Symfony\Component\Console\Output\OutputInterface;
//use Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand;
//use Symfony\Component\Console\Style\SymfonyStyle;

class MigrateDatabaseCommand //extends MigrateCommand
{
//    protected static $defaultName = 'app:db:migrate';
//
//    private $cache;
//
//    public function __construct(CacheInterface $cache)
//    {
//        parent::__construct();
//
//        $this->cache = $cache;
//    }
//
//    protected function configure()
//    {
//        parent::configure();
//
//        $this
//            ->setDescription('Check if a database migration is needed and enable maintenance mode to execute it if there is.')
//            ->addOption('db', null, InputOption::VALUE_REQUIRED, 'The database connection to use for this command.')
//            ->addOption('em', null, InputOption::VALUE_REQUIRED, 'The entity manager to use for this command.')
//            ->addOption('shard', null, InputOption::VALUE_REQUIRED, 'The shard connection to use for this command.')
//        ;
//    }
//
//    public function execute(InputInterface $input, OutputInterface $output)
//    {
//        $io = new SymfonyStyle($input, $output);
//
//        if ($this->cache->get('migrating')) {
//            $io->success('Another instance locked the migration');
//
//            return;
//        }
//
//        // Take the responsibility to execute the migration
//        $io->text('Locking');
//        $this->cache->set('migrating', true, 60);
//        $io->text('Lock obtained');
//
//        // Check whether there are migrations to execute or not
//        $io->text('Loading migrations');
//
//        $app = $this->getApplication();
//        DoctrineCommandHelper::setApplicationHelper($app, $input);
//
//        DoctrineCommand::configureMigrations(
//            $app->getKernel()->getContainer(),
//            $this->getMigrationConfiguration($input, $output)
//        );
//
//        $configuration = $this->getMigrationConfiguration($input, $output);
//        $toExecute = array_diff(
//            $configuration->getAvailableVersions(),
//            $configuration->getMigratedVersions()
//        );
//
//        if (!$toExecute) {
//            // No migration to execute: do not enable maintenance
//            $io->success('No migration to execute');
//
//            $io->text('Releasing lock');
//            $this->cache->delete('migrating');
//            $io->text('Lock released');
//
//            return;
//        }
//
//        // Migrations to execute: enable maintenance and run them
//        $io->text('Migration(s) to execute: '.implode(', ', $toExecute));
//
//        $io->text('Enabling maintenance mode');
//        $this->cache->set('maintenance', true, 60);
//        $io->text('Maintenance enabled');
//
//        $io->text("Executing the migration(s)\n");
//
//        // Enable full output and disable the migration question
//        $output->setVerbosity(OutputInterface::VERBOSITY_DEBUG);
//        $input->setOption('no-interaction', true);
//
//        parent::execute($input, $output);
//
//        $output->write("\n");
//        $io->text('Migration(s) executed');
//
//        $io->text('Disabling maintenance mode');
//        $this->cache->delete('maintenance');
//        $io->text('Maintenance disabled');
//
//        $io->text('Releasing lock');
//        $this->cache->delete('migrating');
//        $io->success('Lock released');
//    }
}