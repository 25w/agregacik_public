<?php

namespace App\Command;

use App\Agregacik\OperationBundle\Enum\Currency;
use App\Entity\Airline;
use App\Entity\Airport;
use App\Entity\CashRegister;
use App\Entity\City;
use App\Entity\Country;
use App\Entity\CrCurrency;
use App\Entity\CrVolume;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * Class GetDictDataCommand
 * @package App\Command
 */
class GetDictDataCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:make:data:dict';
    /** @var ContainerInterface $container */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('make dict data.')
            ->setHelp('This command allows you to make dict data.');
    }

    private function super_unique($array,$key)
    {
        $temp_array = [];
        foreach ($array as &$v) {
            if (!isset($temp_array[$v[$key]]))
                $temp_array[$v[$key]] =& $v;
        }
        $array = array_values($temp_array);
        return $array;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Make dict data',
            '============',
            '',
        ]);

        $output->writeln('Started ...');

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();


        //make airport database
        // import airlines

        // entity Country
        $output->writeln('Entity Country ...');
        $countryDict = array_map('str_getcsv', file(__DIR__ . '/../../private/dict/countries.dat'));
        foreach ($countryDict as $countryRow) {
            $country = $em->getRepository(Country::class)->findOneBy(['name' => $countryRow[0]]);
            if (empty($country)) {
                $country = new Country();

                /*
                    name 	    Full name of the country or territory.
                    iso_code 	Unique two-letter ISO 3166-1 code for the country or territory.
                    dafif_code 	FIPS country codes as used in DAFIF. Obsolete and primarily of historical interested.
                 */
                $country->setName($countryRow[0]);
                $country->setIsoCode($countryRow[1]);
                $country->setDafifCode($countryRow[2]);
                $em->persist($country);
            }
        }
        $em->flush();
        $output->writeln('Entity Country Done!');

        //print_r($countryDict);

        // entity City
        $output->writeln('Entity City ...');
        $airportsDict = array_map('str_getcsv', file(__DIR__ . '/../../private/dict/airports.dat'));
        $airlinesDictUnique = $this->super_unique($airportsDict,2);
        foreach ($airlinesDictUnique as $airportsRow) {
            $city = $em->getRepository(City::class)->findOneBy(['name' => $airportsRow[2]]);
            if (empty($city)) {
                $city = new City();

                /*
                    Airport ID 	Unique OpenFlights identifier for this airport.
                    Name 	    Name of airport. May or may not contain the City name.
                    City 	    Main city served by airport. May be spelled differently from Name.
                    Country 	Country or territory where airport is located. See Countries to cross-reference to ISO 3166-1 codes.
                    ...         ...
                */
                $city->setName($airportsRow[2]);

                $country = $em->getRepository(Country::class)->findOneBy(['name' => $airportsRow[3]]);
                $city->setCountry($country);
                $em->persist($city);

            }
        }
        $em->flush();
        $output->writeln('Entity City Done!');

        // entity Airports
        $output->writeln('Entity Airport ...');
        foreach ($airportsDict as $airportsRow) {
//            print_r($airportsRow);
            $airport = $em->getRepository(Airport::class)->findOneBy(['name' => $airportsRow[1]]);
            if (empty($airport)) {
                $airport = new Airport();

                /*
                    Airport ID 	Unique OpenFlights identifier for this airport.
                    Name 	    Name of airport. May or may not contain the City name.
                    City 	    Main city served by airport. May be spelled differently from Name.
                    Country 	Country or territory where airport is located. See Countries to cross-reference to ISO 3166-1 codes.
                    IATA 	    3-letter IATA code. Null if not assigned/unknown.
                    ICAO 	    4-letter ICAO code. Null if not assigned.
                    Latitude 	Decimal degrees, usually to six significant digits. Negative is South, positive is North.
                    Longitude 	Decimal degrees, usually to six significant digits. Negative is West, positive is East.
                    Altitude 	In feet.
                    Timezone 	Hours offset from UTC. Fractional hours are expressed as decimals, eg. India is 5.5.
                    DST 	    Daylight savings time. One of E (Europe), A (US/Canada), S (South America), O (Australia), Z (New Zealand), N (None) or U (Unknown). See also: Help: Time
                    Tz 	        Timezone in "tz" (Olson) format, eg. "America/Los_Angeles".
                    Type 	    Type of the airport. Value "airport" for air terminals, "station" for train stations, "port" for ferry terminals and "unknown" if not known. In airports.csv, only type=airport is included.
                    Source 	    Source of this data. "OurAirports" for data sourced from OurAirports, "Legacy" for old data not matched to OurAirports (mostly DAFIF), "User" for unverified user contributions. In airports.csv, only source=OurAirports is included.
                 */
                $airport->setName($airportsRow[1]);

                $country = $em->getRepository(Country::class)->findOneBy(['name' => $airportsRow[3]]);
                $city = $em->getRepository(City::class)->findOneBy(['name' => $airportsRow[2]]);
                if (empty($city)) {
                    $city = new City();
                    $city->setName($airportsRow[2]);

                    $city->setCountry($country);
                    $em->persist($city);
                }

                $airport->setCity($city);
                $airport->setCountry($country);
                $airport->setIata($airportsRow[4]);
                $airport->setIcao($airportsRow[5]);
                $airport->setLatitude($airportsRow[6]);
                $airport->setLongitude($airportsRow[7]);
                $airport->setAltitude($airportsRow[8]);
                $airport->setTimezone($airportsRow[9]);
                $airport->setDst($airportsRow[10]);
                $airport->setTz($airportsRow[11]);
                $airport->setType($airportsRow[12]);
                $airport->setSource($airportsRow[13]);
                $em->persist($airport);
            }
        }
        $em->flush();
        $output->writeln('Entity Airport Done!');

        // entity Airlines
        $output->writeln('Entity Airline ...');
        $airlinesDict = array_map('str_getcsv', file(__DIR__ . '/../../private/dict/airlines.dat'));
        foreach ($airlinesDict as $airlineRow) {
            $airline = $em->getRepository(Airline::class)->findOneBy(['name' => $airlineRow[1]]);
            if (empty($airline)) {
                $airline = new Airline();
            }
            /*
                Airline ID 	Unique OpenFlights identifier for this airline.
                Name 	    Name of the airline.
                Alias 	    Alias of the airline. For example, All Nippon Airways is commonly known as "ANA".
                IATA 	    2-letter IATA code, if available.
                ICAO 	    3-letter ICAO code, if available.
                Callsign 	Airline callsign.
                Country 	Country or territory where airport is located. See Countries to cross-reference to ISO 3166-1 codes.
                Active 	    "Y" if the airline is or has until recently been operational, "N" if it is defunct. This field is not reliable: in particular, major airlines that stopped flying long ago, but have not had their IATA code reassigned (eg. Ansett/AN), will incorrectly show as "Y".
             */
            $airline->setName($airlineRow[1]);
            $airline->setAlias($airlineRow[2]);
            $airline->setIata($airlineRow[3]);
            $airline->setIcao($airlineRow[4]);
            $airline->setCallsign($airlineRow[5]);
            $airline->setActive(($airlineRow[7] === 'Y') ? true : false);
            $country = $em->getRepository(Country::class)->findOneBy(['name' => $airlineRow[6]]);
            $airline->setCountry($country);
            $em->persist($airline);
        }
        $em->flush();
        $output->writeln('Entity Airline Done!');


        // entity Currency
        $output->writeln('Entity Currency ...');
        $currencyDict = [
            ['name' => 'PLN'],
            ['name' => 'EUR'],
            ['name' => 'USD'],
        ];
        foreach ($currencyDict as $currencyRow) {
            $currency = $em->getRepository(CrCurrency::class)->findOneBy(['name' => $currencyRow['name']]);
            if (empty($currency)) {
                $currency = new CrCurrency();
            }

            $currency->setName($currencyRow['name']);
            $em->persist($currency);
        }
        $em->flush();
        $output->writeln('Entity Currency Done!');

        $output->writeln('Job done!');
        return 0;
    }
}
