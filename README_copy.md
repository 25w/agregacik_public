start project with:
`symfony server:start`

check routes with:
`php bin/console debug:router`


###Database:
create database `php bin/console doctrine:database:create`

update schema
 - CHECK `php bin/console doctrine:schema:update --dump-sql`
 - RUN `php bin/console doctrine:schema:update --force`
 
 
### Translations
 
Update all en strings `php bin/console translation:update en --force`
Show all en strings `php bin/console translation:update en --dump-messages`

### Cron & Sync

Cron jobs can be listed in DB table `cron_job` or by command  `php bin/console cron:list`
Jobs are stored in `src/Command` folder.

FIXME - For every env you need to add manually all crons to `cron_job` table

To create new job use `php bin/console cron:create`

Synchronisation is made by cron jobs:
 - Sync Users from Shopware `src/SyncShopwareUsersCommand.php`
 
    Sync Shopware5 Users NOW:
    `bin/console cron:run SyncShopwareUsers --schedule_now`
    
## Sync tasks:
 -  bin/console app:sync:shopware:users
 -  bin/console app:sync:shopware:requirements
 -  bin/console app:sync:shopware:products
 -  bin/console app:sync:shopware:orders
 
    
### Vuejs on front

- Run Encore by `npm run dev-server` OR `npm run watch`


### Setup on server

Get code from repository by clone it
`git clone git@gitlab.com:mluebcke/erpmysofabed.git`

Install composer dependencies
`composer install`

Install npm dependencies
`npm install && npm run-script build`

Set proper permissions on folders
`chmod 777 -R var`
`chmod 775 public`

After installation file .env should contain all ready to use required credentials.
If not then change file, push it to repo and pull on server to get changed file.
What you should check:
- DATABASE_URL <- value contain all db credentials
- BASE_URL <- base url for project
- SHOPWARE_* <- there are 4 constants responsible for API access to sw5.
- SHOPWARE_DB_* <- there are 4 constants responsible for DB access to sw5.

BELLOW IS REQUIRED ONLY FIRST SETUP TIME:
 - Create DB by  `php /bin/console doctrine:database:create`
 - Recreate db schema `php bin/console doctrine:schema:create`
 - Sync user from sw `php bin/console app:sync:shopware:users -vvv`
 - Sync required data from sw `php bin/console app:sync:shopware:requirements -vvv`
 - Sync products data from sw `php bin/console app:sync:shopware:products -vvv`
 - Sync orders data from sw `php bin/console app:sync:shopware:orders -vvv`

Also can be done by one command 
`php /bin/console doctrine:database:create &&
php bin/console doctrine:schema:create &&
bin/console app:sync:shopware:users -vvv &&
bin/console app:sync:shopware:requirements -vvv &&
bin/console app:sync:shopware:products -vvv &&
bin/console app:sync:shopware:orders -vvv`

All changes that are made in repo will be deployed by deploy HQ - this should get actual code from repository to server.


### Setup locally

Is similar to deploy above but after setup we can run build in symfony web server and npm for vue:
`symfony server:start` Detailed information can be found here https://symfony.com/doc/current/setup/symfony_server.html
`npm run watch`

Also copy .env file to .env.local, change credentials to local ones 
(do not add it to repo - it should be ignored and live only on local setup) 
more information here - https://symfony.com/doc/current/configuration.html#overriding-environment-values-via-env-local

Now access to http://127.0.0.1:8000/ should display login page, use credentials from sw to login.

# Instruction how to change hermes export and csv file.

First check .env for HERMES_EXPORT_HES_* constants and change for actual ones. 

In file src/Controller/DeliveryController.php there are two methods 
`getHermesCsvDownload` and `sendHermesCsvExport` both use same CSV generation logic but first send
output to browser to download and second sends that CSV to HES ftp server.
Both also use csvExportService service to call getHermesExport method that require bellow params:
- $ids array of order ids that we want to export from
- RiconCustomOrderService that is required for CSV generation logic - here to get CSV content
- $type - can be one of HERMES_EXPORT_TYPE_DOWNLOAD_CSV to download and HERMES_EXPORT_TYPE_EXPORT_CSV to export

In file `src/Service/CsvExportService.php` there is method `uploadWriteCsv` that sends CSV file to HES ftp server
for now is commented to avoid problems - line below line with comment `FIXME enable after testing`.

To change CVS structure for both above functionalities see `RiconCustomOrderService` service 
in file `src/Service/RiconCustomOrderService.php`
Logic is same as in Sw - look at `createQueryFilterWhere, getOrdersByFilter, getHeader and getContent`
Those are responsible for CSV structure.

### If there will be errors like countryID is null then run in mysql this commands:
`use mysofabederp_database;
alter table s_order_billingaddress modify countryID int(11);
alter table s_order_shippingaddress modify countryID int(11);`

##Bellow is for dev only - can be ignored, will be removed in future.

Recreate all on server:
`/opt/plesk/php/7.3/bin/php bin/console doctrine:schema:drop --force && 
/opt/plesk/php/7.3/bin/php bin/console doctrine:schema:create && 
/opt/plesk/php/7.3/bin/php bin/console app:sync:shopware:users -vvv && 
/opt/plesk/php/7.3/bin/php bin/console app:sync:shopware:requirements -vvv && 
/opt/plesk/php/7.3/bin/php bin/console app:sync:shopware:products -vvv && 
/opt/plesk/php/7.3/bin/php bin/console app:sync:shopware:orders -vvv`

Recreate all on local:
`php /bin/console doctrine:drop --force &&
 php bin/console doctrine:schema:create &&
 bin/console app:sync:shopware:users -vvv &&
 bin/console app:sync:shopware:requirements -vvv &&
 bin/console app:sync:shopware:products -vvv &&
 bin/console app:sync:shopware:orders -vvv`