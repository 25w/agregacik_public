start project with:
`symfony server:start`

check routes with:
`php bin/console debug:router`


###Database:
create database `php bin/console doctrine:database:create`

update schema
 - CHECK `php bin/console doctrine:schema:update --dump-sql`
 - RUN `php bin/console doctrine:schema:update --force`
 
 
### Translations
 
Update all en strings `php bin/console translation:update en --force`
Show all en strings `php bin/console translation:update en --dump-messages`

### Cron & Sync

Cron jobs can be listed in DB table `cron_job` or by command  `php bin/console cron:list`
Jobs are stored in `src/Command` folder.

FIXME - For every env you need to add manually all crons to `cron_job` table

To create new job use `php bin/console cron:create`

Synchronisation is made by cron jobs:
 - Sync Users from Shopware `src/SyncShopwareUsersCommand.php`
 
    Sync Shopware5 Users NOW:
    `bin/console cron:run SyncShopwareUsers --schedule_now`
    
## Migrations

After create of new entity run:
`./bin/console make:migration`

after that insert schema to db with:
`php bin/console doctrine:migrations:migrate`

### Setup on build server ex. DeployHQ

Get code from repository by clone it
`git clone `

Install composer dependencies
`composer install`

Install npm dependencies
`npm install && npm run-script build`

Set proper permissions on folders
`chmod 777 -R var`
`chmod 775 public`

build front files
`./node_modules/.bin/encore production`

disable sql_mode=only_full_group_by !!!

only first time create db
`./bin/console doctrine:database:create`

run migrations 
`php bin/console doctrine:migrations:migrate`

add demo data
`php bin/console doctrine:fixtures:load` // this will add demo data with users add  --append if want to add something new
`php bin/console app:make:data:dict` // this will add dict data
or on dhosting:
`php74 -d memory_limit=2G bin/console doctrine:fixtures:load`
`php74 -d memory_limit=2G bin/console app:make:data:dict`

All changes that are made in repo will be deployed by deploy HQ - this should get actual code from repository to server.

`
From deployHQ

    Composer install
    composer install --no-progress --no-dev --optimize-autoloader
    
    NPM Build
    npm install --quiet npm run build
   
    ./node_modules/.bin/encore production
    ./node_modules/.bin/encore production
   
    chmod var public
    chmod 777 -R var chmod 775 public
 
    symfony commands
    APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear

`

### stage setup
see https://simonrjones.net/2019/05/staging-environments-symfony-4/
see https://symfony.com/doc/current/configuration.html
make .env.local on stage server to run as stage ...

### Setup locally

Is similar to deploy above but after setup we can run build in symfony web server and npm for vue:
`symfony server:start` Detailed information can be found here https://symfony.com/doc/current/setup/symfony_server.html
`npm run watch`

Also copy .env file to .env.local, change credentials to local ones 
(do not add it to repo - it should be ignored and live only on local setup) 
more information here - https://symfony.com/doc/current/configuration.html#overriding-environment-values-via-env-local

Now access to http://127.0.0.1:8000/ should display login page, use credentials from sw to login.


Recreate all on local:
`php bin/console doctrine:schema:drop --force &&
 php bin/console doctrine:schema:create &&

 php bin/console doctrine:fixtures:load // this will add demo data with users
 
 php bin/console app:make:data:dict -vvv // this will add dict data
 
 AUTH API by token
   
  1. Api Entity has credentials only for api calls so to use api calls we need to create api user with username and api_secret.
   
  2. To get token auth send login and secret to api/get_access: (where l is username and s is api_secret)
  curl -X POST -H "Content-Type: application/json" -d '{"l":"crxapi","s":"GET_FROM_DB_ITS_GENERATED"}' localhost:8000/api/get_access
  like:
  curl -X POST -H "Content-Type: application/json" -d '{"l":"crxapi","s":"26058653984587293aaf10c57c6140816bc97064aae8f320355299948b3b8486"}' localhost:8000/api/get_access
  
  php74 bin/console doctrine:query:sql 'SELECT api_secret from apiuser'
  
  3. Send token in auth header in every api call action
  curl -H "Authorization: Bearer e7d0c7ae3b4ac421f457cf977377db92d63cce86c0d0ef896ec1ec904e5409f9e6c2987e135a124cbdb4943075943b5e81fb5d4347a8cfa103d3ac4d" http://localhost:8000/api/send_claim

Wniosek z plikiem:

pliki:
curl -H "Authorization: Bearer 5ac2ecb036125963337725775dc4b60490308bee052d0e1b9d560c1646efff77538a41efea4977d360d4d70f4b9a48809878f432eca96a7b3ace4f85" -F 'file1=@public/testing_img.jpg' -F 'file2=@public/testing_img.jpg' http://localhost:8000/api/send_claim

data wniosek odwolanie:
curl -H "Authorization: Bearer 5ac2ecb036125963337725775dc4b60490308bee052d0e1b9d560c1646efff77538a41efea4977d360d4d70f4b9a48809878f432eca96a7b3ace4f85" -d "name=mojeimie&surname=mojenazwisko&email=email@www.pl&type=3"  http://localhost:8000/api/send_claim

 
TODO think about assets : php bin/console assets:install --symlink public

 
 
 ### loty informacje
 
 REALNE dane publicznie dostępne: https://www.flightradar24.com/data/flights/lo3848#254aaa75 
 
 odowlane i poznione loty: https://givt.com/pl/airlines/fr-opoznione-odwolane-loty/
 
 
 INFO:
 Opozniony lot: https://prawopasazera.pl/opozniony-lot,1159.html
 Odwolany lot: https://prawopasazera.pl/odwolany-lot,1160.html
 Odmowa wejscia na poklad: https://prawopasazera.pl/odmowa-przyjecia-na-poklad,1161.html
 
 Realnie opóźniony/odwolany lot:
 https://www.flightradar24.com/data/flights/fr6375
 https://www.flightradar24.com/data/flights/lo784
 
 https://www.citizensadvice.org.uk/consumer/holiday-cancellations-and-compensation/if-your-flights-delayed-or-cancelled/
 https://www.airhelp.com/en/blog/flight-delayed-what-to-do/
 https://givt.com/en/airlines/lo-delayed-cancelled-flights/#compensation-rules
 
  https://claimflights.pl/jak-sprawdzic-czy-lot-jest-opozniony/
  https://claimflights.pl/odszkodowanie-za-opozniony-lot/

  
  np:
  https://forms.claimflights.pl/services_api/api/v1/variflight/?api_key=cf-2019-1228-9c7b&flightNo=W61963&flightDate=10.07.2016
  https://flightaware.com/live/flight/WZZ1963
  
  
  