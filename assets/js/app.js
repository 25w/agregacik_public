/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import '../css/app.scss';

import 'bootstrap';

import $ from 'jquery';
global.$ = global.jQuery = $;

import 'bootstrap-datepicker';

require("typeahead.js/dist/typeahead.jquery.min.js")

window.Bloodhound = require('bloodhound-js');

import 'select2';
import 'select2-bootstrap-css/select2-bootstrap.min.css';

import '../../public/bundles/tetranzselect2entity/js/select2entity.js'

import {initDatepicker} from "./modules/datepicker";
import {initTypeaheadWithBloodhound} from "./modules/typeahead";
import {initFlightradar24LinkInClaimValidation} from "./modules/claim-form-ticket-fight-no-link";

$(document).ready(function() {

    initDatepicker();

    initTypeaheadWithBloodhound();

    initFlightradar24LinkInClaimValidation();

});
