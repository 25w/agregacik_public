import $ from "jquery";

export {
    initTypeaheadWithBloodhound
};

function initTypeaheadWithBloodhound() {

    var engine = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        remote: {
            // url: 'http://localhost:8000/worker/claim/search/QUERY',
            url: document.querySelector('.js-user-claim-search').dataset.url,
            wildcard: 'QUERY'
        }
    });

    $('#typeaheader').typeahead(
        {
            highlight: true,
            minLength: 3
        },
        {
            name: 'term',
            source: engine,
            limit: 10,
            templates: {
                empty: '<div class="suggestion"><span class="suggestion_text no-underline"> - brak wyników - </span></div>',
                suggestion: function (el) {
                    return '<a class="suggestion_text no-underline" href="'+el.url+'"><div class="suggestion">' + el.firstName + ' ' + el.lastName + '</div></a>'
                }
            }
        }

    );
}