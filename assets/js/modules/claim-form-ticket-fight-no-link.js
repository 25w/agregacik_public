import $ from "jquery";

export {
    initFlightradar24LinkInClaimValidation
};

function initFlightradar24LinkInClaimValidation() {
    let claimTicketNo = $('#app_claim_process_step_two_collect_data_ticket_flight_no').val();
    if (claimTicketNo) {
        $('#js_placeholder_claim_link_flightradar24')
            .html('<a target="_blank" href="https://www.flightradar24.com/data/flights/'+claimTicketNo+'">' +
                'Zobacz tutaj więcej na temat wprowadzonego lotu. ' +
                '</a>'
            );
    }
    $('input#app_claim_process_step_two_collect_data_ticket_flight_no').on('change', function () {
        $('#js_placeholder_claim_link_flightradar24')
            .html('<a target="_blank" href="https://www.flightradar24.com/data/flights/'+this.value+'">' +
                'Zobacz tutaj więcej na temat wprowadzonego lotu. ' +
                '</a>'
            );
    });
}