
Useful symfony commands

With the some of the built-in commands below, you can use --env=environment_name flag for specific environments like dev, test so on. Also --em=entity_manager_name for different database schemas can be used.

List all available commands

    php app/console -t


List all available commands for given service

    php app/console list doctrine


Show info about given command

    php app/console help doctrine:database:create


Doctrine ORM schema validation

    php app/console doctrine:schema:validate


Doctrine ORM mapping info

    php app/console doctrine:mapping:info


Run local server on http://localhost:8000

    php app/console server:run


List all available services

    php app/console debug:container


List all available routers

    php app/console router:debug


See details of a particular router

    php app/console router:debug name_of_the_router


List all available doctrine commands

    php app/console doctrine:schema


Show what doctrine command will create as in raw SQL

    php app/console doctrine:schema:update --dump-sql


Create database schema

    php app/console doctrine:schema:create


Update database schema

    php app/console doctrine:schema:update --force


Drop database schema

    php app/console doctrine:schema:drop --force


Create a new entity

    php app/console doctrine:generate:entity


Generate class property getters and setters for given entity

    php app/console doctrine:generate:entities Football/FrontendBundle/Entity/Product --no-backup


Generate class property getters and setters for all entities

    php app/console doctrine:generate:entities Football --no-backup
    php app/console doctrine:generate:entities FootballFrontendBundle --no-backup
    php app/console doctrine:generate:entities Football/FrontendBundle/Entity --no-backup


Load doctrine data fixtures

    php app/console doctrine:fixtures:load --no-interaction --no-debug


Clearing the cache

    app/console doctrine:cache:clear
    app/console doctrine:cache:clear-metadata
    app/console doctrine:cache:clear-query
    app/console doctrine:cache:clear-result


Creating a bcrypt password for secured users listen in security.yml

    php app/console security:encode-password


Registered listeners grouped by event

    php app/console debug:event-dispatcher