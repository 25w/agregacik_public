<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200823091630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', first_name VARCHAR(170) DEFAULT NULL, last_name VARCHAR(170) DEFAULT NULL, company_name VARCHAR(170) DEFAULT NULL, password VARCHAR(170) NOT NULL, private_key VARCHAR(3272) DEFAULT NULL, public_key VARCHAR(800) DEFAULT NULL, type INT DEFAULT 1 NOT NULL, UNIQUE INDEX UNIQ_8D93D649AA08CB10 (login), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_meta (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(170) DEFAULT NULL, value VARCHAR(5000) DEFAULT NULL, INDEX IDX_AD7358FCA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offer (id INT AUTO_INCREMENT NOT NULL, carrier_id INT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(170) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_29D6873E21DFC797 (carrier_id), INDEX IDX_29D6873E727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE claim (id INT AUTO_INCREMENT NOT NULL, carrier_id INT DEFAULT NULL, author_id INT DEFAULT NULL, uid VARCHAR(13) NOT NULL, name VARCHAR(170) NOT NULL, surname VARCHAR(170) NOT NULL, phone VARCHAR(170) DEFAULT NULL, email VARCHAR(170) NOT NULL, type INT NOT NULL, date_added DATETIME NOT NULL, is_authenticated TINYINT(1) NOT NULL, is_verified TINYINT(1) NOT NULL, data_after_validation TEXT DEFAULT NULL, INDEX IDX_A769DE2721DFC797 (carrier_id), INDEX IDX_A769DE27F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, claim_id INT DEFAULT NULL, offer_id INT DEFAULT NULL, name VARCHAR(170) NOT NULL, path VARCHAR(3000) NOT NULL, INDEX IDX_8C9F36107096A49F (claim_id), INDEX IDX_8C9F361053C674EE (offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE apiuser (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(170) NOT NULL, email VARCHAR(170) NOT NULL, password VARCHAR(170) NOT NULL, roles TEXT NOT NULL COMMENT \'(DC2Type:json_array)\', api_secret VARCHAR(170) NOT NULL, UNIQUE INDEX UNIQ_837A8987F85E0677 (username), UNIQUE INDEX UNIQ_837A8987E7927C74 (email), UNIQUE INDEX UNIQ_837A8987DA55AF65 (api_secret), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_meta ADD CONSTRAINT FK_AD7358FCA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E21DFC797 FOREIGN KEY (carrier_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E727ACA70 FOREIGN KEY (parent_id) REFERENCES offer (id)');
        $this->addSql('ALTER TABLE claim ADD CONSTRAINT FK_A769DE2721DFC797 FOREIGN KEY (carrier_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE claim ADD CONSTRAINT FK_A769DE27F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F36107096A49F FOREIGN KEY (claim_id) REFERENCES claim (id)');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F361053C674EE FOREIGN KEY (offer_id) REFERENCES offer (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F36107096A49F');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F361053C674EE');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E727ACA70');
        $this->addSql('ALTER TABLE claim DROP FOREIGN KEY FK_A769DE2721DFC797');
        $this->addSql('ALTER TABLE claim DROP FOREIGN KEY FK_A769DE27F675F31B');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E21DFC797');
        $this->addSql('ALTER TABLE user_meta DROP FOREIGN KEY FK_AD7358FCA76ED395');
        $this->addSql('DROP TABLE apiuser');
        $this->addSql('DROP TABLE claim');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE offer');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_meta');
    }
}
