<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200825141723 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process ADD ticket_meta_uid VARCHAR(50) NOT NULL, ADD ticket_meta_passenger_name VARCHAR(150) NOT NULL, ADD ticket_meta_carrier_id INT NOT NULL, ADD ticket_flight_no VARCHAR(150) NOT NULL, ADD ticket_class INT NOT NULL');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A883A00E2B FOREIGN KEY (ticket_meta_carrier_id) REFERENCES airline (id);');
        $this->addSql('CREATE INDEX IDX_17E6F7A883A00E2B ON claim_process (ticket_meta_carrier_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process DROP ticket_meta_uid, DROP ticket_meta_passenger_name, DROP ticket_meta_carrier, DROP ticket_flight_no, DROP ticket_class');
    }
}
