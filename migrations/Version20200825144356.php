<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200825144356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process ADD ticket_country_from_id INT DEFAULT NULL, ADD ticket_airport_from_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A8B316EA77 FOREIGN KEY (ticket_country_from_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A87908C32 FOREIGN KEY (ticket_airport_from_id) REFERENCES airport (id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A8B316EA77 ON claim_process (ticket_country_from_id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A87908C32 ON claim_process (ticket_airport_from_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A8B316EA77');
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A87908C32');
        $this->addSql('DROP INDEX IDX_17E6F7A8B316EA77 ON claim_process');
        $this->addSql('DROP INDEX IDX_17E6F7A87908C32 ON claim_process');
        $this->addSql('ALTER TABLE claim_process DROP ticket_country_from_id, DROP ticket_airport_from_id');
    }
}
