<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200825101417 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE claim_process (id INT AUTO_INCREMENT NOT NULL, claim_id INT DEFAULT NULL, worker_id INT NOT NULL, is_readable TINYINT(1) NOT NULL, is_actual TINYINT(1) NOT NULL, is_trip_completed TINYINT(1) NOT NULL, INDEX IDX_17E6F7A87096A49F (claim_id), INDEX IDX_17E6F7A86B20BA36 (worker_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A87096A49F FOREIGN KEY (claim_id) REFERENCES claim (id)');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A86B20BA36 FOREIGN KEY (worker_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE claim_process');
    }
}
