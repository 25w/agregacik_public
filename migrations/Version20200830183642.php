<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200830183642 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim ADD worth_value DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE last_login last_login DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim DROP worth_value');
        $this->addSql('ALTER TABLE user CHANGE last_login last_login DATETIME DEFAULT \'1970-01-01 00:00:00\' NOT NULL');
    }
}
