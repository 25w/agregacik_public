<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200823092021 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cr_currency (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(170) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cash_register (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_3D7AB1D9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cr_volume (id INT AUTO_INCREMENT NOT NULL, cash_register INT NOT NULL, cr_currency_id INT DEFAULT NULL, worth INT NOT NULL, type INT DEFAULT 1 NOT NULL, status INT DEFAULT 1 NOT NULL, INDEX IDX_B057CA013D7AB1D9 (cash_register), INDEX IDX_B057CA01E6E3EF1D (cr_currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cash_register ADD CONSTRAINT FK_3D7AB1D9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE cr_volume ADD CONSTRAINT FK_B057CA013D7AB1D9 FOREIGN KEY (cash_register) REFERENCES cash_register (id)');
        $this->addSql('ALTER TABLE cr_volume ADD CONSTRAINT FK_B057CA01E6E3EF1D FOREIGN KEY (cr_currency_id) REFERENCES cr_currency (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cr_volume DROP FOREIGN KEY FK_B057CA013D7AB1D9');
        $this->addSql('ALTER TABLE cr_volume DROP FOREIGN KEY FK_B057CA01E6E3EF1D');
        $this->addSql('DROP TABLE cash_register');
        $this->addSql('DROP TABLE cr_currency');
        $this->addSql('DROP TABLE cr_volume');
    }
}
