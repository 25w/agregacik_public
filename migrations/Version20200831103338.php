<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200831103338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process ADD real_country_from_id INT DEFAULT NULL, ADD real_airport_from_id INT DEFAULT NULL, ADD real_country_to_id INT DEFAULT NULL, ADD real_airport_to_id INT DEFAULT NULL, ADD real_class INT DEFAULT NULL, ADD real_date VARCHAR(255) DEFAULT NULL, ADD real_time VARCHAR(255) DEFAULT NULL, ADD real_seat VARCHAR(10) DEFAULT NULL, ADD real_gate VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A89159CD8E FOREIGN KEY (real_country_from_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A825DFABCB FOREIGN KEY (real_airport_from_id) REFERENCES airport (id)');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A855301D73 FOREIGN KEY (real_country_to_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A8854F146C FOREIGN KEY (real_airport_to_id) REFERENCES airport (id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A89159CD8E ON claim_process (real_country_from_id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A825DFABCB ON claim_process (real_airport_from_id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A855301D73 ON claim_process (real_country_to_id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A8854F146C ON claim_process (real_airport_to_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A89159CD8E');
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A825DFABCB');
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A855301D73');
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A8854F146C');
        $this->addSql('DROP INDEX IDX_17E6F7A89159CD8E ON claim_process');
        $this->addSql('DROP INDEX IDX_17E6F7A825DFABCB ON claim_process');
        $this->addSql('DROP INDEX IDX_17E6F7A855301D73 ON claim_process');
        $this->addSql('DROP INDEX IDX_17E6F7A8854F146C ON claim_process');
        $this->addSql('ALTER TABLE claim_process DROP real_country_from_id, DROP real_airport_from_id, DROP real_country_to_id, DROP real_airport_to_id, DROP real_class, DROP real_date, DROP real_time, DROP real_seat, DROP real_gate');
    }
}
