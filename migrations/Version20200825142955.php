<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200825142955 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process CHANGE ticket_meta_carrier_id ticket_meta_carrier_id INT DEFAULT NULL, CHANGE ticket_meta_uid ticket_meta_uid VARCHAR(50) DEFAULT NULL, CHANGE ticket_meta_passenger_name ticket_meta_passenger_name VARCHAR(150) DEFAULT NULL, CHANGE ticket_flight_no ticket_flight_no VARCHAR(150) DEFAULT NULL, CHANGE ticket_class ticket_class INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process CHANGE ticket_meta_carrier_id ticket_meta_carrier_id INT NOT NULL, CHANGE ticket_meta_uid ticket_meta_uid VARCHAR(50) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, CHANGE ticket_meta_passenger_name ticket_meta_passenger_name VARCHAR(150) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, CHANGE ticket_flight_no ticket_flight_no VARCHAR(150) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, CHANGE ticket_class ticket_class INT NOT NULL');
    }
}
