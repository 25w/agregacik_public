<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200825145930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process ADD ticket_country_to_id INT DEFAULT NULL, ADD ticket_airport_to_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A8D611F313 FOREIGN KEY (ticket_country_to_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE claim_process ADD CONSTRAINT FK_17E6F7A866EFA0C FOREIGN KEY (ticket_airport_to_id) REFERENCES airport (id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A8D611F313 ON claim_process (ticket_country_to_id)');
        $this->addSql('CREATE INDEX IDX_17E6F7A866EFA0C ON claim_process (ticket_airport_to_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A8D611F313');
        $this->addSql('ALTER TABLE claim_process DROP FOREIGN KEY FK_17E6F7A866EFA0C');
        $this->addSql('DROP INDEX IDX_17E6F7A8D611F313 ON claim_process');
        $this->addSql('DROP INDEX IDX_17E6F7A866EFA0C ON claim_process');
        $this->addSql('ALTER TABLE claim_process DROP ticket_country_to_id, DROP ticket_airport_to_id');
    }
}
