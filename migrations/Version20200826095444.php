<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200826095444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process CHANGE is_trip_completed is_trip_completed TINYINT(1) DEFAULT NULL, CHANGE ticket_date ticket_date VARCHAR(255) DEFAULT NULL, CHANGE ticket_time ticket_time VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE claim_process CHANGE ticket_date ticket_date DATE DEFAULT NULL, CHANGE ticket_time ticket_time TIME DEFAULT NULL, CHANGE is_trip_completed is_trip_completed TINYINT(1) NOT NULL');
    }
}
